using System;
using System.Collections.Generic;
using System.Text;

namespace SREST
{
    public class cls_LoginInfo
    {
        private static string LogUser;
        private static string Company;
        private static string CompanyCode;
        private static string Address;
        private static string Telephone;
        private static bool IsMotherCom;
        private static int LogYear, LogMonth, LogWeek;        


        public static string getLoginUser()
        {
            return LogUser;
        }
        public static void setLoginUser(string UserName)
        {
            LogUser = UserName;
        }

        public static string getCompany()
        {
            return Company;
        }
        public static void setCompany(string sCompany)
        {
            Company = sCompany;
        }

        public static string getCompanyCode()
        {
            return CompanyCode;
        }
        public static void setCompanyCode(string sCompanyCode)
        {
            CompanyCode = sCompanyCode;
        }

        public static string getAddress()
        {
            return Address;
        }
        public static void setAddress(string sAddress)
        {
            Address = sAddress;
        }

        public static string getTelephone()
        {
            return Telephone;
        }
        public static void setTelephone(string sTelephone)
        {
            Telephone = sTelephone;
        }

        public static int getLogMonth()
        {
            return LogMonth;
        }

        public static int getLogWeek()
        {
            return LogWeek;
        }

        public static void setLogWeek(int mLogWeek)
        {
            LogWeek = mLogWeek;
        }

        public static void setLogMonth(int mLogMonth)
        {
            LogMonth = mLogMonth;
        }

        public static int getLogYear()
        {
            return LogYear;
        }
        public static void setLogYear(int mLogYear)
        {
            LogYear = mLogYear;
        }

        public static bool getMotherCom()
        {
            return IsMotherCom;
        }
        public static void setMotherCom(bool mIsMotherCom)
        {
            IsMotherCom = mIsMotherCom;
        }

        public string GetLoginUser()
        {
            return LogUser;            
        }
    }
}
