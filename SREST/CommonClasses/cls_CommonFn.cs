using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Management;



namespace SREST
{
    public class cls_CommonFn
    {
        public static string GetHDDSerial()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");

            foreach (ManagementObject wmi_HD in searcher.Get())
            {
                if (wmi_HD["SerialNumber"] != null)
                    return wmi_HD["SerialNumber"].ToString();
            }
            return string.Empty;
        }

        public static void EnableTextBox(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    TBox.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void DisableTextBox(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    TBox.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void LockTextBox(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    TBox.ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void UnLockTextBox(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    TBox.ReadOnly = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void ClearTextBox(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    TBox.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetTextBoxToZero(params TextBox[] TextBoxArray)
        {
            try
            {
                foreach (TextBox TBox in TextBoxArray)
                    if (TBox.Text == string.Empty)
                        TBox.Text = "0.00";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public static void format_DataGridView(DataGridView dg)
        {
            try
            {
                dg.BorderStyle = BorderStyle.None;
                dg.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dg.RowHeadersVisible = false;
                dg.BackgroundColor = Color.AliceBlue;
                dg.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 8F, FontStyle.Underline);
                dg.EnableHeadersVisualStyles = false;
                dg.ColumnHeadersDefaultCellStyle.BackColor = Color.AliceBlue;

                //------------------Modified By Ravindra

                dg.RowHeadersDefaultCellStyle.Font = new Font("Cambria", 9F, FontStyle.Bold);
                dg.RowHeadersDefaultCellStyle.SelectionForeColor = Color.MidnightBlue;

                dg.ColumnHeadersDefaultCellStyle.Font = new Font("Cambria", 9F, FontStyle.Bold);
                dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.AliceBlue;
                dg.ColumnHeadersDefaultCellStyle.BackColor = Color.MidnightBlue;
                dg.DefaultCellStyle.Font = new Font("Cambria", 9F, FontStyle.Bold);



                //------------------------------------------





            }
            catch (Exception ex)
            {
                show_ErrorMsg(ex.Message, "DataGridView");
            }

        }
        public static void show_ErrorMsg(string msg, string header)
        {
            MessageBox.Show(msg + Environment.NewLine + Environment.NewLine + Environment.NewLine + "Payroll System" + Environment.NewLine + "", header, MessageBoxButtons.OK, MessageBoxIcon.Error);

        }


        public static bool EmptyTextBox(ErrorProvider errporCHolInfo, params TextBox[] TextBoxArray)
        {
            bool isEmptytext = true;
            try
            {
                foreach (TextBox Text in TextBoxArray)
                {
                    if (Text.Text.CompareTo("") == 0)
                    {
                        errporCHolInfo.SetError(Text, "This field cannot be empty!");
                        Text.Focus();
                       
                        Text.SelectAll();
                        try
                        {
                            TabPage tc = (TabPage)Text.Parent;
                            TabControl tcontrol = (TabControl) tc.Parent;
                            tcontrol.SelectedTab = tc;
                            //tc.Select();
                        }
                        catch (Exception ex)
                        {
                        }
                        isEmptytext =false;
                    }

                }
                return isEmptytext;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return isEmptytext;
            }
        }

        public static void ClearLabel(params Label[] LabelArray)
        {
            try
            {
                foreach (Label label in LabelArray)
                    label.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void DisableControl(params Control[] _control)
        {
            try
            {
                foreach (Control C in _control)
                    C.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void EnableControl(params Control[] _control)
        {
            try
            {
                foreach (Control C in _control)
                    C.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void EnableCheckBox(params CheckBox[] CheckBoxArray)
        {
            try
            {
                foreach (CheckBox CBox in CheckBoxArray)
                {
                    CBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void DisableCheckBox(params CheckBox[] CheckBoxArray)
        {
            try
            {
                foreach (CheckBox CBox in CheckBoxArray)
                {
                    CBox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void SetValueCheckBox(bool IsCheck, params CheckBox[] CheckBoxArray)
        {
            try
            {
                foreach (CheckBox CBox in CheckBoxArray)
                    CBox.Checked = IsCheck;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void EnableDisableRadioButton(bool IsEnable, params RadioButton[] RadioButtonArray)
        {
            try
            {
                foreach (RadioButton RButton in RadioButtonArray)
                    RButton.Enabled = IsEnable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void EnableDisableDatePicker(bool IsEnable, params DateTimePicker[] DateTimePickerArray)
        {
            try
            {
                foreach (DateTimePicker DTP in DateTimePickerArray)
                    DTP.Enabled = IsEnable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void EnableComboBox(params ComboBox[] ComboBoxArray)
        {
            try
            {
                foreach (ComboBox CBox in ComboBoxArray)
                    CBox.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void DisableComboBox(params ComboBox[] ComboBoxArray)
        {
            try
            {
                foreach (ComboBox CBox in ComboBoxArray)
                    CBox.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void EnableKryptonButton(bool IsEnable, params ComponentFactory.Krypton.Toolkit.KryptonButton[] KryBtnArray)
        {
            try
            {
                foreach (ComponentFactory.Krypton.Toolkit.KryptonButton KryBtn in KryBtnArray)
                {
                    KryBtn.Enabled = IsEnable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public static int ExistItem(DataGridView Grid, string Text, int Column)
        {
            int RowNo = -1;
            for (int i = 0; i < Grid.Rows.Count; i++)
            {
                if (Grid.Rows[i].Cells[Column].Value != null)
                {
                    if (Text == Grid.Rows[i].Cells[Column].Value.ToString())
                    {
                        RowNo = i;
                    }
                }
            }
            return RowNo;
        }

        public static int ExistItem(DataGridView Grid, string Text1, string Text2, int Column1, int Column2)
        {
            int RowNo = -1;
            for (int i = 0; i < Grid.Rows.Count; i++)
            {
                if (Grid.Rows[i].Cells[Column1].Value != null)
                {
                    if ((Text1 == Grid.Rows[i].Cells[Column1].Value.ToString()) && (Text2 == Grid.Rows[i].Cells[Column2].Value.ToString()))
                    {
                        RowNo = i;
                    }
                }
            }
            return RowNo;
        }

        public static void PlaceControl(DataGridView DataGrid, Control CtrlForPlace, int intRowNo, int intColNo, int xScrollVal, int YScrollVal)
        {
            
        }

        public static void LoadDataTableToCombo(DataTable dt, ComboBox Cmb)
        {
            Cmb.DataSource = null;
            Cmb.DataSource = dt;
            Cmb.ValueMember = dt.Columns[0].ColumnName;
            if (dt.Columns.Count > 1)
            {
                Cmb.DisplayMember = dt.Columns[1].ColumnName;
            }
            else
            {
                Cmb.DisplayMember = dt.Columns[3].ColumnName;
            }

            if (Cmb.Items.Count > 0)
            {
                Cmb.SelectedIndex =0 ;
            }
        }


        public static void LoadDataTableToCombosub(DataTable dt, ComboBox Cmb)
        {
            Cmb.DataSource = null;
            Cmb.DataSource = dt;
            Cmb.ValueMember = dt.Columns[2].ColumnName;
            if (dt.Columns.Count > 1)
            {
                Cmb.DisplayMember = dt.Columns[3].ColumnName;
            }
            else
            {
                Cmb.DisplayMember = dt.Columns[3].ColumnName;
            }

            if (Cmb.Items.Count > 0)
            {
                Cmb.SelectedIndex = 0;
            }
        }
        public static void LoadDataTableToList(DataTable dt, CheckedListBox ListBox, int PrimaryKeyLength)
        {
            char space = Convert.ToChar(" ");
            ListBox.Items.Clear();
            if (dt.Columns.Count > 1)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ListBox.Items.Add(dr[0].ToString().PadRight(PrimaryKeyLength, space) + "- " + dr[1].ToString());
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ListBox.Items.Add(dr[0].ToString());
                }
            }
        }






        


        public static string GetMonthName(int month, bool abbrev)
        {
            DateTime date = new DateTime(1900, month, 1);
            if (abbrev) return date.ToString("MMM");
            return date.ToString("MMMM");
        }



        public static int ExistsTextInDataTable(DataTable dt, int ColumnNo, string Text)
        {
            int Row = -1;
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][ColumnNo].ToString() == Text)
                    {
                        Row = i;
                        return Row;
                    }
                }
            }
            return Row;
        }

        public static bool IsInteger(string str)
        {
            bool IsInteger = false;
            try
            {
                int.Parse(str);
                IsInteger = true;
                return IsInteger;
            }
            catch
            {
                return IsInteger;
                throw;
            }
        }
       
    }
}
