using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace SREST
{
    public class clsU_User
    {
        cls_Connection cConnection = new cls_Connection();
        public string UserCode, UserName, UserPassword, UserPC, GroupCode, GroupName;
        public DataTable dt_Options;
        public static string UseName_;
        public DataTable GetUsers()
        {
            SqlCommand scm = new SqlCommand();
            scm.CommandText = "SELECT u.id ,user_name,ug.[group] AS Role FROM users as u inner join user_groups  as ug "+
                               "on u.group_id = ug.Id  ";
            return cConnection.GetDataTable(scm, "users");
        }

        public bool IsExistsUser(string UserCode, string Password)
        {
            //Password = Password != "" ? cls_Common.Encrypt(Password, "&%?,:***") : "";

            SqlCommand scmUser = new SqlCommand();
            scmUser.CommandText = "SELECT user_name ,machine_id FROM users WHERE id = @USE_CODE AND password = @USE_PASSWORD";
            scmUser.Parameters.AddWithValue("@USE_CODE", UserCode);
            scmUser.Parameters.AddWithValue("@USE_PASSWORD", Password);

            DataTable dt_User = cConnection.GetDataTable(scmUser);
            if (dt_User.Rows.Count > 0)
            {
                UserName = dt_User.Rows[0]["user_name"].ToString();
                UserPC = dt_User.Rows[0]["machine_id"].ToString();
               // UserPC = UserPC != "" ? cls_Common.Decrypt(UserPC, "&%?,:***") : "";
                UseName_ = dt_User.Rows[0]["user_name"].ToString();
                User();
                return true;
            }
            else
            {
                return false;
            }
        }

        public string User()
        {
            string U = UseName_;

            return U;
        }

    }
}
