﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SREST.BackOffice.MasterClasses
{
    public class cls_ItemSubCategory
    {
        cls_Connection cConnection = new cls_Connection();
        public string CatCode,SubCatcode,SubDescription,SubCatName;

        //save category
        public bool SaveSubCategory()
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_SAVE_SUBCATEGORY";
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@SUBCAT_CODE", SubCatcode);
                scm.Parameters.AddWithValue("@SUBCAT_DESCRIPTION", SubDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception)
            {
                cls_Connection.RollBack();
                return false;
            }
        }




        //Update Category

        public bool UpdateCategory(int catID)
        {
            try
            {

                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_UPDATE_SUBCATEGORY";
                scm.Parameters.AddWithValue("@CATID", catID);
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@SUBCAT_CODE", SubCatcode);
                scm.Parameters.AddWithValue("@SUBCAT_DESCRIPTION", SubDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                cls_Connection.RollBack();
                return false;
            }
        }


        //get all Category
        public DataTable GetAllSubCategory()
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "select cat.categoryId,sc.name,sc.description,sc.id from item_subcategory	as sc inner join item_category  as cat "+
                               " on sc.category_id = cat.id";
            return cConnection.GetDataTable(scmd, "item_category");
        }


        //Get 1 Category
        public DataTable GetSubCategory(int CatId)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "select s.category_id,i.categoryId,s.name,s.description from item_subcategory as s inner join item_category as i " +
                               " on s.category_id = i.id where s.id=@CatId";
            scmd.Parameters.AddWithValue("@CatId", CatId);
            return cConnection.GetDataTable(scmd, "item_category");
        }


        //Delete Category
        public bool deleteSubCategory(int Code)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "DELETE item_subcategory WHERE id = @COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", Code);
            cConnection.ExecuteSqlCommand(scmd);
            return true;
        }


        public bool CheckDuplicateSubCategory(string CatSubName, string SubDescription, bool Edit)
        {
            DataTable dtCat;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select * from item_subcategory where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatSubName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", SubDescription);
            dtCat = cConnection.GetDataTable(scmd);
            if (dtCat.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool CheckDuplicateSubCategoryUpdate(string CatName, string Description, bool Edit)
        {
            DataTable dtCat;
            bool count;
            SqlCommand scmd = new SqlCommand();
            count = CheckCountUpdate(CatName, Description);
            scmd.CommandText = "Select * from item_subcategory where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            dtCat = cConnection.GetDataTable(scmd);


            if (dtCat.Rows.Count > 0 && count is false)
            {
                return false;
            }
            else
            {
                return true;
            }



        }

        public bool CheckCountUpdate(string CatName, string Description)
        {
            DataTable dt;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select count(name) from item_subcategory where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            int a = Convert.ToInt32(cConnection.ExecuteScalar(scmd));
            if (a is 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
