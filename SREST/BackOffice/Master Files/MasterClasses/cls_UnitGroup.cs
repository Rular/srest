﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SREST.BackOffice.MasterClasses
{
    public class cls_UnitGroup
    {

        public string UnitGroupName;
        cls_Connection cConnection = new cls_Connection();
        //save category
        public bool SaveUnitGroup()
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_SAVE_UNITGROUP";
                scm.Parameters.AddWithValue("@UNITGROUPNAME", UnitGroupName);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception ex)
            {
                cls_Connection.RollBack();
                return false;
            }
        }


        public bool UpdateUnitGroup(int UnitGroupID)
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_UPDATE_UNITGROUP";
                scm.Parameters.AddWithValue("@UNITGROUPID", UnitGroupID);
                scm.Parameters.AddWithValue("@UNITGROUPNAME", UnitGroupName);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception ex)
            {
                cls_Connection.RollBack();
                return false;
            }
        }


        public DataTable GetAllUnitGroup()
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "SELECT group_name,id from unit_group order by id";
            return cConnection.GetDataTable(scmd, "unit_group");
        }


        //Get 1 Category
        public DataTable GetUnitGroup(int CatId)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "SELECT group_name,id from unit_group where id=@CatId";
            scmd.Parameters.AddWithValue("@CatId", CatId);
            return cConnection.GetDataTable(scmd, "unit_group");
        }


        public bool CheckDuplicateUnitGroup(string CatName, bool Edit)
        {
            DataTable dtCat;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select * from unit_group where   group_name=@COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            dtCat = cConnection.GetDataTable(scmd);
            if (dtCat.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool CheckDuplicateUnitGroupUpdate(string CatName, bool Edit)
        {
            DataTable dtCat;
            bool count;
            SqlCommand scmd = new SqlCommand();
            count = CheckCountUpdate(CatName);
            scmd.CommandText = "Select * from unit_group where group_name=@COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            dtCat = cConnection.GetDataTable(scmd);

            //if (dtCat.Rows.Count > 0 && count is false)
            //{
            //    return false;
            //}
            //else
            //    return true;
            if (dtCat.Rows.Count > 0 && count is false)
            {
                return false;
            }
            //else if (count is true)
            //{
            //    return true;
            //}
            else
            {
                return false;
            }



        }

        public bool CheckCountUpdate(string CatName)
        {
            DataTable dt;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select count(group_name) from unit_group where group_name=@COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            int a = Convert.ToInt32(cConnection.ExecuteScalar(scmd));
            if (a is 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public bool deleteUnitGroup(int Code)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "DELETE unit_group WHERE id = @COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", Code);
            cConnection.ExecuteSqlCommand(scmd);
            return true;
        }

    }
}
