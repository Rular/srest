﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SREST.BackOffice.MasterClasses
{
   public class cls_ItemSubCategory2
    {
        cls_Connection cConnection = new cls_Connection();
        public string CatCode, SubCatcode, SubDescription, Sub2CatName;



        //save subcategory2
        public bool SaveSubCategory2()
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_SAVE_SUBCATEGORY2";
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@SUBCAT_CODE", SubCatcode);
                scm.Parameters.AddWithValue("@SUBCAT2_CODE", Sub2CatName);
                scm.Parameters.AddWithValue("@SUBCAT2_DESCRIPTION", SubDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception ex)
            {
                cls_Connection.RollBack();
                return false;
            }
        }

        public bool UpdateSubCategory2(int subcat2id)
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_UPDATE_SUBCATEGORY2";
                scm.Parameters.AddWithValue("@SUBCAT2ID", subcat2id);
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@SUBCAT_CODE", SubCatcode);
                scm.Parameters.AddWithValue("@SUBCAT2_CODE", Sub2CatName);
                scm.Parameters.AddWithValue("@SUBCAT2_DESCRIPTION", SubDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception ex)
            {
                cls_Connection.RollBack();
                return false;
            }
        }


        //get all SubCategory
        public DataTable GetAllSubCategory()
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText =" select cat.categoryId,sub.name,sub2.name as subcat2name,sub2.description,sub2.id from item_subcategory_2 as sub2 "+
                               " inner join item_subcategory as sub on "+
                               " sub2.subcategory_id = sub.id inner join item_category as cat "+
                               " on cat.id = sub.category_id";
            return cConnection.GetDataTable(scmd, "item_Subcategory2");
        }


        //get one SubCategory
        public DataTable GetSubCategory(int catid)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = " select cat.id as catid,cat.description,sub.id as subcatid,sub.description,sub2.name as sub2id,sub2.description as sub2description from item_subcategory_2 as sub2 " +
                               " inner join item_subcategory as sub on " +
                               " sub2.subcategory_id = sub.id inner join item_category as cat " +
                               " on cat.id = sub.category_id where sub2.id=@COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", catid);
            return cConnection.GetDataTable(scmd, "item_Subcategory2");
        }

        //Delete subcategory2
        public bool deleteSubCategory2(int Code)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "DELETE item_subcategory_2 WHERE id = @COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", Code);
            cConnection.ExecuteSqlCommand(scmd);
            return true;
        }

        public bool CheckDuplicateSubCategory(string CatSubName, string SubDescription, bool Edit)
        {
            DataTable dtCat;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select * from item_subcategory_2 where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatSubName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", SubDescription);
            dtCat = cConnection.GetDataTable(scmd);
            if (dtCat.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool CheckDuplicateSubCategoryUpdate(string CatName, string Description, bool Edit)
        {
            DataTable dtCat;
            bool count;
            SqlCommand scmd = new SqlCommand();
            count = CheckCountUpdate(CatName, Description);
            scmd.CommandText = "Select * from item_subcategory_2 where name=@COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            dtCat = cConnection.GetDataTable(scmd);


            if (dtCat.Rows.Count > 0 && count is false)
            {
                return false;
            }
            else
            {
                return true;
            }



        }

        public bool CheckCountUpdate(string CatName, string Description)
        {
            DataTable dt;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select count(name) from item_subcategory_2 where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            int a = Convert.ToInt32(cConnection.ExecuteScalar(scmd));
            if (a is 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
