﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SREST.BackOffice.Master_Files.MasterClasses
{
    public class clsm_Unit
    {
        cls_Connection cConnection = new cls_Connection();

        public string UsergroupId, UnitCode, UnitDescription;

        //Save Unit
        public bool SaveUnit()
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_SAVE_UNIT";
                scm.Parameters.AddWithValue("@UNITGROUPID", UsergroupId);
                scm.Parameters.AddWithValue("@UNIT", UnitCode);
                scm.Parameters.AddWithValue("@UNITDESCRIPTION", UnitDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception)
            {
                cls_Connection.RollBack();
                return false;
            }
        }



        //Update Unit
        public bool SaveUnit(int unitid)
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_UPDATE_UNIT";
                scm.Parameters.AddWithValue("@UNITGROUPID", unitid);
                scm.Parameters.AddWithValue("@UNITGROUPID", UsergroupId);
                scm.Parameters.AddWithValue("@UNIT", UnitCode);
                scm.Parameters.AddWithValue("@UNITDESCRIPTION", UnitDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception)
            {
                cls_Connection.RollBack();
                return false;
            }
        }


        //get all Units
        public DataTable GetAllUnit()
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "select ug.id as usergroupid,ug.group_name groupname,unit,u.description,u.id from unit_group as ug inner join units as u " +
                              " on ug.id = u.unit_group_id ";
            return cConnection.GetDataTable(scmd, "Units");
        }

        //get one Units
        public DataTable GetUnit(int unitid)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "select ug.id as usergroupid,unit,u.description from unit_group as ug inner join units as u " +
                              " on ug.id = u.unit_group_id where u.id=@UnitId";
            scmd.Parameters.AddWithValue("@UnitId", unitid);
            return cConnection.GetDataTable(scmd, "Units");
        }


        //Delete Category
        public bool deleteUnit(int Code)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "DELETE units WHERE id = @COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", Code);
            cConnection.ExecuteSqlCommand(scmd);
            return true;
        }


        public bool CheckDuplicateUnit(string CatSubName, string SubDescription, bool Edit)
        {
            DataTable dtCat;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select * from units where unit=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatSubName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", SubDescription);
            dtCat = cConnection.GetDataTable(scmd);
            if (dtCat.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool CheckDuplicateUnitUpdate(string CatName, string Description, bool Edit)
        {
            DataTable dtCat;
            bool count;
            SqlCommand scmd = new SqlCommand();
            count = CheckCountUpdate(CatName, Description);
            scmd.CommandText = "Select * from units where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            dtCat = cConnection.GetDataTable(scmd);


            if (dtCat.Rows.Count > 0 && count is false)
            {
                return false;
            }
            else
            {
                return true;
            }



        }

        public bool CheckCountUpdate(string CatName, string Description)
        {
            DataTable dt;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select count(name) from units where name=@COD_CODE ";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            int a = Convert.ToInt32(cConnection.ExecuteScalar(scmd));
            if (a is 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

    }
}
