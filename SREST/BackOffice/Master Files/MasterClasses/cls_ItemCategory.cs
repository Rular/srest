﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SREST.BackOffice.MasterClasses
{
    public class cls_ItemCategory
    {
        cls_Connection cConnection = new cls_Connection();
        public string CatCode, CatDescription;

        //save category
        public bool SaveCategory()
        {
            try
            {


                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_SAVE_CATEGORY";
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@CAT_DESCRIPTION", CatDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;

            }
            catch (Exception)
            {
                cls_Connection.RollBack();
                return false;
            }
        }



        //Update Category

        public bool UpdateCategory(int catID)
        {
            try
            {

                SqlCommand scm = new SqlCommand();
                scm.CommandType = CommandType.StoredProcedure;
                cls_Connection.BegingTransaction();
                scm.CommandText = "USPM_UPDATE_CATEGORY";
                scm.Parameters.AddWithValue("@CATID", catID);
                scm.Parameters.AddWithValue("@CAT_CODE", CatCode);
                scm.Parameters.AddWithValue("@CAT_DESCRIPTION", CatDescription);
                scm.Parameters.AddWithValue("@HOTEL_ID", cls_Connection.Hotel_Id);
                scm.Parameters.AddWithValue("@COMPANY_ID", cls_Connection.Company_Id);
                cConnection.ExecuteSqlCommand(scm);
                cls_Connection.CommitTransaction();
                return true;
            }
            catch (Exception)
            {
                cls_Connection.RollBack();
                return false;
            }
        }

        //get all Category
        public DataTable GetAllCategory()
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "SELECT categoryid,description,id from item_category order by id";
            return cConnection.GetDataTable(scmd, "item_category");
        }


        //Get 1 Category
        public DataTable GetCategory(int CatId)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "SELECT categoryid,description from item_category where id=@CatId";
            scmd.Parameters.AddWithValue("@CatId", CatId);
            return cConnection.GetDataTable(scmd, "item_category");
        }


        //Delete Category
        public bool deleteCategory(int Code)
        {
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "DELETE item_category WHERE id = @COD_CODE";
            scmd.Parameters.AddWithValue("@COD_CODE", Code);
            cConnection.ExecuteSqlCommand(scmd);
            return true;
        }


        public bool CheckDuplicateCategory(string CatName, string Description, bool Edit)
        {
            DataTable dtCat;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select * from item_category where categoryId=@COD_CODE or  Description=@COD_DESCRIPTION";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            dtCat = cConnection.GetDataTable(scmd);
            if (dtCat.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public bool CheckDuplicateCategoryUpdate(string CatName, string Description, bool Edit)
        {
            DataTable dtCat;
            bool count;
            SqlCommand scmd = new SqlCommand();
            count = CheckCountUpdate(CatName, Description);
            scmd.CommandText = "Select * from item_category where categoryId=@COD_CODE or  Description=@COD_DESCRIPTION";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            dtCat = cConnection.GetDataTable(scmd);


            if (dtCat.Rows.Count > 0 && count is false)
            {
                return false;
            }
            else if (count is true)
            {
                return true;
            }
            else
            {
                return false;
            }



        }

        public bool CheckCountUpdate(string CatName, string Description)
        {
            DataTable dt;
            SqlCommand scmd = new SqlCommand();
            scmd.CommandText = "Select count(categoryId) from item_category where categoryId=@COD_CODE or  Description=@COD_DESCRIPTION";
            scmd.Parameters.AddWithValue("@COD_CODE", CatName);
            scmd.Parameters.AddWithValue("@COD_DESCRIPTION", Description);
            int a = Convert.ToInt32(cConnection.ExecuteScalar(scmd));
            if (a is 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

    }
}
