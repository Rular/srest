﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzTable : frmC_ModelBlack
    {
        public frmB_BackOfzTable()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzTable singleton = null;

        public static frmB_BackOfzTable Instance
        {
            get
            {
                if (frmB_BackOfzTable.singleton == null)
                {
                    frmB_BackOfzTable.singleton = new frmB_BackOfzTable();
                }
                return frmB_BackOfzTable.singleton;
            }
        }

        private void FrmB_BackOfzTable_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzTable.singleton = null;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzTable.singleton = null;
            this.Close();
        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddTable().ShowDialog();
        }
    }
}
