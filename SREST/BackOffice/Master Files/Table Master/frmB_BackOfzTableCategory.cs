﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzTableCategory : frmC_ModelBlack
    {
        public frmB_BackOfzTableCategory()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzTableCategory singleton = null;

        public static frmB_BackOfzTableCategory Instance
        {
            get
            {
                if (frmB_BackOfzTableCategory.singleton == null)
                {
                    frmB_BackOfzTableCategory.singleton = new frmB_BackOfzTableCategory();
                }
                return frmB_BackOfzTableCategory.singleton;
            }
        }

        private void FrmB_BackOfzTableCategory_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzTableCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzTableCategory.singleton = null;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzTableCategory.singleton = null;
            this.Close();
        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddTableCategory().ShowDialog();
        }
    }
}
