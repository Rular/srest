﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzAddItemType : Form
    {
        public frmB_BackOfzAddItemType()
        {
            InitializeComponent();
        }

        private void FrmB_BackOfzAddItemType_Load(object sender, EventArgs e)
        {

        }

        private void BttnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BttnRawItem_Click(object sender, EventArgs e)
        {         
            Close();
            new frmB_BackOfzAddItemRaw().ShowDialog();
        }

        private void BttnProductItem_Click(object sender, EventArgs e)
        {
            Close();
            new frmB_BackOfzAddItemProduct().ShowDialog();
        }
    }
}
