﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzAddSubCategory2 : Form
    {
        cls_ItemCategory cat = new cls_ItemCategory();
        cls_ItemSubCategory subcat= new cls_ItemSubCategory();
        cls_ItemSubCategory2 subcat2 = new cls_ItemSubCategory2();


        public static int SubCatId;
        static int CatId;
        static bool isEditCategory = false;
        public DataTable cate;
        public frmB_BackOfzAddSubCategory2()
        {
            InitializeComponent();
        }

        public static void GetEditDetails(bool edit, int CatCode)
        {
            isEditCategory = edit;
            SubCatId = CatCode;
        }
        private void Itemcategory()
        {

            DataTable dt = cat.GetAllCategory();
            cmbCategory.DataSource = dt;
            cmbCategory.DisplayMember = "categoryid";
            cmbCategory.ValueMember = "id";


        }

        private void ItemSubcategory()
        {

            DataTable dt = subcat.GetAllSubCategory();
            cmbSubCategory.DataSource = dt;
            cmbSubCategory.DisplayMember = "description";
            cmbSubCategory.ValueMember = "id";


        }

        private void FrmB_BackOfzAddSubCategory2_Load(object sender, EventArgs e)
        {
            Itemcategory();
            ItemSubcategory();

            if(isEditCategory)
            {
                loadSubCategory();
                bttnSave.Text = "Update (F2)";
            }
        }

        private void BttnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // hell Validate TextBoxes
                if (validate())
                {
                    subcat2.CatCode = cmbCategory.SelectedValue.ToString();
                    subcat2.SubCatcode = cmbSubCategory.SelectedValue.ToString();
                    subcat2.Sub2CatName = txtSubCategory2.Text.Trim();
                    subcat2.SubDescription = txtDescription.Text.ToString();

                    if (isEditCategory == true)
                    {

                        updateSubCategory();
                       // isEditCategory = false;
                    }

                    else if (isEditCategory == false)
                    {
                        if (subcat2.CheckDuplicateSubCategory(txtSubCategory2.Text.Trim(), txtDescription.Text.Trim(), false) is true)
                        {
                            if (subcat2.SaveSubCategory2() == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Added";
                                clear();
                                bttnSave.Text = "Save (F2)";
                                frmB_BackOfzItemSubCategory2 itemSub = frmB_BackOfzItemSubCategory2.Instance;
                                itemSub.LoadDataGrid();

                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Sub Category Details";
                            bttnSave.Text = "Update (F2)";

                        }


                    }


                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }



        public void updateSubCategory()
        {
            try
            {

                if (validate())
                {

                    subcat2.CatCode = cmbCategory.SelectedValue.ToString();
                    subcat2.SubCatcode = cmbSubCategory.SelectedValue.ToString();
                    subcat2.Sub2CatName = txtSubCategory2.Text.Trim();
                    subcat2.SubDescription = txtDescription.Text.ToString();


                    if (isEditCategory == true)
                    {
                        //Check Count For Update
                        if (subcat2.CheckDuplicateSubCategoryUpdate(txtSubCategory2.Text.Trim(), txtDescription.Text.Trim(), isEditCategory) is true)
                        {
                            if (subcat2.UpdateSubCategory2(SubCatId) == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Updated";
                                clear();
                                isEditCategory = false;
                                frmB_BackOfzItemSubCategory2 ItemCat = frmB_BackOfzItemSubCategory2.Instance;
                                ItemCat.LoadDataGrid();
                                bttnSave.Text = "Save (F2)";
                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate sub2 Category Details";
                            bttnSave.Text = "Update(F2)";
                            return;
                        }

                    }

                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }


        //validate text Boxes
        public bool validate()
        {

            if (string.IsNullOrEmpty(cmbCategory.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Select Your Category Code To Continue";
                txtSubCategory2.Focus();
                return false;

            }


            if (string.IsNullOrEmpty(cmbSubCategory.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Select Your Sub Category Code To Continue";
                cmbSubCategory.Focus();
                return false;

            }

            if (string.IsNullOrEmpty(txtSubCategory2.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Sub Category2 Code To Continue";
                txtSubCategory2.Focus();
                return false;

            }

            if (string.IsNullOrEmpty(txtDescription.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Sub Category Description To Continue";
                txtDescription.Focus();
                return false;
            }
            else
            {
                return true;
            }

            return false;
        }


        //clear
        void clear()
        {
            cmbCategory.DataSource = null;
            cmbSubCategory.DataSource = null;
            txtSubCategory2.Clear();
            txtDescription.Clear();
            txtSubCategory2.Focus();
        }

        void loadSubCategory()
        {
            cate = subcat2.GetSubCategory(SubCatId);
            if (cate.Rows.Count > 0)
            {

                //cls_CommonFn.LoadDataTableToCombo(cate, cmbCategory);
                //cmbCategory.SelectedIndex = 0;
                //cmbCategory.Focus();

                //cls_CommonFn.LoadDataTableToCombosub(cate, cmbSubCategory);
                //cmbSubCategory.SelectedIndex = 0;
                //cmbSubCategory.Focus();
                cmbCategory.SelectedValue = int.Parse(cate.Rows[0]["catid"].ToString());
                cmbSubCategory.SelectedValue = int.Parse(cate.Rows[0]["subcatid"].ToString());

                txtSubCategory2.Text = cate.Rows[0]["sub2id"].ToString();
                txtDescription.Text = cate.Rows[0]["sub2description"].ToString();
            }
        }

        private void CmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Itemcategory();
        }

        private void CmbSubCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
           // ItemSubcategory();
        }

        private void CmbCategory_ChangeUICues(object sender, UICuesEventArgs e)
        {
           // Itemcategory();
        }

        private void CmbSubCategory_Click(object sender, EventArgs e)
        {
           // ItemSubcategory();
        }

        private void CmbCategory_Click(object sender, EventArgs e)
        {
            //Itemcategory();
        }
    }
}
