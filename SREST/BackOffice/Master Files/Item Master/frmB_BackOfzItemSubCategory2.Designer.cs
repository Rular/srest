﻿namespace SREST
{
    partial class frmB_BackOfzItemSubCategory2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzItemSubCategory2));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bttnDelete = new System.Windows.Forms.Button();
            this.bttnEditCategory = new System.Windows.Forms.Button();
            this.bttnNewCategory = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SearchBar = new ADGV.SearchToolBar();
            this.dgwSubCategory2 = new ADGV.AdvancedDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwSubCategory2)).BeginInit();
            this.SuspendLayout();
            // 
            // bttnClose
            // 
            this.bttnClose.Location = new System.Drawing.Point(1127, 0);
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // lblFormCaption
            // 
            this.lblFormCaption.Location = new System.Drawing.Point(879, 0);
            this.lblFormCaption.Text = "frmC_Model";
            // 
            // bttnDelete
            // 
            this.bttnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(42)))), ((int)(((byte)(16)))));
            this.bttnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnDelete.FlatAppearance.BorderSize = 0;
            this.bttnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnDelete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnDelete.Image = ((System.Drawing.Image)(resources.GetObject("bttnDelete.Image")));
            this.bttnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnDelete.Location = new System.Drawing.Point(255, 7);
            this.bttnDelete.Name = "bttnDelete";
            this.bttnDelete.Size = new System.Drawing.Size(111, 42);
            this.bttnDelete.TabIndex = 727;
            this.bttnDelete.TabStop = false;
            this.bttnDelete.Text = "Delete";
            this.bttnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnDelete.UseVisualStyleBackColor = false;
            this.bttnDelete.Click += new System.EventHandler(this.BttnDelete_Click);
            // 
            // bttnEditCategory
            // 
            this.bttnEditCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(95)))), ((int)(((byte)(144)))));
            this.bttnEditCategory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnEditCategory.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnEditCategory.FlatAppearance.BorderSize = 0;
            this.bttnEditCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnEditCategory.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.bttnEditCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnEditCategory.Image = ((System.Drawing.Image)(resources.GetObject("bttnEditCategory.Image")));
            this.bttnEditCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnEditCategory.Location = new System.Drawing.Point(138, 7);
            this.bttnEditCategory.Name = "bttnEditCategory";
            this.bttnEditCategory.Size = new System.Drawing.Size(111, 42);
            this.bttnEditCategory.TabIndex = 726;
            this.bttnEditCategory.TabStop = false;
            this.bttnEditCategory.Text = "Edit";
            this.bttnEditCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnEditCategory.UseVisualStyleBackColor = false;
            this.bttnEditCategory.Click += new System.EventHandler(this.BttnEditCategory_Click);
            // 
            // bttnNewCategory
            // 
            this.bttnNewCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            this.bttnNewCategory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnNewCategory.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnNewCategory.FlatAppearance.BorderSize = 0;
            this.bttnNewCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNewCategory.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.bttnNewCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnNewCategory.Image = ((System.Drawing.Image)(resources.GetObject("bttnNewCategory.Image")));
            this.bttnNewCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNewCategory.Location = new System.Drawing.Point(21, 7);
            this.bttnNewCategory.Name = "bttnNewCategory";
            this.bttnNewCategory.Size = new System.Drawing.Size(111, 42);
            this.bttnNewCategory.TabIndex = 725;
            this.bttnNewCategory.TabStop = false;
            this.bttnNewCategory.Text = "New (F5)";
            this.bttnNewCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnNewCategory.UseVisualStyleBackColor = false;
            this.bttnNewCategory.Click += new System.EventHandler(this.BttnNewCategory_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.SearchBar, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgwSubCategory2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 57);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1146, 358);
            this.tableLayoutPanel1.TabIndex = 728;
            // 
            // SearchBar
            // 
            this.SearchBar.AllowMerge = false;
            this.SearchBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.SearchBar.Location = new System.Drawing.Point(0, 0);
            this.SearchBar.MaximumSize = new System.Drawing.Size(0, 25);
            this.SearchBar.MinimumSize = new System.Drawing.Size(0, 25);
            this.SearchBar.Name = "SearchBar";
            this.SearchBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.SearchBar.Size = new System.Drawing.Size(1146, 25);
            this.SearchBar.TabIndex = 722;
            this.SearchBar.Text = "Search Bar";
            // 
            // dgwSubCategory2
            // 
            this.dgwSubCategory2.AllowUserToAddRows = false;
            this.dgwSubCategory2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Linen;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgwSubCategory2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgwSubCategory2.AutoGenerateContextFilters = true;
            this.dgwSubCategory2.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.dgwSubCategory2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgwSubCategory2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgwSubCategory2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(95)))), ((int)(((byte)(144)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwSubCategory2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgwSubCategory2.ColumnHeadersHeight = 35;
            this.dgwSubCategory2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgwSubCategory2.DateWithTime = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgwSubCategory2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgwSubCategory2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwSubCategory2.EnableHeadersVisualStyles = false;
            this.dgwSubCategory2.GridColor = System.Drawing.Color.AliceBlue;
            this.dgwSubCategory2.Location = new System.Drawing.Point(3, 28);
            this.dgwSubCategory2.MultiSelect = false;
            this.dgwSubCategory2.Name = "dgwSubCategory2";
            this.dgwSubCategory2.ReadOnly = true;
            this.dgwSubCategory2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwSubCategory2.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgwSubCategory2.RowHeadersWidth = 25;
            this.dgwSubCategory2.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgwSubCategory2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgwSubCategory2.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgwSubCategory2.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            this.dgwSubCategory2.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgwSubCategory2.RowTemplate.Height = 30;
            this.dgwSubCategory2.Size = new System.Drawing.Size(1140, 523);
            this.dgwSubCategory2.TabIndex = 717;
            this.dgwSubCategory2.TimeFilter = true;
            // 
            // frmB_BackOfzItemSubCategory2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 415);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.bttnDelete);
            this.Controls.Add(this.bttnEditCategory);
            this.Controls.Add(this.bttnNewCategory);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzItemSubCategory2";
            this.Text = "Sub Category 2";
            this.Load += new System.EventHandler(this.FrmB_BackOfzItemSubCategory2_Load);
            this.Controls.SetChildIndex(this.bttnNewCategory, 0);
            this.Controls.SetChildIndex(this.bttnEditCategory, 0);
            this.Controls.SetChildIndex(this.bttnDelete, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwSubCategory2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnDelete;
        private System.Windows.Forms.Button bttnEditCategory;
        private System.Windows.Forms.Button bttnNewCategory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public ADGV.SearchToolBar SearchBar;
        private ADGV.AdvancedDataGridView dgwSubCategory2;
    }
}