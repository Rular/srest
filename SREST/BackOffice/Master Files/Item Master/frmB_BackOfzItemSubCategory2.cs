﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzItemSubCategory2 : frmC_ModelBlack
    {

        cls_ItemCategory cat = new cls_ItemCategory();
        cls_ItemSubCategory subcat = new cls_ItemSubCategory();
        cls_ItemSubCategory2 subcat2 = new cls_ItemSubCategory2();

        public int SubCatCode;
        public bool IsEdit = false;
        public frmB_BackOfzItemSubCategory2()
        {
            InitializeComponent();
        }


        private static frmB_BackOfzItemSubCategory2 singleton = null;

        public static frmB_BackOfzItemSubCategory2 Instance
        {
            get
            {
                if (frmB_BackOfzItemSubCategory2.singleton == null)
                {
                    frmB_BackOfzItemSubCategory2.singleton = new frmB_BackOfzItemSubCategory2();
                }
                return frmB_BackOfzItemSubCategory2.singleton;
            }
        }

        private void FrmB_BackOfzItemSubCategory2_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        public void LoadDataGrid()
        {
            DataTable dtSubCategory2 = subcat2.GetAllSubCategory();

            if (dtSubCategory2.Columns.Count >= 1)
                dgwSubCategory2.DataSource = dtSubCategory2;

            dgwSubCategory2.Columns[4].Visible = false;


        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemSubCategory2.singleton = null;
            this.Close();
        }

        private void BttnNewCategory_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddSubCategory2().ShowDialog();

        }

        private void BttnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgwSubCategory2.SelectedRows.Count > 0)
                {
                    SubCatCode = int.Parse(dgwSubCategory2.CurrentRow.Cells[4].FormattedValue.ToString());
                }
                else
                {
                    MessageBox.Show("Please Select Category to Continue");
                    return;
                }


                if (dgwSubCategory2.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Are You Sure You Want Delete This SubCategory2?", Application.CompanyName + " " + Application.ProductName + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (subcat2.deleteSubCategory2(SubCatCode) == true)
                        {
                            MessageBox.Show("Success Fully Deleted");
                            frmB_BackOfzItemSubCategory2 ItemCat = frmB_BackOfzItemSubCategory2.Instance;
                            ItemCat.LoadDataGrid();

                        }
                        else
                        {
                            MessageBox.Show("Error");
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Please Select Full Row To Continue");
                }
            }

            catch (Exception EX)
            {

                MessageBox.Show("This SubCategory2 Use Other Products Unable To Delete");
            }
        }

        private void BttnEditCategory_Click(object sender, EventArgs e)
        {
            if (dgwSubCategory2.SelectedRows.Count > 0)
            {
                IsEdit = true;

                frmB_BackOfzAddSubCategory2.GetEditDetails(IsEdit, int.Parse(dgwSubCategory2.CurrentRow.Cells[4].FormattedValue.ToString()));
                new frmB_BackOfzAddSubCategory2().ShowDialog();
            }

            else
                MessageBox.Show("Please Select Full Row To Continue");
        }
    }
}
