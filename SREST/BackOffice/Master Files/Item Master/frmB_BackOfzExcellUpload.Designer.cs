﻿namespace SREST
{
    partial class frmB_BackOfzExcellUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.txtCompany = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.bttnUpload = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.cmbProperty = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cmbType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.bttnExcellFile = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 744;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Excell Upload";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(26, 335);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(397, 23);
            this.lblMsg.TabIndex = 747;
            this.lblMsg.Text = "Messege";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(220, 375);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 6;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // txtCompany
            // 
            // 
            // 
            // 
            this.txtCompany.CustomButton.Image = null;
            this.txtCompany.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtCompany.CustomButton.Name = "";
            this.txtCompany.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCompany.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCompany.CustomButton.TabIndex = 1;
            this.txtCompany.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCompany.CustomButton.UseSelectable = true;
            this.txtCompany.CustomButton.Visible = false;
            this.txtCompany.Enabled = false;
            this.txtCompany.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtCompany.Lines = new string[0];
            this.txtCompany.Location = new System.Drawing.Point(30, 95);
            this.txtCompany.MaxLength = 32767;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.PasswordChar = '\0';
            this.txtCompany.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCompany.SelectedText = "";
            this.txtCompany.SelectionLength = 0;
            this.txtCompany.SelectionStart = 0;
            this.txtCompany.ShortcutsEnabled = true;
            this.txtCompany.Size = new System.Drawing.Size(397, 23);
            this.txtCompany.TabIndex = 1;
            this.txtCompany.UseSelectable = true;
            this.txtCompany.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCompany.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(30, 69);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(75, 19);
            this.metroLabel1.TabIndex = 745;
            this.metroLabel1.Text = "Company :";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // bttnUpload
            // 
            this.bttnUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnUpload.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnUpload.Location = new System.Drawing.Point(327, 375);
            this.bttnUpload.Name = "bttnUpload";
            this.bttnUpload.Size = new System.Drawing.Size(103, 44);
            this.bttnUpload.TabIndex = 5;
            this.bttnUpload.Text = "&Upload ( F2 )";
            this.bttnUpload.UseCustomBackColor = true;
            this.bttnUpload.UseCustomForeColor = true;
            this.bttnUpload.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(113, 375);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 7;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // cmbProperty
            // 
            this.cmbProperty.FormattingEnabled = true;
            this.cmbProperty.ItemHeight = 23;
            this.cmbProperty.Location = new System.Drawing.Point(30, 152);
            this.cmbProperty.Name = "cmbProperty";
            this.cmbProperty.Size = new System.Drawing.Size(397, 29);
            this.cmbProperty.TabIndex = 2;
            this.cmbProperty.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(30, 127);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(69, 19);
            this.metroLabel3.TabIndex = 763;
            this.metroLabel3.Text = "Property :";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.ItemHeight = 23;
            this.cmbType.Location = new System.Drawing.Point(30, 220);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(397, 29);
            this.cmbType.TabIndex = 3;
            this.cmbType.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(30, 195);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(44, 19);
            this.metroLabel2.TabIndex = 765;
            this.metroLabel2.Text = "Type :";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // bttnExcellFile
            // 
            // 
            // 
            // 
            this.bttnExcellFile.CustomButton.Image = null;
            this.bttnExcellFile.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.bttnExcellFile.CustomButton.Name = "";
            this.bttnExcellFile.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.bttnExcellFile.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.bttnExcellFile.CustomButton.TabIndex = 1;
            this.bttnExcellFile.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.bttnExcellFile.CustomButton.UseSelectable = true;
            this.bttnExcellFile.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.bttnExcellFile.Lines = new string[0];
            this.bttnExcellFile.Location = new System.Drawing.Point(33, 298);
            this.bttnExcellFile.MaxLength = 32767;
            this.bttnExcellFile.Name = "bttnExcellFile";
            this.bttnExcellFile.PasswordChar = '\0';
            this.bttnExcellFile.PromptText = "Click Button to Browse";
            this.bttnExcellFile.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.bttnExcellFile.SelectedText = "";
            this.bttnExcellFile.SelectionLength = 0;
            this.bttnExcellFile.SelectionStart = 0;
            this.bttnExcellFile.ShortcutsEnabled = true;
            this.bttnExcellFile.ShowButton = true;
            this.bttnExcellFile.Size = new System.Drawing.Size(397, 23);
            this.bttnExcellFile.TabIndex = 4;
            this.bttnExcellFile.UseSelectable = true;
            this.bttnExcellFile.WaterMark = "Click Button to Browse";
            this.bttnExcellFile.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.bttnExcellFile.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.bttnExcellFile.Click += new System.EventHandler(this.MetroTextBox1_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.Color.DimGray;
            this.metroLabel4.Location = new System.Drawing.Point(33, 272);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(86, 19);
            this.metroLabel4.TabIndex = 767;
            this.metroLabel4.Text = "Excell Sheet :";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            this.metroLabel4.Click += new System.EventHandler(this.MetroLabel4_Click);
            // 
            // frmB_BackOfzExcellUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnClose;
            this.ClientSize = new System.Drawing.Size(458, 445);
            this.Controls.Add(this.bttnExcellFile);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.cmbProperty);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnUpload);
            this.Controls.Add(this.bttnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzExcellUpload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzExcellUpload";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmB_BackOfzExcellUpload_FormClosing);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroTextBox txtCompany;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton bttnUpload;
        private MetroFramework.Controls.MetroButton bttnClose;
        private MetroFramework.Controls.MetroComboBox cmbProperty;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cmbType;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox bttnExcellFile;
        private MetroFramework.Controls.MetroLabel metroLabel4;
    }
}