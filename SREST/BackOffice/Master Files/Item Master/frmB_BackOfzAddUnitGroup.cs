﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzAddUnitGroup : Form
    {
        public static string UnitGroupName;
        public static int UnitGroupId;
        public static bool isEditCategory = false;


        cls_UnitGroup unit = new cls_UnitGroup();
        public frmB_BackOfzAddUnitGroup()
        {
            InitializeComponent();
        }

        public static void GetEditDetails(bool edit, int CatCode,string UbitGroupName)
        {
            isEditCategory = edit;
            UnitGroupId = CatCode;
            UnitGroupName = UbitGroupName;
        }


        public static void GetDeleteDetails( int CatCode)
        {
            UnitGroupId = CatCode;
        }
        private void BttnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // hell Validate TextBoxes
                if (validate())
                {
                    unit.UnitGroupName = txtUnitGroup.Text.ToString().Trim();

                    if (isEditCategory == true)
                    {
                        updateUnitGroup();
                        isEditCategory = false;
                    }

                    else if (isEditCategory == false)
                    {
                        if (unit.CheckDuplicateUnitGroup(txtUnitGroup.Text.Trim(), false) is true)
                        {
                            if (unit.SaveUnitGroup() == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Added";
                                clear();
                                bttnSave.Text = "Save (F2)";
                                frmB_BackOfzUnitGroup ItemCat = frmB_BackOfzUnitGroup.Instance;
                                ItemCat.LoadDataGrid();

                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate UnitGroup Details";

                        }


                    }


                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }

        //update Category
        public void updateUnitGroup()
        {
            try
            {

                if (validate())
                {
                    
                    unit.UnitGroupName = txtUnitGroup.Text.ToString().Trim();

                    if (isEditCategory == true)
                    {
                        //Check Count For Update
                        if (unit.CheckDuplicateUnitGroupUpdate(txtUnitGroup.Text.Trim(), isEditCategory) is true)
                        {
                            if (unit.UpdateUnitGroup(UnitGroupId) == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Updated";
                                clear();
                                frmB_BackOfzUnitGroup ItemCat = frmB_BackOfzUnitGroup.Instance;
                                ItemCat.LoadDataGrid();
                                bttnSave.Text = "Save (F2)";
                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Category Details";
                            bttnSave.Text = "Update(F2)";
                            return;
                        }

                    }

                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }



        private void BttnClear_Click(object sender, EventArgs e)
        {
             clear();
        }

        //clear text Boxes
        void clear()
        {
            txtUnitGroup.Clear();
            txtUnitGroup.Focus();
            lblMsg.Visible = false;
        }

        public bool validate()
        {
            if (string.IsNullOrEmpty(txtUnitGroup.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Unit Group Name Code To Continue";
                txtUnitGroup.Focus();
                return false;

            }

            else
            {
                return true;
            }

            return false;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmB_BackOfzAddUnitGroup_Load(object sender, EventArgs e)
        {
            if(isEditCategory)
            {
                txtUnitGroup.Text = UnitGroupName;
                bttnSave.Text = "Update (F2)";
            }
        }

       
    }
}
