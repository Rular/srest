﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzAddCategory : Form
    {
        cls_ItemCategory cat = new cls_ItemCategory();
        static bool isEditCategory = false;
        static int CatId;

        public DataTable cate;
        public frmB_BackOfzAddCategory()
        {
            InitializeComponent();
        }


        public static void GetEditDetails(bool edit, int CatCode)
        {
            isEditCategory = edit;
            CatId = CatCode;
        }

        public static void GetDeleteId(int CatCode)
        {
            CatId = CatCode;
        }

        private void FrmB_BackOfzAddCategory_Load(object sender, EventArgs e)
        {
            if (isEditCategory)
            {
                loadCategory();
                bttnSave.Text = "Update (F2)";
            }

        }

        private void BttnCancel_Click(object sender, EventArgs e)
        {
            isEditCategory = false;
            this.Close();
        }

        private void BttnClear_Click(object sender, EventArgs e)
        {
            clear();
        }


        //clear text Boxes
        void clear()
        {
            txtCategory.Clear();
            txtDescription.Clear();
            txtCategory.Focus();
            lblMsg.Visible = false;
        }

        private void TxtCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtDescription.Focus();
            }
        }

        private void TxtDescription_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                bttnSave.Focus();
            }
        }


        //Save Category
        private void BttnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // hell Validate TextBoxes
                if (validate())
                {
                    cat.CatCode = txtCategory.Text.Trim();
                    cat.CatDescription = txtDescription.Text.Trim();

                    if (isEditCategory == true)
                    {
                        updateCategory();
                        isEditCategory = false;
                    }

                    else if (isEditCategory == false)
                    {
                        if (cat.CheckDuplicateCategory(txtCategory.Text.Trim(), txtDescription.Text.Trim(), false) is true)
                        {
                            if (cat.SaveCategory() == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Added";
                                clear();
                                bttnSave.Text = "Save (F2)";
                                frmB_BackOfzItemCategory ItemCat = frmB_BackOfzItemCategory.Instance;
                                ItemCat.LoadDataGrid();

                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Category Details";

                        }


                    }


                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }


        //update Category
        public void updateCategory()
        {
            try
            {

                if (validate())
                {
                    cat.CatCode = txtCategory.Text;
                    cat.CatDescription = txtDescription.Text;

                    if (isEditCategory == true)
                    {
                        //Check Count For Update
                        if (cat.CheckDuplicateCategoryUpdate(txtCategory.Text.Trim(), txtDescription.Text.Trim(), isEditCategory) is true)
                        {
                            if (cat.UpdateCategory(CatId) == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Updated";
                                clear();
                                frmB_BackOfzItemCategory ItemCat = frmB_BackOfzItemCategory.Instance;
                                ItemCat.LoadDataGrid();
                                bttnSave.Text = "Save (F2)";
                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Category Details";
                            bttnSave.Text = "Update(F2)";
                            return;
                        }

                    }

                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }



        //validate text Boxes
        public bool validate()
        {
            if (string.IsNullOrEmpty(txtCategory.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Category Code To Continue";
                txtCategory.Focus();
                return false;

            }

            if (string.IsNullOrEmpty(txtDescription.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Description To Continue";
                txtDescription.Focus();
                return false;
            }
            else
            {
                return true;
            }

            return false;
        }




        void loadCategory()
        {
            cate = cat.GetCategory(CatId);
            if (cate.Rows.Count > 0)
            {
                txtCategory.Text = cate.Rows[0]["categoryid"].ToString();
                txtDescription.Text = cate.Rows[0]["description"].ToString();
            }
        }


        void refreshGrid()
        {

        }

        private void FrmB_BackOfzAddCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                BttnSave_Click(sender, e);
            }
        }
    }

}

