﻿namespace SREST
{
    partial class frmB_BackOfzAddSubCat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cmbCategory = new MetroFramework.Controls.MetroComboBox();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtSubCategory = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.lblMsg = new System.Windows.Forms.Label();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(33, 64);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(72, 19);
            this.metroLabel3.TabIndex = 750;
            this.metroLabel3.Text = "Category :";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.ItemHeight = 23;
            this.cmbCategory.Location = new System.Drawing.Point(33, 89);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(397, 29);
            this.cmbCategory.TabIndex = 1;
            this.cmbCategory.UseSelectable = true;
            this.cmbCategory.Click += new System.EventHandler(this.CmbCategory_Click);
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(117, 400);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 4;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // bttnSave
            // 
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(331, 400);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 44);
            this.bttnSave.TabIndex = 6;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            this.bttnSave.Click += new System.EventHandler(this.BttnSave_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(33, 129);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(99, 19);
            this.metroLabel1.TabIndex = 745;
            this.metroLabel1.Text = "Sub Category :";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // txtSubCategory
            // 
            // 
            // 
            // 
            this.txtSubCategory.CustomButton.Image = null;
            this.txtSubCategory.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtSubCategory.CustomButton.Name = "";
            this.txtSubCategory.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSubCategory.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSubCategory.CustomButton.TabIndex = 1;
            this.txtSubCategory.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSubCategory.CustomButton.UseSelectable = true;
            this.txtSubCategory.CustomButton.Visible = false;
            this.txtSubCategory.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtSubCategory.Lines = new string[0];
            this.txtSubCategory.Location = new System.Drawing.Point(33, 155);
            this.txtSubCategory.MaxLength = 32767;
            this.txtSubCategory.Name = "txtSubCategory";
            this.txtSubCategory.PasswordChar = '\0';
            this.txtSubCategory.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSubCategory.SelectedText = "";
            this.txtSubCategory.SelectionLength = 0;
            this.txtSubCategory.SelectionStart = 0;
            this.txtSubCategory.ShortcutsEnabled = true;
            this.txtSubCategory.Size = new System.Drawing.Size(397, 23);
            this.txtSubCategory.TabIndex = 2;
            this.txtSubCategory.UseSelectable = true;
            this.txtSubCategory.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSubCategory.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(33, 190);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(85, 19);
            this.metroLabel2.TabIndex = 746;
            this.metroLabel2.Text = "Description :";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.CustomButton.Image = null;
            this.txtDescription.CustomButton.Location = new System.Drawing.Point(257, 2);
            this.txtDescription.CustomButton.Name = "";
            this.txtDescription.CustomButton.Size = new System.Drawing.Size(137, 137);
            this.txtDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescription.CustomButton.TabIndex = 1;
            this.txtDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescription.CustomButton.UseSelectable = true;
            this.txtDescription.CustomButton.Visible = false;
            this.txtDescription.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtDescription.Lines = new string[0];
            this.txtDescription.Location = new System.Drawing.Point(33, 217);
            this.txtDescription.MaxLength = 32767;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PasswordChar = '\0';
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescription.SelectedText = "";
            this.txtDescription.SelectionLength = 0;
            this.txtDescription.SelectionStart = 0;
            this.txtDescription.ShortcutsEnabled = true;
            this.txtDescription.Size = new System.Drawing.Size(397, 142);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.UseSelectable = true;
            this.txtDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(224, 400);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 5;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            this.bttnClear.Click += new System.EventHandler(this.BttnClear_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(33, 366);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(398, 23);
            this.lblMsg.TabIndex = 747;
            this.lblMsg.Text = "Message";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Sub Category One";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 748;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // frmB_BackOfzAddSubCat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 455);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtSubCategory);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnSave);
            this.Controls.Add(this.bttnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddSubCat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddSubCat";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddSubCat_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cmbCategory;
        private MetroFramework.Controls.MetroButton bttnClose;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtSubCategory;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private MetroFramework.Controls.MetroButton bttnClear;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroPanel metroPanel1;
    }
}