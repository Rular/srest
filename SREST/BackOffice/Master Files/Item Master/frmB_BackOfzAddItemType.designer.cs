﻿namespace SREST
{
    partial class frmB_BackOfzAddItemType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzAddItemType));
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.bttnCancel = new MetroFramework.Controls.MetroButton();
            this.bttnProductItem = new System.Windows.Forms.Button();
            this.bttnRawItem = new System.Windows.Forms.Button();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(391, 46);
            this.metroPanel1.TabIndex = 729;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(391, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Item Type";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // bttnCancel
            // 
            this.bttnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnCancel.Location = new System.Drawing.Point(125, 238);
            this.bttnCancel.Name = "bttnCancel";
            this.bttnCancel.Size = new System.Drawing.Size(135, 44);
            this.bttnCancel.TabIndex = 733;
            this.bttnCancel.Text = "Cancel";
            this.bttnCancel.UseCustomBackColor = true;
            this.bttnCancel.UseCustomForeColor = true;
            this.bttnCancel.UseSelectable = true;
            this.bttnCancel.Click += new System.EventHandler(this.BttnCancel_Click);
            // 
            // bttnProductItem
            // 
            this.bttnProductItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bttnProductItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnProductItem.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnProductItem.FlatAppearance.BorderSize = 0;
            this.bttnProductItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnProductItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnProductItem.ForeColor = System.Drawing.Color.Gray;
            this.bttnProductItem.Image = ((System.Drawing.Image)(resources.GetObject("bttnProductItem.Image")));
            this.bttnProductItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnProductItem.Location = new System.Drawing.Point(0, 130);
            this.bttnProductItem.Name = "bttnProductItem";
            this.bttnProductItem.Size = new System.Drawing.Size(391, 80);
            this.bttnProductItem.TabIndex = 731;
            this.bttnProductItem.TabStop = false;
            this.bttnProductItem.Text = "Product Item";
            this.bttnProductItem.UseVisualStyleBackColor = false;
            this.bttnProductItem.Click += new System.EventHandler(this.BttnProductItem_Click);
            // 
            // bttnRawItem
            // 
            this.bttnRawItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bttnRawItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnRawItem.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnRawItem.FlatAppearance.BorderSize = 0;
            this.bttnRawItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnRawItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnRawItem.ForeColor = System.Drawing.Color.Gray;
            this.bttnRawItem.Image = ((System.Drawing.Image)(resources.GetObject("bttnRawItem.Image")));
            this.bttnRawItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnRawItem.Location = new System.Drawing.Point(0, 48);
            this.bttnRawItem.Name = "bttnRawItem";
            this.bttnRawItem.Size = new System.Drawing.Size(391, 80);
            this.bttnRawItem.TabIndex = 730;
            this.bttnRawItem.TabStop = false;
            this.bttnRawItem.Text = "Raw Item";
            this.bttnRawItem.UseVisualStyleBackColor = false;
            this.bttnRawItem.Click += new System.EventHandler(this.BttnRawItem_Click);
            // 
            // frmB_BackOfzAddItemType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnCancel;
            this.ClientSize = new System.Drawing.Size(391, 301);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.bttnCancel);
            this.Controls.Add(this.bttnProductItem);
            this.Controls.Add(this.bttnRawItem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddItemType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose Item Type";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddItemType_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroButton bttnCancel;
        private System.Windows.Forms.Button bttnProductItem;
        private System.Windows.Forms.Button bttnRawItem;
    }
}