﻿namespace SREST
{
    partial class frmB_BackOfzAddPrinter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.txtPrinterName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.txtPrinterType = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Printers";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // txtPrinterName
            // 
            // 
            // 
            // 
            this.txtPrinterName.CustomButton.Image = null;
            this.txtPrinterName.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtPrinterName.CustomButton.Name = "";
            this.txtPrinterName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrinterName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrinterName.CustomButton.TabIndex = 1;
            this.txtPrinterName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrinterName.CustomButton.UseSelectable = true;
            this.txtPrinterName.CustomButton.Visible = false;
            this.txtPrinterName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtPrinterName.Lines = new string[0];
            this.txtPrinterName.Location = new System.Drawing.Point(36, 164);
            this.txtPrinterName.MaxLength = 32767;
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.PasswordChar = '\0';
            this.txtPrinterName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrinterName.SelectedText = "";
            this.txtPrinterName.SelectionLength = 0;
            this.txtPrinterName.SelectionStart = 0;
            this.txtPrinterName.ShortcutsEnabled = true;
            this.txtPrinterName.Size = new System.Drawing.Size(397, 23);
            this.txtPrinterName.TabIndex = 773;
            this.txtPrinterName.UseSelectable = true;
            this.txtPrinterName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrinterName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(36, 135);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(97, 19);
            this.metroLabel4.TabIndex = 774;
            this.metroLabel4.Text = "Printer Name :";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 771;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(35, 210);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(398, 23);
            this.lblMsg.TabIndex = 770;
            this.lblMsg.Text = "Message";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(183, 236);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 767;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // txtPrinterType
            // 
            // 
            // 
            // 
            this.txtPrinterType.CustomButton.Image = null;
            this.txtPrinterType.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtPrinterType.CustomButton.Name = "";
            this.txtPrinterType.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrinterType.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrinterType.CustomButton.TabIndex = 1;
            this.txtPrinterType.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrinterType.CustomButton.UseSelectable = true;
            this.txtPrinterType.CustomButton.Visible = false;
            this.txtPrinterType.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtPrinterType.Lines = new string[0];
            this.txtPrinterType.Location = new System.Drawing.Point(36, 91);
            this.txtPrinterType.MaxLength = 32767;
            this.txtPrinterType.Name = "txtPrinterType";
            this.txtPrinterType.PasswordChar = '\0';
            this.txtPrinterType.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrinterType.SelectedText = "";
            this.txtPrinterType.SelectionLength = 0;
            this.txtPrinterType.SelectionStart = 0;
            this.txtPrinterType.ShortcutsEnabled = true;
            this.txtPrinterType.Size = new System.Drawing.Size(397, 23);
            this.txtPrinterType.TabIndex = 765;
            this.txtPrinterType.UseSelectable = true;
            this.txtPrinterType.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrinterType.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(36, 63);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(89, 19);
            this.metroLabel1.TabIndex = 769;
            this.metroLabel1.Text = "Printer Type :";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // bttnSave
            // 
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(290, 236);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 44);
            this.bttnSave.TabIndex = 768;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(76, 236);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 766;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            // 
            // frmB_BackOfzAddPrinter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 312);
            this.Controls.Add(this.txtPrinterName);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtPrinterType);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnSave);
            this.Controls.Add(this.bttnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddPrinter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddPrinter";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddPrinter_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroTextBox txtPrinterName;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroTextBox txtPrinterType;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClose;
    }
}