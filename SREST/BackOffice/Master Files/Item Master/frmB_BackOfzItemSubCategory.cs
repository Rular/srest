﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;
namespace SREST
{
    public partial class frmB_BackOfzItemSubCategory : frmC_ModelBlack
    {
        cls_ItemSubCategory subcat = new cls_ItemSubCategory();
        public bool IsEdit = false;
        public int SubCatCode;
        public DataTable cate;
        public frmB_BackOfzItemSubCategory()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzItemSubCategory singleton = null;

        public static frmB_BackOfzItemSubCategory Instance
        {
            get
            {
                if (frmB_BackOfzItemSubCategory.singleton == null)
                {
                    frmB_BackOfzItemSubCategory.singleton = new frmB_BackOfzItemSubCategory();
                }
                return frmB_BackOfzItemSubCategory.singleton;
            }
        }

        private void FrmB_BackOfzItemSubCategory_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void BttnNewCategory_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddSubCat().ShowDialog();
        }




        //Load Grid to Data Funtion
        public void LoadDataGrid()
        {
            DataTable dtSubCategory = subcat.GetAllSubCategory();

            if (dtSubCategory.Columns.Count >= 1)
                dgwSubCategories.DataSource = dtSubCategory;

            dgwSubCategories.Columns[3].Visible = false;


        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemSubCategory.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddSubCat().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnEditCategory_Click(object sender, EventArgs e)
        {
            if (dgwSubCategories.SelectedRows.Count > 0)
            {
                IsEdit = true;

                frmB_BackOfzAddSubCat.GetEditDetails(IsEdit, int.Parse(dgwSubCategories.CurrentRow.Cells[3].FormattedValue.ToString()));
                new frmB_BackOfzAddSubCat().ShowDialog();
            }

            else
                MessageBox.Show("Please Select Full Row To Continue");
        }

        private void BttnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgwSubCategories.SelectedRows.Count > 0)
                {
                    SubCatCode = int.Parse(dgwSubCategories.CurrentRow.Cells[3].FormattedValue.ToString());
                }
                else
                {
                    MessageBox.Show("Please Select Category to Continue");
                    return;
                }


                if (dgwSubCategories.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Are You Sure You Want Delete This SubCategory?", Application.CompanyName + " " + Application.ProductName + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (subcat.deleteSubCategory(SubCatCode) == true)
                        {
                            MessageBox.Show("Success Fully Deleted");
                            frmB_BackOfzItemSubCategory ItemCat = frmB_BackOfzItemSubCategory.Instance;
                            ItemCat.LoadDataGrid();

                        }
                        else
                        {
                            MessageBox.Show("Error");
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Please Select Full Row To Continue");
                }
            }

            catch (Exception EX)
            {

                MessageBox.Show("This SubCategory Use Other Products Unable To Delete");
            }

        }





    }
}
