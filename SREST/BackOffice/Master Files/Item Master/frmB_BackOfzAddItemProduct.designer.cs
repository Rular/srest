﻿namespace SREST
{
    partial class frmB_BackOfzAddItemProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelHeader = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.panelSubHeader = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutSub = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.cmbPrinter = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.toggleEndProduct = new MetroFramework.Controls.MetroToggle();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.cmbCategory = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.cmbSubCategory1 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cmbSubCategory2 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtItemName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtBarcode = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtCostPrice = new MetroFramework.Controls.MetroTextBox();
            this.txtSalesPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cmbUnitGroup = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.cmbUnit = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtReorderLevel = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toggleNotifyExpireDate = new MetroFramework.Controls.MetroToggle();
            this.txtEarlyNotfyDays = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.toggleInventoryItem = new MetroFramework.Controls.MetroToggle();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.toggleActive = new MetroFramework.Controls.MetroToggle();
            this.flowLayoutType = new System.Windows.Forms.FlowLayoutPanel();
            this.rbttnFood = new MetroFramework.Controls.MetroRadioButton();
            this.rbttnBeverage = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.togglePromotionItem = new MetroFramework.Controls.MetroToggle();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.txtItemNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.flowLayoutTax = new System.Windows.Forms.FlowLayoutPanel();
            this.chkServiceCharge = new MetroFramework.Controls.MetroCheckBox();
            this.chkTax = new MetroFramework.Controls.MetroCheckBox();
            this.chkVat = new MetroFramework.Controls.MetroCheckBox();
            this.tableLayoutPanelBttns = new System.Windows.Forms.TableLayoutPanel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.lblMsg = new System.Windows.Forms.Label();
            this.tableLayoutPanelPic = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelSubPic = new System.Windows.Forms.TableLayoutPanel();
            this.PicBoxItem = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.bttncapture = new MetroFramework.Controls.MetroButton();
            this.bttnstart = new MetroFramework.Controls.MetroButton();
            this.cmbcamlist = new System.Windows.Forms.ComboBox();
            this.PanelHeader.SuspendLayout();
            this.tableLayoutMain.SuspendLayout();
            this.tableLayoutSub.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutType.SuspendLayout();
            this.flowLayoutTax.SuspendLayout();
            this.tableLayoutPanelBttns.SuspendLayout();
            this.tableLayoutPanelPic.SuspendLayout();
            this.tableLayoutPanelSubPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelHeader
            // 
            this.PanelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.PanelHeader.Controls.Add(this.lblTitle);
            this.PanelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelHeader.Enabled = false;
            this.PanelHeader.HorizontalScrollbarBarColor = true;
            this.PanelHeader.HorizontalScrollbarHighlightOnWheel = false;
            this.PanelHeader.HorizontalScrollbarSize = 10;
            this.PanelHeader.Location = new System.Drawing.Point(0, 0);
            this.PanelHeader.Name = "PanelHeader";
            this.PanelHeader.Size = new System.Drawing.Size(943, 45);
            this.PanelHeader.TabIndex = 731;
            this.PanelHeader.UseCustomBackColor = true;
            this.PanelHeader.VerticalScrollbarBarColor = true;
            this.PanelHeader.VerticalScrollbarHighlightOnWheel = false;
            this.PanelHeader.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(943, 45);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Product Item";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // panelSubHeader
            // 
            this.panelSubHeader.AutoScroll = true;
            this.panelSubHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubHeader.HorizontalScrollbar = true;
            this.panelSubHeader.HorizontalScrollbarBarColor = true;
            this.panelSubHeader.HorizontalScrollbarHighlightOnWheel = false;
            this.panelSubHeader.HorizontalScrollbarSize = 10;
            this.panelSubHeader.Location = new System.Drawing.Point(0, 45);
            this.panelSubHeader.Name = "panelSubHeader";
            this.panelSubHeader.Size = new System.Drawing.Size(943, 32);
            this.panelSubHeader.TabIndex = 732;
            this.panelSubHeader.UseCustomBackColor = true;
            this.panelSubHeader.VerticalScrollbar = true;
            this.panelSubHeader.VerticalScrollbarBarColor = true;
            this.panelSubHeader.VerticalScrollbarHighlightOnWheel = false;
            this.panelSubHeader.VerticalScrollbarSize = 10;
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.AutoScroll = true;
            this.tableLayoutMain.ColumnCount = 2;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutSub, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutPanelPic, 1, 0);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 77);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 1;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutMain.Size = new System.Drawing.Size(943, 523);
            this.tableLayoutMain.TabIndex = 733;
            // 
            // tableLayoutSub
            // 
            this.tableLayoutSub.AutoScroll = true;
            this.tableLayoutSub.ColumnCount = 2;
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.54506F));
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.45494F));
            this.tableLayoutSub.Controls.Add(this.metroLabel19, 0, 8);
            this.tableLayoutSub.Controls.Add(this.metroLabel17, 0, 12);
            this.tableLayoutSub.Controls.Add(this.cmbPrinter, 1, 11);
            this.tableLayoutSub.Controls.Add(this.metroLabel16, 0, 11);
            this.tableLayoutSub.Controls.Add(this.toggleEndProduct, 1, 4);
            this.tableLayoutSub.Controls.Add(this.metroLabel15, 0, 4);
            this.tableLayoutSub.Controls.Add(this.cmbCategory, 1, 0);
            this.tableLayoutSub.Controls.Add(this.metroLabel1, 0, 1);
            this.tableLayoutSub.Controls.Add(this.cmbSubCategory1, 1, 1);
            this.tableLayoutSub.Controls.Add(this.metroLabel2, 0, 2);
            this.tableLayoutSub.Controls.Add(this.cmbSubCategory2, 1, 2);
            this.tableLayoutSub.Controls.Add(this.metroLabel3, 0, 3);
            this.tableLayoutSub.Controls.Add(this.txtItemName, 1, 3);
            this.tableLayoutSub.Controls.Add(this.metroLabel4, 0, 5);
            this.tableLayoutSub.Controls.Add(this.txtBarcode, 1, 5);
            this.tableLayoutSub.Controls.Add(this.metroLabel5, 0, 6);
            this.tableLayoutSub.Controls.Add(this.txtCostPrice, 1, 6);
            this.tableLayoutSub.Controls.Add(this.txtSalesPrice, 1, 7);
            this.tableLayoutSub.Controls.Add(this.metroLabel6, 0, 7);
            this.tableLayoutSub.Controls.Add(this.metroLabel7, 0, 9);
            this.tableLayoutSub.Controls.Add(this.cmbUnitGroup, 1, 9);
            this.tableLayoutSub.Controls.Add(this.metroLabel8, 0, 10);
            this.tableLayoutSub.Controls.Add(this.cmbUnit, 1, 10);
            this.tableLayoutSub.Controls.Add(this.metroLabel9, 0, 13);
            this.tableLayoutSub.Controls.Add(this.txtReorderLevel, 1, 13);
            this.tableLayoutSub.Controls.Add(this.metroLabel10, 0, 14);
            this.tableLayoutSub.Controls.Add(this.tableLayoutPanel1, 1, 14);
            this.tableLayoutSub.Controls.Add(this.metroLabel11, 0, 15);
            this.tableLayoutSub.Controls.Add(this.toggleInventoryItem, 1, 15);
            this.tableLayoutSub.Controls.Add(this.metroLabel12, 0, 16);
            this.tableLayoutSub.Controls.Add(this.toggleActive, 1, 16);
            this.tableLayoutSub.Controls.Add(this.flowLayoutType, 1, 12);
            this.tableLayoutSub.Controls.Add(this.metroLabel13, 0, 17);
            this.tableLayoutSub.Controls.Add(this.togglePromotionItem, 1, 17);
            this.tableLayoutSub.Controls.Add(this.metroLabel18, 0, 18);
            this.tableLayoutSub.Controls.Add(this.txtItemNumber, 1, 18);
            this.tableLayoutSub.Controls.Add(this.metroLabel14, 0, 0);
            this.tableLayoutSub.Controls.Add(this.flowLayoutTax, 1, 8);
            this.tableLayoutSub.Controls.Add(this.tableLayoutPanelBttns, 1, 19);
            this.tableLayoutSub.Controls.Add(this.lblMsg, 1, 20);
            this.tableLayoutSub.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutSub.Name = "tableLayoutSub";
            this.tableLayoutSub.RowCount = 22;
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.480061F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.479059F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.481993F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.756098F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.829268F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.926829F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.56962F));
            this.tableLayoutSub.Size = new System.Drawing.Size(545, 820);
            this.tableLayoutSub.TabIndex = 0;
            // 
            // metroLabel19
            // 
            this.metroLabel19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel19.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel19.Location = new System.Drawing.Point(3, 296);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(27, 19);
            this.metroLabel19.TabIndex = 757;
            this.metroLabel19.Text = "Tax";
            this.metroLabel19.UseCustomBackColor = true;
            this.metroLabel19.UseCustomForeColor = true;
            // 
            // metroLabel17
            // 
            this.metroLabel17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel17.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel17.Location = new System.Drawing.Point(3, 440);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(37, 19);
            this.metroLabel17.TabIndex = 762;
            this.metroLabel17.Text = "Type";
            this.metroLabel17.UseCustomBackColor = true;
            this.metroLabel17.UseCustomForeColor = true;
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.ItemHeight = 23;
            this.cmbPrinter.Location = new System.Drawing.Point(174, 399);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(368, 29);
            this.cmbPrinter.TabIndex = 762;
            this.cmbPrinter.UseSelectable = true;
            // 
            // metroLabel16
            // 
            this.metroLabel16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel16.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel16.Location = new System.Drawing.Point(3, 404);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(50, 19);
            this.metroLabel16.TabIndex = 761;
            this.metroLabel16.Text = "Printer";
            this.metroLabel16.UseCustomBackColor = true;
            this.metroLabel16.UseCustomForeColor = true;
            // 
            // toggleEndProduct
            // 
            this.toggleEndProduct.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleEndProduct.AutoSize = true;
            this.toggleEndProduct.Location = new System.Drawing.Point(174, 153);
            this.toggleEndProduct.Name = "toggleEndProduct";
            this.toggleEndProduct.Size = new System.Drawing.Size(80, 17);
            this.toggleEndProduct.TabIndex = 69;
            this.toggleEndProduct.Text = "Off";
            this.toggleEndProduct.UseCustomBackColor = true;
            this.toggleEndProduct.UseSelectable = true;
            // 
            // metroLabel15
            // 
            this.metroLabel15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel15.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel15.Location = new System.Drawing.Point(3, 152);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(84, 19);
            this.metroLabel15.TabIndex = 751;
            this.metroLabel15.Text = "End Product";
            this.metroLabel15.UseCustomBackColor = true;
            this.metroLabel15.UseCustomForeColor = true;
            // 
            // cmbCategory
            // 
            this.cmbCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.ItemHeight = 23;
            this.cmbCategory.Location = new System.Drawing.Point(174, 3);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(368, 29);
            this.cmbCategory.TabIndex = 748;
            this.cmbCategory.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(3, 44);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(96, 19);
            this.metroLabel1.TabIndex = 746;
            this.metroLabel1.Text = "Sub Category ";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // cmbSubCategory1
            // 
            this.cmbSubCategory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSubCategory1.FormattingEnabled = true;
            this.cmbSubCategory1.ItemHeight = 23;
            this.cmbSubCategory1.Location = new System.Drawing.Point(174, 39);
            this.cmbSubCategory1.Name = "cmbSubCategory1";
            this.cmbSubCategory1.Size = new System.Drawing.Size(368, 29);
            this.cmbSubCategory1.TabIndex = 747;
            this.cmbSubCategory1.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(3, 80);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(112, 19);
            this.metroLabel2.TabIndex = 748;
            this.metroLabel2.Text = "Sub Category  2 ";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // cmbSubCategory2
            // 
            this.cmbSubCategory2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSubCategory2.FormattingEnabled = true;
            this.cmbSubCategory2.ItemHeight = 23;
            this.cmbSubCategory2.Location = new System.Drawing.Point(174, 75);
            this.cmbSubCategory2.Name = "cmbSubCategory2";
            this.cmbSubCategory2.Size = new System.Drawing.Size(368, 29);
            this.cmbSubCategory2.TabIndex = 749;
            this.cmbSubCategory2.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(3, 116);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(81, 19);
            this.metroLabel3.TabIndex = 750;
            this.metroLabel3.Text = "Item Name ";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // txtItemName
            // 
            // 
            // 
            // 
            this.txtItemName.CustomButton.Image = null;
            this.txtItemName.CustomButton.Location = new System.Drawing.Point(340, 2);
            this.txtItemName.CustomButton.Name = "";
            this.txtItemName.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtItemName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtItemName.CustomButton.TabIndex = 1;
            this.txtItemName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtItemName.CustomButton.UseSelectable = true;
            this.txtItemName.CustomButton.Visible = false;
            this.txtItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtItemName.Lines = new string[0];
            this.txtItemName.Location = new System.Drawing.Point(174, 111);
            this.txtItemName.MaxLength = 32767;
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.PasswordChar = '\0';
            this.txtItemName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtItemName.SelectedText = "";
            this.txtItemName.SelectionLength = 0;
            this.txtItemName.SelectionStart = 0;
            this.txtItemName.ShortcutsEnabled = true;
            this.txtItemName.Size = new System.Drawing.Size(368, 30);
            this.txtItemName.TabIndex = 751;
            this.txtItemName.UseSelectable = true;
            this.txtItemName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtItemName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(3, 188);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(62, 19);
            this.metroLabel4.TabIndex = 752;
            this.metroLabel4.Text = "Barcode ";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // txtBarcode
            // 
            // 
            // 
            // 
            this.txtBarcode.CustomButton.Image = null;
            this.txtBarcode.CustomButton.Location = new System.Drawing.Point(340, 2);
            this.txtBarcode.CustomButton.Name = "";
            this.txtBarcode.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtBarcode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBarcode.CustomButton.TabIndex = 1;
            this.txtBarcode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBarcode.CustomButton.UseSelectable = true;
            this.txtBarcode.CustomButton.Visible = false;
            this.txtBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBarcode.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtBarcode.Lines = new string[0];
            this.txtBarcode.Location = new System.Drawing.Point(174, 183);
            this.txtBarcode.MaxLength = 32767;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.PasswordChar = '\0';
            this.txtBarcode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBarcode.SelectedText = "";
            this.txtBarcode.SelectionLength = 0;
            this.txtBarcode.SelectionStart = 0;
            this.txtBarcode.ShortcutsEnabled = true;
            this.txtBarcode.Size = new System.Drawing.Size(368, 30);
            this.txtBarcode.TabIndex = 753;
            this.txtBarcode.UseSelectable = true;
            this.txtBarcode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBarcode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel5.Location = new System.Drawing.Point(3, 224);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(70, 19);
            this.metroLabel5.TabIndex = 754;
            this.metroLabel5.Text = "Cost Price";
            this.metroLabel5.UseCustomBackColor = true;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // txtCostPrice
            // 
            // 
            // 
            // 
            this.txtCostPrice.CustomButton.Image = null;
            this.txtCostPrice.CustomButton.Location = new System.Drawing.Point(340, 2);
            this.txtCostPrice.CustomButton.Name = "";
            this.txtCostPrice.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtCostPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCostPrice.CustomButton.TabIndex = 1;
            this.txtCostPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCostPrice.CustomButton.UseSelectable = true;
            this.txtCostPrice.CustomButton.Visible = false;
            this.txtCostPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCostPrice.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtCostPrice.Lines = new string[0];
            this.txtCostPrice.Location = new System.Drawing.Point(174, 219);
            this.txtCostPrice.MaxLength = 32767;
            this.txtCostPrice.Name = "txtCostPrice";
            this.txtCostPrice.PasswordChar = '\0';
            this.txtCostPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCostPrice.SelectedText = "";
            this.txtCostPrice.SelectionLength = 0;
            this.txtCostPrice.SelectionStart = 0;
            this.txtCostPrice.ShortcutsEnabled = true;
            this.txtCostPrice.Size = new System.Drawing.Size(368, 30);
            this.txtCostPrice.TabIndex = 755;
            this.txtCostPrice.UseSelectable = true;
            this.txtCostPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCostPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtSalesPrice
            // 
            // 
            // 
            // 
            this.txtSalesPrice.CustomButton.Image = null;
            this.txtSalesPrice.CustomButton.Location = new System.Drawing.Point(340, 2);
            this.txtSalesPrice.CustomButton.Name = "";
            this.txtSalesPrice.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtSalesPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSalesPrice.CustomButton.TabIndex = 1;
            this.txtSalesPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSalesPrice.CustomButton.UseSelectable = true;
            this.txtSalesPrice.CustomButton.Visible = false;
            this.txtSalesPrice.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtSalesPrice.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtSalesPrice.Lines = new string[0];
            this.txtSalesPrice.Location = new System.Drawing.Point(174, 255);
            this.txtSalesPrice.MaxLength = 32767;
            this.txtSalesPrice.Name = "txtSalesPrice";
            this.txtSalesPrice.PasswordChar = '\0';
            this.txtSalesPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSalesPrice.SelectedText = "";
            this.txtSalesPrice.SelectionLength = 0;
            this.txtSalesPrice.SelectionStart = 0;
            this.txtSalesPrice.ShortcutsEnabled = true;
            this.txtSalesPrice.Size = new System.Drawing.Size(368, 30);
            this.txtSalesPrice.TabIndex = 757;
            this.txtSalesPrice.UseSelectable = true;
            this.txtSalesPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSalesPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel6.Location = new System.Drawing.Point(3, 260);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(72, 19);
            this.metroLabel6.TabIndex = 756;
            this.metroLabel6.Text = "Sales Price";
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel7.Location = new System.Drawing.Point(3, 332);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(78, 19);
            this.metroLabel7.TabIndex = 758;
            this.metroLabel7.Text = "Unit Group";
            this.metroLabel7.UseCustomBackColor = true;
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // cmbUnitGroup
            // 
            this.cmbUnitGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUnitGroup.FormattingEnabled = true;
            this.cmbUnitGroup.ItemHeight = 23;
            this.cmbUnitGroup.Location = new System.Drawing.Point(174, 327);
            this.cmbUnitGroup.Name = "cmbUnitGroup";
            this.cmbUnitGroup.Size = new System.Drawing.Size(368, 29);
            this.cmbUnitGroup.TabIndex = 759;
            this.cmbUnitGroup.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel8.Location = new System.Drawing.Point(3, 368);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(35, 19);
            this.metroLabel8.TabIndex = 760;
            this.metroLabel8.Text = "Unit";
            this.metroLabel8.UseCustomBackColor = true;
            this.metroLabel8.UseCustomForeColor = true;
            // 
            // cmbUnit
            // 
            this.cmbUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.ItemHeight = 23;
            this.cmbUnit.Location = new System.Drawing.Point(174, 363);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(368, 29);
            this.cmbUnit.TabIndex = 761;
            this.cmbUnit.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel9.Location = new System.Drawing.Point(3, 476);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(92, 19);
            this.metroLabel9.TabIndex = 762;
            this.metroLabel9.Text = "Reorder Level";
            this.metroLabel9.UseCustomBackColor = true;
            this.metroLabel9.UseCustomForeColor = true;
            // 
            // txtReorderLevel
            // 
            // 
            // 
            // 
            this.txtReorderLevel.CustomButton.Image = null;
            this.txtReorderLevel.CustomButton.Location = new System.Drawing.Point(340, 2);
            this.txtReorderLevel.CustomButton.Name = "";
            this.txtReorderLevel.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtReorderLevel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtReorderLevel.CustomButton.TabIndex = 1;
            this.txtReorderLevel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtReorderLevel.CustomButton.UseSelectable = true;
            this.txtReorderLevel.CustomButton.Visible = false;
            this.txtReorderLevel.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtReorderLevel.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtReorderLevel.Lines = new string[0];
            this.txtReorderLevel.Location = new System.Drawing.Point(174, 471);
            this.txtReorderLevel.MaxLength = 32767;
            this.txtReorderLevel.Name = "txtReorderLevel";
            this.txtReorderLevel.PasswordChar = '\0';
            this.txtReorderLevel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReorderLevel.SelectedText = "";
            this.txtReorderLevel.SelectionLength = 0;
            this.txtReorderLevel.SelectionStart = 0;
            this.txtReorderLevel.ShortcutsEnabled = true;
            this.txtReorderLevel.Size = new System.Drawing.Size(368, 30);
            this.txtReorderLevel.TabIndex = 763;
            this.txtReorderLevel.UseSelectable = true;
            this.txtReorderLevel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtReorderLevel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel10.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel10.Location = new System.Drawing.Point(3, 512);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(119, 19);
            this.metroLabel10.TabIndex = 764;
            this.metroLabel10.Text = "Notify Expire Date";
            this.metroLabel10.UseCustomBackColor = true;
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.90217F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.09782F));
            this.tableLayoutPanel1.Controls.Add(this.toggleNotifyExpireDate, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtEarlyNotfyDays, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(174, 507);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(368, 30);
            this.tableLayoutPanel1.TabIndex = 765;
            // 
            // toggleNotifyExpireDate
            // 
            this.toggleNotifyExpireDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleNotifyExpireDate.AutoSize = true;
            this.toggleNotifyExpireDate.Location = new System.Drawing.Point(3, 6);
            this.toggleNotifyExpireDate.Name = "toggleNotifyExpireDate";
            this.toggleNotifyExpireDate.Size = new System.Drawing.Size(80, 17);
            this.toggleNotifyExpireDate.TabIndex = 0;
            this.toggleNotifyExpireDate.Text = "Off";
            this.toggleNotifyExpireDate.UseCustomBackColor = true;
            this.toggleNotifyExpireDate.UseSelectable = true;
            // 
            // txtEarlyNotfyDays
            // 
            // 
            // 
            // 
            this.txtEarlyNotfyDays.CustomButton.Image = null;
            this.txtEarlyNotfyDays.CustomButton.Location = new System.Drawing.Point(98, 2);
            this.txtEarlyNotfyDays.CustomButton.Name = "";
            this.txtEarlyNotfyDays.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txtEarlyNotfyDays.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEarlyNotfyDays.CustomButton.TabIndex = 1;
            this.txtEarlyNotfyDays.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEarlyNotfyDays.CustomButton.UseSelectable = true;
            this.txtEarlyNotfyDays.CustomButton.Visible = false;
            this.txtEarlyNotfyDays.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtEarlyNotfyDays.Lines = new string[0];
            this.txtEarlyNotfyDays.Location = new System.Drawing.Point(102, 3);
            this.txtEarlyNotfyDays.MaxLength = 32767;
            this.txtEarlyNotfyDays.Name = "txtEarlyNotfyDays";
            this.txtEarlyNotfyDays.PasswordChar = '\0';
            this.txtEarlyNotfyDays.PromptText = "Early Notify Days";
            this.txtEarlyNotfyDays.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEarlyNotfyDays.SelectedText = "";
            this.txtEarlyNotfyDays.SelectionLength = 0;
            this.txtEarlyNotfyDays.SelectionStart = 0;
            this.txtEarlyNotfyDays.ShortcutsEnabled = true;
            this.txtEarlyNotfyDays.Size = new System.Drawing.Size(120, 24);
            this.txtEarlyNotfyDays.TabIndex = 766;
            this.txtEarlyNotfyDays.UseSelectable = true;
            this.txtEarlyNotfyDays.WaterMark = "Early Notify Days";
            this.txtEarlyNotfyDays.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEarlyNotfyDays.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel11.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel11.Location = new System.Drawing.Point(3, 548);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(100, 19);
            this.metroLabel11.TabIndex = 766;
            this.metroLabel11.Text = "Inventory Item";
            this.metroLabel11.UseCustomBackColor = true;
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // toggleInventoryItem
            // 
            this.toggleInventoryItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleInventoryItem.AutoSize = true;
            this.toggleInventoryItem.Location = new System.Drawing.Point(174, 549);
            this.toggleInventoryItem.Name = "toggleInventoryItem";
            this.toggleInventoryItem.Size = new System.Drawing.Size(80, 17);
            this.toggleInventoryItem.TabIndex = 767;
            this.toggleInventoryItem.Text = "Off";
            this.toggleInventoryItem.UseCustomBackColor = true;
            this.toggleInventoryItem.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel12.Location = new System.Drawing.Point(3, 584);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(46, 19);
            this.metroLabel12.TabIndex = 768;
            this.metroLabel12.Text = "Active";
            this.metroLabel12.UseCustomBackColor = true;
            this.metroLabel12.UseCustomForeColor = true;
            // 
            // toggleActive
            // 
            this.toggleActive.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleActive.AutoSize = true;
            this.toggleActive.Location = new System.Drawing.Point(174, 585);
            this.toggleActive.Name = "toggleActive";
            this.toggleActive.Size = new System.Drawing.Size(80, 17);
            this.toggleActive.TabIndex = 769;
            this.toggleActive.Text = "Off";
            this.toggleActive.UseCustomBackColor = true;
            this.toggleActive.UseSelectable = true;
            // 
            // flowLayoutType
            // 
            this.flowLayoutType.Controls.Add(this.rbttnFood);
            this.flowLayoutType.Controls.Add(this.rbttnBeverage);
            this.flowLayoutType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutType.Location = new System.Drawing.Point(174, 435);
            this.flowLayoutType.Name = "flowLayoutType";
            this.flowLayoutType.Size = new System.Drawing.Size(368, 30);
            this.flowLayoutType.TabIndex = 776;
            // 
            // rbttnFood
            // 
            this.rbttnFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rbttnFood.AutoSize = true;
            this.rbttnFood.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbttnFood.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.rbttnFood.Location = new System.Drawing.Point(3, 3);
            this.rbttnFood.Name = "rbttnFood";
            this.rbttnFood.Size = new System.Drawing.Size(64, 19);
            this.rbttnFood.TabIndex = 1;
            this.rbttnFood.Text = "FOOD";
            this.rbttnFood.UseCustomBackColor = true;
            this.rbttnFood.UseSelectable = true;
            // 
            // rbttnBeverage
            // 
            this.rbttnBeverage.AutoSize = true;
            this.rbttnBeverage.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.rbttnBeverage.Location = new System.Drawing.Point(73, 3);
            this.rbttnBeverage.Name = "rbttnBeverage";
            this.rbttnBeverage.Size = new System.Drawing.Size(90, 19);
            this.rbttnBeverage.TabIndex = 0;
            this.rbttnBeverage.Text = "BEVERAGE";
            this.rbttnBeverage.UseCustomBackColor = true;
            this.rbttnBeverage.UseSelectable = true;
            // 
            // metroLabel13
            // 
            this.metroLabel13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel13.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel13.Location = new System.Drawing.Point(3, 620);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(106, 19);
            this.metroLabel13.TabIndex = 777;
            this.metroLabel13.Text = "Promotion Item";
            this.metroLabel13.UseCustomBackColor = true;
            this.metroLabel13.UseCustomForeColor = true;
            // 
            // togglePromotionItem
            // 
            this.togglePromotionItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.togglePromotionItem.AutoSize = true;
            this.togglePromotionItem.Location = new System.Drawing.Point(174, 621);
            this.togglePromotionItem.Name = "togglePromotionItem";
            this.togglePromotionItem.Size = new System.Drawing.Size(80, 17);
            this.togglePromotionItem.TabIndex = 778;
            this.togglePromotionItem.Text = "Off";
            this.togglePromotionItem.UseCustomBackColor = true;
            this.togglePromotionItem.UseSelectable = true;
            // 
            // metroLabel18
            // 
            this.metroLabel18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel18.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel18.Location = new System.Drawing.Point(3, 657);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(91, 19);
            this.metroLabel18.TabIndex = 780;
            this.metroLabel18.Text = "Item Number";
            this.metroLabel18.UseCustomBackColor = true;
            this.metroLabel18.UseCustomForeColor = true;
            // 
            // txtItemNumber
            // 
            // 
            // 
            // 
            this.txtItemNumber.CustomButton.Image = null;
            this.txtItemNumber.CustomButton.Location = new System.Drawing.Point(338, 2);
            this.txtItemNumber.CustomButton.Name = "";
            this.txtItemNumber.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtItemNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtItemNumber.CustomButton.TabIndex = 1;
            this.txtItemNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtItemNumber.CustomButton.UseSelectable = true;
            this.txtItemNumber.CustomButton.Visible = false;
            this.txtItemNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemNumber.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtItemNumber.Lines = new string[0];
            this.txtItemNumber.Location = new System.Drawing.Point(174, 651);
            this.txtItemNumber.MaxLength = 32767;
            this.txtItemNumber.Name = "txtItemNumber";
            this.txtItemNumber.PasswordChar = '\0';
            this.txtItemNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtItemNumber.SelectedText = "";
            this.txtItemNumber.SelectionLength = 0;
            this.txtItemNumber.SelectionStart = 0;
            this.txtItemNumber.ShortcutsEnabled = true;
            this.txtItemNumber.Size = new System.Drawing.Size(368, 32);
            this.txtItemNumber.TabIndex = 781;
            this.txtItemNumber.UseSelectable = true;
            this.txtItemNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtItemNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel14
            // 
            this.metroLabel14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel14.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel14.Location = new System.Drawing.Point(3, 8);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(69, 19);
            this.metroLabel14.TabIndex = 775;
            this.metroLabel14.Text = "Category ";
            this.metroLabel14.UseCustomBackColor = true;
            this.metroLabel14.UseCustomForeColor = true;
            // 
            // flowLayoutTax
            // 
            this.flowLayoutTax.Controls.Add(this.chkServiceCharge);
            this.flowLayoutTax.Controls.Add(this.chkTax);
            this.flowLayoutTax.Controls.Add(this.chkVat);
            this.flowLayoutTax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutTax.Location = new System.Drawing.Point(174, 291);
            this.flowLayoutTax.Name = "flowLayoutTax";
            this.flowLayoutTax.Size = new System.Drawing.Size(368, 30);
            this.flowLayoutTax.TabIndex = 782;
            // 
            // chkServiceCharge
            // 
            this.chkServiceCharge.AutoSize = true;
            this.chkServiceCharge.Location = new System.Drawing.Point(3, 3);
            this.chkServiceCharge.Name = "chkServiceCharge";
            this.chkServiceCharge.Size = new System.Drawing.Size(101, 15);
            this.chkServiceCharge.TabIndex = 0;
            this.chkServiceCharge.Text = "Service Charge";
            this.chkServiceCharge.UseCustomBackColor = true;
            this.chkServiceCharge.UseSelectable = true;
            // 
            // chkTax
            // 
            this.chkTax.AutoSize = true;
            this.chkTax.Location = new System.Drawing.Point(110, 3);
            this.chkTax.Name = "chkTax";
            this.chkTax.Size = new System.Drawing.Size(44, 15);
            this.chkTax.TabIndex = 1;
            this.chkTax.Text = "TAX";
            this.chkTax.UseCustomBackColor = true;
            this.chkTax.UseSelectable = true;
            // 
            // chkVat
            // 
            this.chkVat.AutoSize = true;
            this.chkVat.Location = new System.Drawing.Point(160, 3);
            this.chkVat.Name = "chkVat";
            this.chkVat.Size = new System.Drawing.Size(43, 15);
            this.chkVat.TabIndex = 2;
            this.chkVat.Text = "VAT";
            this.chkVat.UseCustomBackColor = true;
            this.chkVat.UseSelectable = true;
            // 
            // tableLayoutPanelBttns
            // 
            this.tableLayoutPanelBttns.AutoScroll = true;
            this.tableLayoutPanelBttns.ColumnCount = 3;
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.Controls.Add(this.bttnSave, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClear, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClose, 0, 0);
            this.tableLayoutPanelBttns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBttns.Location = new System.Drawing.Point(174, 689);
            this.tableLayoutPanelBttns.Name = "tableLayoutPanelBttns";
            this.tableLayoutPanelBttns.RowCount = 1;
            this.tableLayoutPanelBttns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBttns.Size = new System.Drawing.Size(368, 49);
            this.tableLayoutPanelBttns.TabIndex = 772;
            // 
            // bttnSave
            // 
            this.bttnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(221, 3);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 43);
            this.bttnSave.TabIndex = 7;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            // 
            // bttnClear
            // 
            this.bttnClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(112, 3);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 43);
            this.bttnClear.TabIndex = 6;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(3, 3);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 43);
            this.bttnClose.TabIndex = 5;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(174, 741);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(368, 23);
            this.lblMsg.TabIndex = 779;
            this.lblMsg.Text = "Messege";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // tableLayoutPanelPic
            // 
            this.tableLayoutPanelPic.AutoScroll = true;
            this.tableLayoutPanelPic.ColumnCount = 1;
            this.tableLayoutPanelPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelPic.Controls.Add(this.tableLayoutPanelSubPic, 0, 0);
            this.tableLayoutPanelPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelPic.Location = new System.Drawing.Point(554, 3);
            this.tableLayoutPanelPic.Name = "tableLayoutPanelPic";
            this.tableLayoutPanelPic.RowCount = 2;
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.41958F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.58042F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.Size = new System.Drawing.Size(386, 720);
            this.tableLayoutPanelPic.TabIndex = 1;
            // 
            // tableLayoutPanelSubPic
            // 
            this.tableLayoutPanelSubPic.AutoScroll = true;
            this.tableLayoutPanelSubPic.ColumnCount = 3;
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.57627F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.42373F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanelSubPic.Controls.Add(this.PicBoxItem, 1, 0);
            this.tableLayoutPanelSubPic.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanelSubPic.Controls.Add(this.cmbcamlist, 1, 2);
            this.tableLayoutPanelSubPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelSubPic.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelSubPic.Name = "tableLayoutPanelSubPic";
            this.tableLayoutPanelSubPic.RowCount = 4;
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 224F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSubPic.Size = new System.Drawing.Size(380, 503);
            this.tableLayoutPanelSubPic.TabIndex = 0;
            // 
            // PicBoxItem
            // 
            this.PicBoxItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PicBoxItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBoxItem.Location = new System.Drawing.Point(109, 3);
            this.PicBoxItem.Name = "PicBoxItem";
            this.PicBoxItem.Size = new System.Drawing.Size(196, 204);
            this.PicBoxItem.TabIndex = 11;
            this.PicBoxItem.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoScroll = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.bttncapture, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.bttnstart, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(109, 213);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(196, 33);
            this.tableLayoutPanel2.TabIndex = 12;
            // 
            // bttncapture
            // 
            this.bttncapture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttncapture.Location = new System.Drawing.Point(101, 3);
            this.bttncapture.Name = "bttncapture";
            this.bttncapture.Size = new System.Drawing.Size(92, 27);
            this.bttncapture.TabIndex = 67;
            this.bttncapture.Text = "Capture";
            this.bttncapture.UseSelectable = true;
            // 
            // bttnstart
            // 
            this.bttnstart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnstart.Location = new System.Drawing.Point(3, 3);
            this.bttnstart.Name = "bttnstart";
            this.bttnstart.Size = new System.Drawing.Size(92, 27);
            this.bttnstart.TabIndex = 66;
            this.bttnstart.Text = "Start";
            this.bttnstart.UseSelectable = true;
            // 
            // cmbcamlist
            // 
            this.cmbcamlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbcamlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcamlist.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcamlist.FormattingEnabled = true;
            this.cmbcamlist.ItemHeight = 18;
            this.cmbcamlist.Location = new System.Drawing.Point(109, 252);
            this.cmbcamlist.Name = "cmbcamlist";
            this.cmbcamlist.Size = new System.Drawing.Size(196, 26);
            this.cmbcamlist.TabIndex = 69;
            // 
            // frmB_BackOfzAddItemProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnClose;
            this.ClientSize = new System.Drawing.Size(943, 600);
            this.Controls.Add(this.tableLayoutMain);
            this.Controls.Add(this.panelSubHeader);
            this.Controls.Add(this.PanelHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddItemProduct";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Product";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddItemProduct_Load);
            this.PanelHeader.ResumeLayout(false);
            this.tableLayoutMain.ResumeLayout(false);
            this.tableLayoutSub.ResumeLayout(false);
            this.tableLayoutSub.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutType.ResumeLayout(false);
            this.flowLayoutType.PerformLayout();
            this.flowLayoutTax.ResumeLayout(false);
            this.flowLayoutTax.PerformLayout();
            this.tableLayoutPanelBttns.ResumeLayout(false);
            this.tableLayoutPanelPic.ResumeLayout(false);
            this.tableLayoutPanelSubPic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel PanelHeader;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroPanel panelSubHeader;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSub;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cmbSubCategory1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox cmbSubCategory2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtItemName;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtBarcode;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtCostPrice;
        private MetroFramework.Controls.MetroTextBox txtSalesPrice;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroComboBox cmbUnitGroup;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox cmbUnit;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox txtReorderLevel;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroToggle toggleNotifyExpireDate;
        private MetroFramework.Controls.MetroTextBox txtEarlyNotfyDays;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroToggle toggleInventoryItem;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroToggle toggleActive;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBttns;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroButton bttnClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPic;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSubPic;
        internal System.Windows.Forms.PictureBox PicBoxItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton bttncapture;
        private MetroFramework.Controls.MetroButton bttnstart;
        private MetroFramework.Controls.MetroComboBox cmbCategory;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroComboBox cmbPrinter;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroToggle toggleEndProduct;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutType;
        private MetroFramework.Controls.MetroRadioButton rbttnBeverage;
        private MetroFramework.Controls.MetroRadioButton rbttnFood;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroToggle togglePromotionItem;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroTextBox txtItemNumber;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutTax;
        private MetroFramework.Controls.MetroCheckBox chkServiceCharge;
        private MetroFramework.Controls.MetroCheckBox chkTax;
        private MetroFramework.Controls.MetroCheckBox chkVat;
        private System.Windows.Forms.ComboBox cmbcamlist;
    }
}