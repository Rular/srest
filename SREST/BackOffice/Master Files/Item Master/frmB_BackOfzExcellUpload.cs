﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzExcellUpload : Form
    {
        public frmB_BackOfzExcellUpload()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzExcellUpload singleton = null;
        public static frmB_BackOfzExcellUpload Instance
        {
            get
            {
                if (frmB_BackOfzExcellUpload.singleton == null)
                {
                    frmB_BackOfzExcellUpload.singleton = new frmB_BackOfzExcellUpload();
                }
                return frmB_BackOfzExcellUpload.singleton;
            }
        }

        private void MetroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void MetroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzExcellUpload_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzExcellUpload.singleton = null;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
