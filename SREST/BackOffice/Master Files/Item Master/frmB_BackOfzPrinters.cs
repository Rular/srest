﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzPrinters : frmC_ModelBlack
    {
        public frmB_BackOfzPrinters()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzPrinters singleton = null;

        public static frmB_BackOfzPrinters Instance
        {
            get
            {
                if (frmB_BackOfzPrinters.singleton == null)
                {
                    frmB_BackOfzPrinters.singleton = new frmB_BackOfzPrinters();
                }
                return frmB_BackOfzPrinters.singleton;
            }
        }

        private void FrmB_BackOfzPrinters_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzPrinters_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzPrinters.singleton = null;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzPrinters.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddPrinter().ShowDialog();

        }

       
    }
}
