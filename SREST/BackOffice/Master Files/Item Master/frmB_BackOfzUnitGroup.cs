﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzUnitGroup : frmC_ModelBlack
    {
        public bool IsEdit = false;
        cls_UnitGroup unit = new cls_UnitGroup();
        public int unitid;
        public frmB_BackOfzUnitGroup()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzUnitGroup singleton = null;

        public static frmB_BackOfzUnitGroup Instance
        {
            get
            {
                if (frmB_BackOfzUnitGroup.singleton == null)
                {
                    frmB_BackOfzUnitGroup.singleton = new frmB_BackOfzUnitGroup();
                }
                return frmB_BackOfzUnitGroup.singleton;
            }
        }

        private void FrmB_BackOfzUnitGroup_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        public void LoadDataGrid()
        {
            DataTable dtUnitGroup = unit.GetAllUnitGroup();

            if (dtUnitGroup.Columns.Count >= 1)
                dgwUnitGroup.DataSource = dtUnitGroup;

            // Grid Decoration
            dgwUnitGroup.Columns[1].Visible = false;
        }

        private void FrmB_BackOfzUnitGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzUnitGroup.singleton = null;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnitGroup.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnNewCategory_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddUnitGroup().ShowDialog();
        }

        private void BttnEditCategory_Click(object sender, EventArgs e)
        {
            if (dgwUnitGroup.SelectedRows.Count > 0)
            {
                IsEdit = true;
                frmB_BackOfzAddUnitGroup.GetEditDetails(IsEdit, int.Parse(dgwUnitGroup.CurrentRow.Cells[1].FormattedValue.ToString()), dgwUnitGroup.CurrentRow.Cells[0].FormattedValue.ToString());
                new frmB_BackOfzAddUnitGroup().ShowDialog();

            }
              
            else
                MessageBox.Show("Please Select Full Row To Continue");


        }

        private void BttnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgwUnitGroup.SelectedRows.Count > 0)
                {
                    unitid = int.Parse(dgwUnitGroup.CurrentRow.Cells[1].FormattedValue.ToString());
                }
                else
                {
                    MessageBox.Show("Please Select Category to Continue");
                    return;
                }


                if (dgwUnitGroup.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Are You Sure You Want Delete This UnitGroup?", Application.CompanyName + " " + Application.ProductName + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (unit.deleteUnitGroup(unitid) == true)
                        {
                            MessageBox.Show("Success Fully Deleted");
                            frmB_BackOfzUnitGroup ItemCat = frmB_BackOfzUnitGroup.Instance;
                            ItemCat.LoadDataGrid();

                        }
                        else
                        {
                            MessageBox.Show("Error");
                        }
                    }

                }

                else
                {
                    MessageBox.Show("Please Select Full Row To Continue");
                }
            }
            catch (Exception EX)
            {

                MessageBox.Show("This Category Use Other Products Unable To Delete");
            }
        }
    }
}
