﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST.BackOffice
{
    public partial class frmB_BackOfzUnitConversion : frmC_ModelBlack
    {
        public frmB_BackOfzUnitConversion()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzUnitConversion singleton = null;

        public static frmB_BackOfzUnitConversion Instance
        {
            get
            {
                if (frmB_BackOfzUnitConversion.singleton == null)
                {
                    frmB_BackOfzUnitConversion.singleton = new frmB_BackOfzUnitConversion();
                }
                return frmB_BackOfzUnitConversion.singleton;
            }
        }

        private void FrmB_BackOfzUnitConversion_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzUnitConversion_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzUnitConversion.singleton = null;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnitConversion.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddUnitConversion().ShowDialog();
        }
    }
}
