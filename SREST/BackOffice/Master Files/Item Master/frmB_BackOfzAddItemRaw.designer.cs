﻿namespace SREST
{
    partial class frmB_BackOfzAddItemRaw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutSub = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.cmbSubCategory1 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cmbSubCategory2 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtItemName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtBarcode = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtCostPrice = new MetroFramework.Controls.MetroTextBox();
            this.txtSalesPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cmbUnitGroup = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.cmbUnit = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtReorderLevel = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanelExpireDate = new System.Windows.Forms.TableLayoutPanel();
            this.toggleNotifyExpireDate = new MetroFramework.Controls.MetroToggle();
            this.txtEarlyNotfyDays = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.toggleInventoryItem = new MetroFramework.Controls.MetroToggle();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.toggleActive = new MetroFramework.Controls.MetroToggle();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.cmbStockStandard = new MetroFramework.Controls.MetroComboBox();
            this.tableLayoutPanelBttns = new System.Windows.Forms.TableLayoutPanel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.lblMsg = new System.Windows.Forms.Label();
            this.tableLayoutPanelPic = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelSubPic = new System.Windows.Forms.TableLayoutPanel();
            this.PicBoxItem = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanelCamButton = new System.Windows.Forms.TableLayoutPanel();
            this.bttncapture = new MetroFramework.Controls.MetroButton();
            this.bttnstart = new MetroFramework.Controls.MetroButton();
            this.cmbcamlist = new System.Windows.Forms.ComboBox();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutMain.SuspendLayout();
            this.tableLayoutSub.SuspendLayout();
            this.tableLayoutPanelExpireDate.SuspendLayout();
            this.tableLayoutPanelBttns.SuspendLayout();
            this.tableLayoutPanelPic.SuspendLayout();
            this.tableLayoutPanelSubPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).BeginInit();
            this.tableLayoutPanelCamButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.Enabled = false;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(943, 45);
            this.metroPanel1.TabIndex = 730;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(943, 45);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Raw Item";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // metroPanel2
            // 
            this.metroPanel2.AutoScroll = true;
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel2.HorizontalScrollbar = true;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 45);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(943, 32);
            this.metroPanel2.TabIndex = 731;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.VerticalScrollbar = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.AutoScroll = true;
            this.tableLayoutMain.ColumnCount = 2;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutSub, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutPanelPic, 1, 0);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 77);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 1;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutMain.Size = new System.Drawing.Size(943, 523);
            this.tableLayoutMain.TabIndex = 732;
            // 
            // tableLayoutSub
            // 
            this.tableLayoutSub.AutoScroll = true;
            this.tableLayoutSub.ColumnCount = 2;
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.54506F));
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.45494F));
            this.tableLayoutSub.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutSub.Controls.Add(this.cmbSubCategory1, 1, 0);
            this.tableLayoutSub.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutSub.Controls.Add(this.cmbSubCategory2, 1, 1);
            this.tableLayoutSub.Controls.Add(this.metroLabel3, 0, 2);
            this.tableLayoutSub.Controls.Add(this.txtItemName, 1, 2);
            this.tableLayoutSub.Controls.Add(this.metroLabel4, 0, 3);
            this.tableLayoutSub.Controls.Add(this.txtBarcode, 1, 3);
            this.tableLayoutSub.Controls.Add(this.metroLabel5, 0, 4);
            this.tableLayoutSub.Controls.Add(this.txtCostPrice, 1, 4);
            this.tableLayoutSub.Controls.Add(this.txtSalesPrice, 1, 5);
            this.tableLayoutSub.Controls.Add(this.metroLabel6, 0, 5);
            this.tableLayoutSub.Controls.Add(this.metroLabel7, 0, 6);
            this.tableLayoutSub.Controls.Add(this.cmbUnitGroup, 1, 6);
            this.tableLayoutSub.Controls.Add(this.metroLabel8, 0, 7);
            this.tableLayoutSub.Controls.Add(this.cmbUnit, 1, 7);
            this.tableLayoutSub.Controls.Add(this.metroLabel9, 0, 8);
            this.tableLayoutSub.Controls.Add(this.txtReorderLevel, 1, 8);
            this.tableLayoutSub.Controls.Add(this.metroLabel10, 0, 9);
            this.tableLayoutSub.Controls.Add(this.tableLayoutPanelExpireDate, 1, 9);
            this.tableLayoutSub.Controls.Add(this.metroLabel11, 0, 10);
            this.tableLayoutSub.Controls.Add(this.toggleInventoryItem, 1, 10);
            this.tableLayoutSub.Controls.Add(this.metroLabel12, 0, 11);
            this.tableLayoutSub.Controls.Add(this.toggleActive, 1, 11);
            this.tableLayoutSub.Controls.Add(this.metroLabel13, 0, 12);
            this.tableLayoutSub.Controls.Add(this.cmbStockStandard, 1, 12);
            this.tableLayoutSub.Controls.Add(this.tableLayoutPanelBttns, 1, 13);
            this.tableLayoutSub.Controls.Add(this.lblMsg, 1, 14);
            this.tableLayoutSub.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutSub.Name = "tableLayoutSub";
            this.tableLayoutSub.RowCount = 16;
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839365F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839365F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.839459F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.386847F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.131627F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.56874F));
            this.tableLayoutSub.Size = new System.Drawing.Size(545, 750);
            this.tableLayoutSub.TabIndex = 0;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(3, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.TabIndex = 746;
            this.metroLabel1.Text = "Raw Sub Category ";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // cmbSubCategory1
            // 
            this.cmbSubCategory1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSubCategory1.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbSubCategory1.FormattingEnabled = true;
            this.cmbSubCategory1.ItemHeight = 29;
            this.cmbSubCategory1.Location = new System.Drawing.Point(174, 3);
            this.cmbSubCategory1.Name = "cmbSubCategory1";
            this.cmbSubCategory1.Size = new System.Drawing.Size(368, 35);
            this.cmbSubCategory1.TabIndex = 747;
            this.cmbSubCategory1.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(3, 55);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(112, 19);
            this.metroLabel2.TabIndex = 748;
            this.metroLabel2.Text = "Sub Category  2 ";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // cmbSubCategory2
            // 
            this.cmbSubCategory2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSubCategory2.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbSubCategory2.FormattingEnabled = true;
            this.cmbSubCategory2.ItemHeight = 29;
            this.cmbSubCategory2.Location = new System.Drawing.Point(174, 46);
            this.cmbSubCategory2.Name = "cmbSubCategory2";
            this.cmbSubCategory2.Size = new System.Drawing.Size(368, 35);
            this.cmbSubCategory2.TabIndex = 749;
            this.cmbSubCategory2.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(3, 98);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(110, 19);
            this.metroLabel3.TabIndex = 750;
            this.metroLabel3.Text = "Raw Item Name ";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // txtItemName
            // 
            // 
            // 
            // 
            this.txtItemName.CustomButton.Image = null;
            this.txtItemName.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtItemName.CustomButton.Name = "";
            this.txtItemName.CustomButton.Size = new System.Drawing.Size(35, 35);
            this.txtItemName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtItemName.CustomButton.TabIndex = 1;
            this.txtItemName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtItemName.CustomButton.UseSelectable = true;
            this.txtItemName.CustomButton.Visible = false;
            this.txtItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtItemName.Lines = new string[0];
            this.txtItemName.Location = new System.Drawing.Point(174, 89);
            this.txtItemName.MaxLength = 32767;
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.PasswordChar = '\0';
            this.txtItemName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtItemName.SelectedText = "";
            this.txtItemName.SelectionLength = 0;
            this.txtItemName.SelectionStart = 0;
            this.txtItemName.ShortcutsEnabled = true;
            this.txtItemName.Size = new System.Drawing.Size(368, 37);
            this.txtItemName.TabIndex = 751;
            this.txtItemName.UseSelectable = true;
            this.txtItemName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtItemName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(3, 141);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(62, 19);
            this.metroLabel4.TabIndex = 752;
            this.metroLabel4.Text = "Barcode ";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // txtBarcode
            // 
            // 
            // 
            // 
            this.txtBarcode.CustomButton.Image = null;
            this.txtBarcode.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtBarcode.CustomButton.Name = "";
            this.txtBarcode.CustomButton.Size = new System.Drawing.Size(35, 35);
            this.txtBarcode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBarcode.CustomButton.TabIndex = 1;
            this.txtBarcode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBarcode.CustomButton.UseSelectable = true;
            this.txtBarcode.CustomButton.Visible = false;
            this.txtBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBarcode.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtBarcode.Lines = new string[0];
            this.txtBarcode.Location = new System.Drawing.Point(174, 132);
            this.txtBarcode.MaxLength = 32767;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.PasswordChar = '\0';
            this.txtBarcode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBarcode.SelectedText = "";
            this.txtBarcode.SelectionLength = 0;
            this.txtBarcode.SelectionStart = 0;
            this.txtBarcode.ShortcutsEnabled = true;
            this.txtBarcode.Size = new System.Drawing.Size(368, 37);
            this.txtBarcode.TabIndex = 753;
            this.txtBarcode.UseSelectable = true;
            this.txtBarcode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBarcode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel5.Location = new System.Drawing.Point(3, 184);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(70, 19);
            this.metroLabel5.TabIndex = 754;
            this.metroLabel5.Text = "Cost Price";
            this.metroLabel5.UseCustomBackColor = true;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // txtCostPrice
            // 
            // 
            // 
            // 
            this.txtCostPrice.CustomButton.Image = null;
            this.txtCostPrice.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtCostPrice.CustomButton.Name = "";
            this.txtCostPrice.CustomButton.Size = new System.Drawing.Size(35, 35);
            this.txtCostPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCostPrice.CustomButton.TabIndex = 1;
            this.txtCostPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCostPrice.CustomButton.UseSelectable = true;
            this.txtCostPrice.CustomButton.Visible = false;
            this.txtCostPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCostPrice.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtCostPrice.Lines = new string[0];
            this.txtCostPrice.Location = new System.Drawing.Point(174, 175);
            this.txtCostPrice.MaxLength = 32767;
            this.txtCostPrice.Name = "txtCostPrice";
            this.txtCostPrice.PasswordChar = '\0';
            this.txtCostPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCostPrice.SelectedText = "";
            this.txtCostPrice.SelectionLength = 0;
            this.txtCostPrice.SelectionStart = 0;
            this.txtCostPrice.ShortcutsEnabled = true;
            this.txtCostPrice.Size = new System.Drawing.Size(368, 37);
            this.txtCostPrice.TabIndex = 755;
            this.txtCostPrice.UseSelectable = true;
            this.txtCostPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCostPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtSalesPrice
            // 
            // 
            // 
            // 
            this.txtSalesPrice.CustomButton.Image = null;
            this.txtSalesPrice.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtSalesPrice.CustomButton.Name = "";
            this.txtSalesPrice.CustomButton.Size = new System.Drawing.Size(35, 35);
            this.txtSalesPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSalesPrice.CustomButton.TabIndex = 1;
            this.txtSalesPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSalesPrice.CustomButton.UseSelectable = true;
            this.txtSalesPrice.CustomButton.Visible = false;
            this.txtSalesPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSalesPrice.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtSalesPrice.Lines = new string[0];
            this.txtSalesPrice.Location = new System.Drawing.Point(174, 218);
            this.txtSalesPrice.MaxLength = 32767;
            this.txtSalesPrice.Name = "txtSalesPrice";
            this.txtSalesPrice.PasswordChar = '\0';
            this.txtSalesPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSalesPrice.SelectedText = "";
            this.txtSalesPrice.SelectionLength = 0;
            this.txtSalesPrice.SelectionStart = 0;
            this.txtSalesPrice.ShortcutsEnabled = true;
            this.txtSalesPrice.Size = new System.Drawing.Size(368, 37);
            this.txtSalesPrice.TabIndex = 757;
            this.txtSalesPrice.UseSelectable = true;
            this.txtSalesPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSalesPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel6.Location = new System.Drawing.Point(3, 227);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(72, 19);
            this.metroLabel6.TabIndex = 756;
            this.metroLabel6.Text = "Sales Price";
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel7.Location = new System.Drawing.Point(3, 270);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(78, 19);
            this.metroLabel7.TabIndex = 758;
            this.metroLabel7.Text = "Unit Group";
            this.metroLabel7.UseCustomBackColor = true;
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // cmbUnitGroup
            // 
            this.cmbUnitGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUnitGroup.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbUnitGroup.FormattingEnabled = true;
            this.cmbUnitGroup.ItemHeight = 29;
            this.cmbUnitGroup.Location = new System.Drawing.Point(174, 261);
            this.cmbUnitGroup.Name = "cmbUnitGroup";
            this.cmbUnitGroup.Size = new System.Drawing.Size(368, 35);
            this.cmbUnitGroup.TabIndex = 759;
            this.cmbUnitGroup.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel8.Location = new System.Drawing.Point(3, 313);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(35, 19);
            this.metroLabel8.TabIndex = 760;
            this.metroLabel8.Text = "Unit";
            this.metroLabel8.UseCustomBackColor = true;
            this.metroLabel8.UseCustomForeColor = true;
            // 
            // cmbUnit
            // 
            this.cmbUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUnit.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.ItemHeight = 29;
            this.cmbUnit.Location = new System.Drawing.Point(174, 304);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(368, 35);
            this.cmbUnit.TabIndex = 761;
            this.cmbUnit.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel9.Location = new System.Drawing.Point(3, 356);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(92, 19);
            this.metroLabel9.TabIndex = 762;
            this.metroLabel9.Text = "Reorder Level";
            this.metroLabel9.UseCustomBackColor = true;
            this.metroLabel9.UseCustomForeColor = true;
            // 
            // txtReorderLevel
            // 
            // 
            // 
            // 
            this.txtReorderLevel.CustomButton.Image = null;
            this.txtReorderLevel.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtReorderLevel.CustomButton.Name = "";
            this.txtReorderLevel.CustomButton.Size = new System.Drawing.Size(35, 35);
            this.txtReorderLevel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtReorderLevel.CustomButton.TabIndex = 1;
            this.txtReorderLevel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtReorderLevel.CustomButton.UseSelectable = true;
            this.txtReorderLevel.CustomButton.Visible = false;
            this.txtReorderLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReorderLevel.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtReorderLevel.Lines = new string[0];
            this.txtReorderLevel.Location = new System.Drawing.Point(174, 347);
            this.txtReorderLevel.MaxLength = 32767;
            this.txtReorderLevel.Name = "txtReorderLevel";
            this.txtReorderLevel.PasswordChar = '\0';
            this.txtReorderLevel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReorderLevel.SelectedText = "";
            this.txtReorderLevel.SelectionLength = 0;
            this.txtReorderLevel.SelectionStart = 0;
            this.txtReorderLevel.ShortcutsEnabled = true;
            this.txtReorderLevel.Size = new System.Drawing.Size(368, 37);
            this.txtReorderLevel.TabIndex = 763;
            this.txtReorderLevel.UseSelectable = true;
            this.txtReorderLevel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtReorderLevel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel10.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel10.Location = new System.Drawing.Point(3, 399);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(119, 19);
            this.metroLabel10.TabIndex = 764;
            this.metroLabel10.Text = "Notify Expire Date";
            this.metroLabel10.UseCustomBackColor = true;
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // tableLayoutPanelExpireDate
            // 
            this.tableLayoutPanelExpireDate.AutoScroll = true;
            this.tableLayoutPanelExpireDate.ColumnCount = 2;
            this.tableLayoutPanelExpireDate.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.90217F));
            this.tableLayoutPanelExpireDate.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.09782F));
            this.tableLayoutPanelExpireDate.Controls.Add(this.toggleNotifyExpireDate, 0, 0);
            this.tableLayoutPanelExpireDate.Controls.Add(this.txtEarlyNotfyDays, 1, 0);
            this.tableLayoutPanelExpireDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelExpireDate.Location = new System.Drawing.Point(174, 390);
            this.tableLayoutPanelExpireDate.Name = "tableLayoutPanelExpireDate";
            this.tableLayoutPanelExpireDate.RowCount = 1;
            this.tableLayoutPanelExpireDate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelExpireDate.Size = new System.Drawing.Size(368, 37);
            this.tableLayoutPanelExpireDate.TabIndex = 765;
            // 
            // toggleNotifyExpireDate
            // 
            this.toggleNotifyExpireDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleNotifyExpireDate.AutoSize = true;
            this.toggleNotifyExpireDate.Location = new System.Drawing.Point(3, 10);
            this.toggleNotifyExpireDate.Name = "toggleNotifyExpireDate";
            this.toggleNotifyExpireDate.Size = new System.Drawing.Size(80, 17);
            this.toggleNotifyExpireDate.TabIndex = 0;
            this.toggleNotifyExpireDate.Text = "Off";
            this.toggleNotifyExpireDate.UseCustomBackColor = true;
            this.toggleNotifyExpireDate.UseSelectable = true;
            // 
            // txtEarlyNotfyDays
            // 
            // 
            // 
            // 
            this.txtEarlyNotfyDays.CustomButton.Image = null;
            this.txtEarlyNotfyDays.CustomButton.Location = new System.Drawing.Point(90, 1);
            this.txtEarlyNotfyDays.CustomButton.Name = "";
            this.txtEarlyNotfyDays.CustomButton.Size = new System.Drawing.Size(29, 29);
            this.txtEarlyNotfyDays.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEarlyNotfyDays.CustomButton.TabIndex = 1;
            this.txtEarlyNotfyDays.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEarlyNotfyDays.CustomButton.UseSelectable = true;
            this.txtEarlyNotfyDays.CustomButton.Visible = false;
            this.txtEarlyNotfyDays.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtEarlyNotfyDays.Lines = new string[0];
            this.txtEarlyNotfyDays.Location = new System.Drawing.Point(102, 3);
            this.txtEarlyNotfyDays.MaxLength = 32767;
            this.txtEarlyNotfyDays.Name = "txtEarlyNotfyDays";
            this.txtEarlyNotfyDays.PasswordChar = '\0';
            this.txtEarlyNotfyDays.PromptText = "Early Notify Days";
            this.txtEarlyNotfyDays.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEarlyNotfyDays.SelectedText = "";
            this.txtEarlyNotfyDays.SelectionLength = 0;
            this.txtEarlyNotfyDays.SelectionStart = 0;
            this.txtEarlyNotfyDays.ShortcutsEnabled = true;
            this.txtEarlyNotfyDays.Size = new System.Drawing.Size(120, 31);
            this.txtEarlyNotfyDays.TabIndex = 766;
            this.txtEarlyNotfyDays.UseSelectable = true;
            this.txtEarlyNotfyDays.WaterMark = "Early Notify Days";
            this.txtEarlyNotfyDays.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEarlyNotfyDays.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel11.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel11.Location = new System.Drawing.Point(3, 442);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(100, 19);
            this.metroLabel11.TabIndex = 766;
            this.metroLabel11.Text = "Inventory Item";
            this.metroLabel11.UseCustomBackColor = true;
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // toggleInventoryItem
            // 
            this.toggleInventoryItem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleInventoryItem.AutoSize = true;
            this.toggleInventoryItem.Location = new System.Drawing.Point(174, 443);
            this.toggleInventoryItem.Name = "toggleInventoryItem";
            this.toggleInventoryItem.Size = new System.Drawing.Size(80, 17);
            this.toggleInventoryItem.TabIndex = 767;
            this.toggleInventoryItem.Text = "Off";
            this.toggleInventoryItem.UseCustomBackColor = true;
            this.toggleInventoryItem.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel12.Location = new System.Drawing.Point(3, 485);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(46, 19);
            this.metroLabel12.TabIndex = 768;
            this.metroLabel12.Text = "Active";
            this.metroLabel12.UseCustomBackColor = true;
            this.metroLabel12.UseCustomForeColor = true;
            // 
            // toggleActive
            // 
            this.toggleActive.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleActive.AutoSize = true;
            this.toggleActive.Location = new System.Drawing.Point(174, 486);
            this.toggleActive.Name = "toggleActive";
            this.toggleActive.Size = new System.Drawing.Size(80, 17);
            this.toggleActive.TabIndex = 769;
            this.toggleActive.Text = "Off";
            this.toggleActive.UseCustomBackColor = true;
            this.toggleActive.UseSelectable = true;
            // 
            // metroLabel13
            // 
            this.metroLabel13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel13.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel13.Location = new System.Drawing.Point(3, 528);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(101, 19);
            this.metroLabel13.TabIndex = 770;
            this.metroLabel13.Text = "Stock Standard";
            this.metroLabel13.UseCustomBackColor = true;
            this.metroLabel13.UseCustomForeColor = true;
            // 
            // cmbStockStandard
            // 
            this.cmbStockStandard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbStockStandard.FormattingEnabled = true;
            this.cmbStockStandard.ItemHeight = 23;
            this.cmbStockStandard.Location = new System.Drawing.Point(174, 519);
            this.cmbStockStandard.Name = "cmbStockStandard";
            this.cmbStockStandard.Size = new System.Drawing.Size(368, 29);
            this.cmbStockStandard.TabIndex = 771;
            this.cmbStockStandard.UseSelectable = true;
            // 
            // tableLayoutPanelBttns
            // 
            this.tableLayoutPanelBttns.AutoScroll = true;
            this.tableLayoutPanelBttns.ColumnCount = 3;
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.Controls.Add(this.bttnSave, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClear, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClose, 0, 0);
            this.tableLayoutPanelBttns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBttns.Location = new System.Drawing.Point(174, 562);
            this.tableLayoutPanelBttns.Name = "tableLayoutPanelBttns";
            this.tableLayoutPanelBttns.RowCount = 1;
            this.tableLayoutPanelBttns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBttns.Size = new System.Drawing.Size(368, 49);
            this.tableLayoutPanelBttns.TabIndex = 772;
            // 
            // bttnSave
            // 
            this.bttnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(221, 3);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 43);
            this.bttnSave.TabIndex = 7;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            // 
            // bttnClear
            // 
            this.bttnClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(112, 3);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 43);
            this.bttnClear.TabIndex = 6;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(3, 3);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 43);
            this.bttnClose.TabIndex = 5;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(174, 614);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(368, 23);
            this.lblMsg.TabIndex = 773;
            this.lblMsg.Text = "Messege";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // tableLayoutPanelPic
            // 
            this.tableLayoutPanelPic.AutoScroll = true;
            this.tableLayoutPanelPic.ColumnCount = 1;
            this.tableLayoutPanelPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelPic.Controls.Add(this.tableLayoutPanelSubPic, 0, 0);
            this.tableLayoutPanelPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelPic.Location = new System.Drawing.Point(554, 3);
            this.tableLayoutPanelPic.Name = "tableLayoutPanelPic";
            this.tableLayoutPanelPic.RowCount = 2;
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.41958F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.58042F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.Size = new System.Drawing.Size(386, 720);
            this.tableLayoutPanelPic.TabIndex = 1;
            // 
            // tableLayoutPanelSubPic
            // 
            this.tableLayoutPanelSubPic.AutoScroll = true;
            this.tableLayoutPanelSubPic.ColumnCount = 3;
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.57627F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.42373F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanelSubPic.Controls.Add(this.PicBoxItem, 1, 0);
            this.tableLayoutPanelSubPic.Controls.Add(this.tableLayoutPanelCamButton, 1, 1);
            this.tableLayoutPanelSubPic.Controls.Add(this.cmbcamlist, 1, 2);
            this.tableLayoutPanelSubPic.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelSubPic.Name = "tableLayoutPanelSubPic";
            this.tableLayoutPanelSubPic.RowCount = 4;
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 224F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSubPic.Size = new System.Drawing.Size(380, 503);
            this.tableLayoutPanelSubPic.TabIndex = 0;
            // 
            // PicBoxItem
            // 
            this.PicBoxItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PicBoxItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBoxItem.Location = new System.Drawing.Point(109, 3);
            this.PicBoxItem.Name = "PicBoxItem";
            this.PicBoxItem.Size = new System.Drawing.Size(195, 204);
            this.PicBoxItem.TabIndex = 11;
            this.PicBoxItem.TabStop = false;
            // 
            // tableLayoutPanelCamButton
            // 
            this.tableLayoutPanelCamButton.AutoScroll = true;
            this.tableLayoutPanelCamButton.ColumnCount = 2;
            this.tableLayoutPanelCamButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCamButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCamButton.Controls.Add(this.bttncapture, 0, 0);
            this.tableLayoutPanelCamButton.Controls.Add(this.bttnstart, 0, 0);
            this.tableLayoutPanelCamButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCamButton.Location = new System.Drawing.Point(109, 213);
            this.tableLayoutPanelCamButton.Name = "tableLayoutPanelCamButton";
            this.tableLayoutPanelCamButton.RowCount = 1;
            this.tableLayoutPanelCamButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCamButton.Size = new System.Drawing.Size(195, 33);
            this.tableLayoutPanelCamButton.TabIndex = 12;
            // 
            // bttncapture
            // 
            this.bttncapture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttncapture.Location = new System.Drawing.Point(100, 3);
            this.bttncapture.Name = "bttncapture";
            this.bttncapture.Size = new System.Drawing.Size(92, 27);
            this.bttncapture.TabIndex = 67;
            this.bttncapture.Text = "Capture";
            this.bttncapture.UseSelectable = true;
            // 
            // bttnstart
            // 
            this.bttnstart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnstart.Location = new System.Drawing.Point(3, 3);
            this.bttnstart.Name = "bttnstart";
            this.bttnstart.Size = new System.Drawing.Size(91, 27);
            this.bttnstart.TabIndex = 66;
            this.bttnstart.Text = "Start";
            this.bttnstart.UseSelectable = true;
            // 
            // cmbcamlist
            // 
            this.cmbcamlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbcamlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcamlist.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcamlist.FormattingEnabled = true;
            this.cmbcamlist.Location = new System.Drawing.Point(109, 252);
            this.cmbcamlist.Name = "cmbcamlist";
            this.cmbcamlist.Size = new System.Drawing.Size(195, 26);
            this.cmbcamlist.TabIndex = 68;
            // 
            // frmB_BackOfzAddItemRaw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnClose;
            this.ClientSize = new System.Drawing.Size(943, 600);
            this.Controls.Add(this.tableLayoutMain);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddItemRaw";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddItem";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddItem_Load);
            this.metroPanel1.ResumeLayout(false);
            this.tableLayoutMain.ResumeLayout(false);
            this.tableLayoutSub.ResumeLayout(false);
            this.tableLayoutSub.PerformLayout();
            this.tableLayoutPanelExpireDate.ResumeLayout(false);
            this.tableLayoutPanelExpireDate.PerformLayout();
            this.tableLayoutPanelBttns.ResumeLayout(false);
            this.tableLayoutPanelPic.ResumeLayout(false);
            this.tableLayoutPanelSubPic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).EndInit();
            this.tableLayoutPanelCamButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSub;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cmbSubCategory1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox cmbSubCategory2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtItemName;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtBarcode;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtCostPrice;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtSalesPrice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPic;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSubPic;
        internal System.Windows.Forms.PictureBox PicBoxItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCamButton;
        private MetroFramework.Controls.MetroButton bttncapture;
        private MetroFramework.Controls.MetroButton bttnstart;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroComboBox cmbUnitGroup;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox cmbUnit;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox txtReorderLevel;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelExpireDate;
        private MetroFramework.Controls.MetroToggle toggleNotifyExpireDate;
        private MetroFramework.Controls.MetroTextBox txtEarlyNotfyDays;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroToggle toggleInventoryItem;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroToggle toggleActive;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroComboBox cmbStockStandard;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBttns;
        private MetroFramework.Controls.MetroButton bttnClose;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroButton bttnSave;
        private System.Windows.Forms.ComboBox cmbcamlist;
        private System.Windows.Forms.Label lblMsg;
    }
}