﻿using SREST.BackOffice.MasterClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzAddSubCat : Form
    {
        cls_ItemCategory cat = new cls_ItemCategory();
        cls_ItemSubCategory subCat =new  cls_ItemSubCategory();


        public static int SubCatId;
        static int CatId;
        static bool isEditCategory = false;
        public DataTable cate;
        public frmB_BackOfzAddSubCat()
        {
            InitializeComponent();
        }

        public static void GetEditDetails(bool edit, int CatCode)
        {
            isEditCategory = edit;
            SubCatId = CatCode;
        }

        private void BttnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // hell Validate TextBoxes
                if (validate())
                {
                    subCat.CatCode = cmbCategory.SelectedValue.ToString();
                    subCat.SubCatcode = txtSubCategory.Text.Trim();
                    subCat.SubDescription = txtDescription.Text.Trim();

                    if (isEditCategory == true)
                    {
                        updateSubCategory();
                        //isEditCategory = false;
                    }

                    else if (isEditCategory == false)
                    {
                        if (subCat.CheckDuplicateSubCategory(txtSubCategory.Text.Trim(), txtDescription.Text.Trim(), false) is true)
                        {
                            if (subCat.SaveSubCategory() == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Added";
                                clear();
                                bttnSave.Text = "Save (F2)";
                                frmB_BackOfzItemSubCategory itemSub = frmB_BackOfzItemSubCategory.Instance;
                                itemSub.LoadDataGrid();

                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Sub Category Details";
                            bttnSave.Text = "Save (F2)";
                        }


                    }


                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }



        //update Category
        public void updateSubCategory()
        {
            try
            {

                if (validate())
                {
                    subCat.SubCatName = cmbCategory.Text.ToString().Trim();
                    subCat.CatCode = cmbCategory.SelectedValue.ToString();
                    subCat.SubCatcode = txtSubCategory.Text.Trim();       
                    subCat.SubDescription = txtDescription.Text.Trim();


                    if (isEditCategory == true)
                    {
                        //Check Count For Update
                        if (subCat.CheckDuplicateSubCategoryUpdate(txtSubCategory.Text.Trim(), txtDescription.Text.Trim(), isEditCategory) is true)
                        {
                            if (subCat.UpdateCategory(SubCatId) == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Updated";
                                clear();
                                frmB_BackOfzItemSubCategory ItemCat = frmB_BackOfzItemSubCategory.Instance;
                                ItemCat.LoadDataGrid();
                                bttnSave.Text = "Save (F2)";
                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Category Details";
                            bttnSave.Text = "Update(F2)";
                            return;
                        }

                    }

                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
             isEditCategory = false;
            this.Close();
        }

        private void BttnClear_Click(object sender, EventArgs e)
        {
            clear();
        }

        void clear()
        {
            txtSubCategory.Clear();
            txtDescription.Clear();
            cmbCategory.DataSource=null;
            txtSubCategory.Focus();
            lblMsg.Visible = false;
        }

        private void FrmB_BackOfzAddSubCat_Load(object sender, EventArgs e)
        {
            Itemcategory();

            if (isEditCategory)
            {
                loadSubCategory();
                bttnSave.Text = "Update (F2)";
            }
        }

        private void Itemcategory()
        {
           
            DataTable dt = cat.GetAllCategory();
            cmbCategory.DataSource = dt;
            cmbCategory.DisplayMember = "categoryid";
            cmbCategory.ValueMember = "id";
        }

        private void CmbCategory_Click(object sender, EventArgs e)
        {
            Itemcategory();
        }

        //validate text Boxes
        public bool validate()
        {

            if (string.IsNullOrEmpty(cmbCategory.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Select Your Category Code To Continue";
                txtSubCategory.Focus();
                return false;

            }


            if (string.IsNullOrEmpty(txtSubCategory.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Sub Category Code To Continue";
                txtSubCategory.Focus();
                return false;

            }

            if (string.IsNullOrEmpty(txtDescription.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Sub Category Description To Continue";
                txtDescription.Focus();
                return false;
            }
            else
            {
                return true;
            }

            return false;
        }

        void loadSubCategory()
        {
            cate = subCat.GetSubCategory(SubCatId);
            if (cate.Rows.Count > 0)
            {

                cls_CommonFn.LoadDataTableToCombo(cate, cmbCategory);
                cmbCategory.SelectedIndex = 0;
                cmbCategory.Focus();


                txtSubCategory.Text = cate.Rows[0]["name"].ToString();
                txtDescription.Text = cate.Rows[0]["description"].ToString();
            }
        }
    }
}
