﻿namespace SREST
{
    partial class frmB_BackOfzAddUnitGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.txtUnitGroup = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 744;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Unit Group";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(23, 127);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(397, 23);
            this.lblMsg.TabIndex = 747;
            this.lblMsg.Text = "Messege";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(174, 168);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 742;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            this.bttnClear.Click += new System.EventHandler(this.BttnClear_Click);
            // 
            // txtUnitGroup
            // 
            // 
            // 
            // 
            this.txtUnitGroup.CustomButton.Image = null;
            this.txtUnitGroup.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtUnitGroup.CustomButton.Name = "";
            this.txtUnitGroup.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUnitGroup.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUnitGroup.CustomButton.TabIndex = 1;
            this.txtUnitGroup.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUnitGroup.CustomButton.UseSelectable = true;
            this.txtUnitGroup.CustomButton.Visible = false;
            this.txtUnitGroup.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtUnitGroup.Lines = new string[0];
            this.txtUnitGroup.Location = new System.Drawing.Point(26, 95);
            this.txtUnitGroup.MaxLength = 32767;
            this.txtUnitGroup.Name = "txtUnitGroup";
            this.txtUnitGroup.PasswordChar = '\0';
            this.txtUnitGroup.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitGroup.SelectedText = "";
            this.txtUnitGroup.SelectionLength = 0;
            this.txtUnitGroup.SelectionStart = 0;
            this.txtUnitGroup.ShortcutsEnabled = true;
            this.txtUnitGroup.Size = new System.Drawing.Size(397, 23);
            this.txtUnitGroup.TabIndex = 1;
            this.txtUnitGroup.UseSelectable = true;
            this.txtUnitGroup.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUnitGroup.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(26, 69);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.TabIndex = 745;
            this.metroLabel1.Text = "Unit Group Name :";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // bttnSave
            // 
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(281, 168);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 44);
            this.bttnSave.TabIndex = 741;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            this.bttnSave.Click += new System.EventHandler(this.BttnSave_Click);
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(67, 168);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 743;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // frmB_BackOfzAddUnitGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 224);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtUnitGroup);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnSave);
            this.Controls.Add(this.bttnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddUnitGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddUnitGroup";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddUnitGroup_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroTextBox txtUnitGroup;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClose;
    }
}