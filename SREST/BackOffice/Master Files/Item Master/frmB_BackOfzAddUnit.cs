﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.Master_Files.MasterClasses;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzAddUnit : Form
    {

        clsm_Unit unit = new clsm_Unit();
        cls_UnitGroup unitgroup = new cls_UnitGroup();

        public static bool isEditUnit = false;
        public static int unitId;

        public DataTable units;
        public frmB_BackOfzAddUnit()
        {
            InitializeComponent();
        }

        public static void GetEditDetails(bool edit, int unitid)
        {
            isEditUnit = edit;
            unitId = unitid;
        }
        private void BttnClear_Click(object sender, EventArgs e)
        {
            clear();
        }

        void clear()
        {
            txtUnitName.Clear();
            txtUnitSymbol.Clear();
            txtUnitName.Focus();
                
        }

        private void BttnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // hell Validate TextBoxes
                if (validate())
                {
                    unit.UsergroupId = cmbUnitGroup.SelectedValue.ToString();
                    unit.UnitCode = txtUnitName.Text.ToString().Trim();
                    unit.UnitDescription = txtUnitSymbol.Text.ToString().Trim();

                    if (isEditUnit == true)
                    {
                        updateSubCategory();
                        //isEditCategory = false;
                    }

                    else if (isEditUnit == false)
                    {
                        if (unit.CheckDuplicateUnit(txtUnitName.Text.Trim(), txtUnitSymbol.Text.Trim(), false) is true)
                        {
                            if (unit.SaveUnit() == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Added";
                                clear();
                                bttnSave.Text = "Save (F2)";
                                frmB_BackOfzUnit ItemCat = frmB_BackOfzUnit.Instance;
                                ItemCat.LoadDataGrid();

                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Unit Details";
                            bttnSave.Text = "Save (F2)";
                        }


                    }


                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }


        //update Category
        public void updateSubCategory()
        {
            try
            {

                if (validate())
                {


                    if (isEditUnit == true)
                    {
                        //Check Count For Update
                        if (unit.CheckDuplicateUnitUpdate(txtUnitName.Text.Trim(), txtUnitSymbol.Text.Trim(), isEditUnit) is true)
                        {
                            if (unit.SaveUnit(unitId) == true)
                            {
                                lblMsg.Visible = true;
                                lblMsg.ForeColor = Color.FromArgb(0, 136, 17);
                                lblMsg.Text = "Successfully Updated";
                                clear();
                                frmB_BackOfzUnit ItemCat = frmB_BackOfzUnit.Instance;
                                ItemCat.LoadDataGrid();
                                bttnSave.Text = "Save (F2)";
                            }
                        }

                        else
                        {
                            lblMsg.Visible = true;
                            lblMsg.Text = "Cannot Add  Duplicate Category Details";
                            bttnSave.Text = "Update(F2)";
                            return;
                        }

                    }

                    else
                    {
                        MessageBox.Show("Error");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());
            }
        }


        //validate text Boxes
        public bool validate()
        {

            if (string.IsNullOrEmpty(cmbUnitGroup.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Select Your Unit Group To Continue";
                txtUnitName.Focus();
                return false;

            }


            if (string.IsNullOrEmpty(txtUnitName.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your UnitName Code To Continue";
                txtUnitName.Focus();
                return false;

            }

            if (string.IsNullOrEmpty(txtUnitSymbol.Text.ToString()))
            {
                lblMsg.Visible = true;
                lblMsg.ForeColor = Color.FromArgb(164, 42, 16);
                lblMsg.Text = "Please Enter Your Unit Description To Continue";
                txtUnitSymbol.Focus();
                return false;
            }
            else
            {
                return true;
            }

            return false;
        }


        void loadUnit()
        {
            units = unit.GetUnit(unitId);
            if (units.Rows.Count > 0)
            {
                cmbUnitGroup.SelectedValue = units.Rows[0]["name"].ToString();
                txtUnitName.Text = units.Rows[0]["name"].ToString();
                txtUnitSymbol.Text = units.Rows[0]["description"].ToString();
            }
        }

        private void FrmB_BackOfzAddUnit_Load(object sender, EventArgs e)
        {
            LoadUnitGroup();

            if (isEditUnit)
            {
                loadUnit();
                bttnSave.Text = "Update (F2)";
            }
        }


        private void LoadUnitGroup()
        {

            DataTable dt = unitgroup.GetAllUnitGroup();
            cmbUnitGroup.DataSource = dt;
            cmbUnitGroup.DisplayMember = "group_name";
            cmbUnitGroup.ValueMember = "id";
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            isEditUnit = false;
            this.Close();
            
        }
    }
}
