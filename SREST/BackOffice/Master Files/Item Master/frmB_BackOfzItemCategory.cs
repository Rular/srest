﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST.BackOffice.MasterClasses;

namespace SREST
{
    public partial class frmB_BackOfzItemCategory : frmC_ModelBlack
    {

        cls_ItemCategory cat = new cls_ItemCategory();
        public int CatCode;


        public bool IsEdit = false;
        public frmB_BackOfzItemCategory()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzItemCategory singleton = null;

        public static frmB_BackOfzItemCategory Instance
        {
            get
            {
                if (frmB_BackOfzItemCategory.singleton == null)
                {
                    frmB_BackOfzItemCategory.singleton = new frmB_BackOfzItemCategory();
                }
                return frmB_BackOfzItemCategory.singleton;
            }
        }

        private void FrmB_BackOfzItemCategory_Load(object sender, EventArgs e)
        {
            //load grid Form Load
            LoadDataGrid();
        }


        //Load Grid to Data Funtion
        public void LoadDataGrid()
        {
            DataTable dtCategory = cat.GetAllCategory();

            if (dtCategory.Columns.Count >= 1)
                dgwCategories.DataSource = dtCategory;

            // Grid Decoration
            dgwCategories.Columns[2].Visible = false;
            dgwCategories.Columns[0].Width = 150;
            dgwCategories.Columns[0].HeaderText = "Category";
            dgwCategories.Columns[1].Width = 350;
            dgwCategories.Columns[1].HeaderText = "Description";
            SearchBar.SetColumns(dgwCategories.Columns);
        }

        private void FrmB_BackOfzItemCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzItemCategory.singleton = null;
        }

        private void BttnNewCategory_Click(object sender, EventArgs e)
        {

            new frmB_BackOfzAddCategory().ShowDialog();
        }

        private void DgwCategories_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgwCategories.SelectedCells.Count > 0)
            {
                var CatId = dgwCategories.Rows[e.RowIndex].Cells[0].Value.ToString();
                var Description = dgwCategories.Rows[e.RowIndex].Cells[1].Value.ToString();
            }

        }

        private void DgwCategories_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgwCategories.SelectedCells.Count > 0)
            {
                var CatId = dgwCategories.Rows[e.RowIndex].Cells[0].Value.ToString();
                var Description = dgwCategories.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }

        //Button edit Click
        private void BttnEditCategory_Click(object sender, EventArgs e)
        {

            if (dgwCategories.SelectedRows.Count > 0)
            {
                IsEdit = true;

                frmB_BackOfzAddCategory.GetEditDetails(IsEdit, int.Parse(dgwCategories.CurrentRow.Cells[2].FormattedValue.ToString()));
                new frmB_BackOfzAddCategory().ShowDialog();
            }

            else
                MessageBox.Show("Please Select Full Row To Continue");

        }


        //Delete button Click
        private void BttnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgwCategories.SelectedRows.Count > 0)
                {
                    CatCode = int.Parse(dgwCategories.CurrentRow.Cells[2].FormattedValue.ToString());
                }
                else
                {
                    MessageBox.Show("Please Select Category to Continue");
                    return;
                }


                if (dgwCategories.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Are You Sure You Want Delete This Category?", Application.CompanyName + " " + Application.ProductName + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (cat.deleteCategory(CatCode) == true)
                        {
                            MessageBox.Show("Success Fully Deleted");
                            frmB_BackOfzItemCategory ItemCat = frmB_BackOfzItemCategory.Instance;
                            ItemCat.LoadDataGrid();

                        }
                        else
                        {
                            MessageBox.Show("Error");
                        }
                    }

                }

                else
                {
                    MessageBox.Show("Please Select Full Row To Continue");
                }
            }
            catch (Exception EX)
            {

                MessageBox.Show("This Category Use Other Products Unable To Delete");
            }


        }

        private void DgwCategories_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // CatCode = int.Parse(dgwCategories.Rows[e.RowIndex].Cells[2].FormattedValue.ToString());
        }


        //DataGrid search Method
        private void SearchBar_Search(object sender, ADGV.SearchToolBarSearchEventArgs e)
        {
            if (dgwCategories.ColumnCount > 0 && dgwCategories.RowCount > 0)
            {
                bool restartsearch = true;
                int startColumn = 0;
                int startRow = 0;
                if (!e.FromBegin)
                {
                    bool endcol = dgwCategories.CurrentCell.ColumnIndex + 1 >= dgwCategories.ColumnCount;
                    bool endrow = dgwCategories.CurrentCell.RowIndex + 1 >= dgwCategories.RowCount;

                    if (endcol && endrow)
                    {
                        startColumn = dgwCategories.CurrentCell.ColumnIndex;
                        startRow = dgwCategories.CurrentCell.RowIndex;
                    }
                    else
                    {
                        startColumn = endcol ? 0 : dgwCategories.CurrentCell.ColumnIndex + 1;
                        startRow = dgwCategories.CurrentCell.RowIndex + (endcol ? 1 : 0);
                    }
                }
                DataGridViewCell c = dgwCategories.FindCell(
                    e.ValueToSearch,
                    e.ColumnToSearch != null ? e.ColumnToSearch.Name : null,
                    startRow,
                    startColumn,
                    e.WholeWord,
                    e.CaseSensitive);
                if (c == null && restartsearch)
                    c = dgwCategories.FindCell(
                        e.ValueToSearch,
                        e.ColumnToSearch != null ? e.ColumnToSearch.Name : null,
                        0,
                        0,
                        e.WholeWord,
                        e.CaseSensitive);
                if (c != null)
                    dgwCategories.CurrentCell = c;
            }
        }

        private void DgwCategories_FilterStringChanged(object sender, EventArgs e)
        {
            (dgwCategories.DataSource as DataTable).DefaultView.RowFilter = this.dgwCategories.FilterString;

        }

        private void DgwCategories_SortStringChanged(object sender, EventArgs e)
        {
            (dgwCategories.DataSource as DataTable).DefaultView.Sort = this.dgwCategories.SortString;
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void SearchBar_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void DgwCategories_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemCategory.singleton = null;
            this.Close();
        }
    }
}
