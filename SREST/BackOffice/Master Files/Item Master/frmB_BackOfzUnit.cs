﻿using SREST.BackOffice.Master_Files.MasterClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzUnit : frmC_ModelBlack
    {

        clsm_Unit unit = new clsm_Unit();

        public bool IsEdit = false;
        public int Unitid;
        public frmB_BackOfzUnit()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzUnit singleton = null;

        public static frmB_BackOfzUnit Instance
        {
            get
            {
                if (frmB_BackOfzUnit.singleton == null)
                {
                    frmB_BackOfzUnit.singleton = new frmB_BackOfzUnit();
                }
                return frmB_BackOfzUnit.singleton;
            }
        }

        private void FrmB_BackOfzUnit_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }


        //Load Grid to Data Funtion
        public void LoadDataGrid()
        {
            DataTable dtUnit = unit.GetAllUnit();

            if (dtUnit.Columns.Count >= 1)
                dgwUnit.DataSource = dtUnit;

            dgwUnit.Columns[4].Visible = false;


        }

        private void FrmB_BackOfzUnit_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzUnit.singleton = null;
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnit.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddUnit().ShowDialog();
        }

        private void BttnEditCategory_Click(object sender, EventArgs e)
        {
            if (dgwUnit.SelectedRows.Count > 0)
            {
                IsEdit = true;

                frmB_BackOfzAddUnit.GetEditDetails(IsEdit, int.Parse(dgwUnit.CurrentRow.Cells[4].FormattedValue.ToString()));
                new frmB_BackOfzAddUnit().ShowDialog();
            }

            else
                MessageBox.Show("Please Select Full Row To Continue");
        }

        private void BttnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgwUnit.SelectedRows.Count > 0)
                {
                    Unitid = int.Parse(dgwUnit.CurrentRow.Cells[4].FormattedValue.ToString());
                }
                else
                {
                    MessageBox.Show("Please Select Unit to Continue");
                    return;
                }


                if (dgwUnit.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Are You Sure You Want Delete This Unit?", Application.CompanyName + " " + Application.ProductName + Application.ProductVersion, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (unit.deleteUnit(Unitid) == true)
                        {
                            MessageBox.Show("Success Fully Deleted");
                            frmB_BackOfzUnit ItemCat = frmB_BackOfzUnit.Instance;
                            ItemCat.LoadDataGrid();

                        }
                        else
                        {
                            MessageBox.Show("Error");
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Please Select Full Row To Continue");
                }
            }

            catch (Exception EX)
            {

                MessageBox.Show("This SubCategory Use Other Products Unable To Delete");
            }

        }
    
    }
}
