﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzItem : frmC_ModelBlack
    {
        public frmB_BackOfzItem()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzItem singleton = null;

        public static frmB_BackOfzItem Instance
        {
            get
            {
                if (frmB_BackOfzItem.singleton == null)
                {
                    frmB_BackOfzItem.singleton = new frmB_BackOfzItem();
                }
                return frmB_BackOfzItem.singleton;
            }
        }

        private void FrmB_BackOfzItem_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzItem.singleton = null;

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItem.singleton = null;
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }


            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddItemType().Show();

        }
    }
}
