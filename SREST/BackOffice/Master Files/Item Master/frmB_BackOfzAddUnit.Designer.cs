﻿namespace SREST
{
    partial class frmB_BackOfzAddUnit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.cmbUnitGroup = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.txtUnitName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.txtUnitSymbol = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Unit";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // cmbUnitGroup
            // 
            this.cmbUnitGroup.FormattingEnabled = true;
            this.cmbUnitGroup.ItemHeight = 23;
            this.cmbUnitGroup.Location = new System.Drawing.Point(36, 235);
            this.cmbUnitGroup.Name = "cmbUnitGroup";
            this.cmbUnitGroup.Size = new System.Drawing.Size(397, 29);
            this.cmbUnitGroup.TabIndex = 751;
            this.cmbUnitGroup.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(36, 213);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(89, 19);
            this.metroLabel3.TabIndex = 761;
            this.metroLabel3.Text = "Unit Group  :";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 760;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(35, 285);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(398, 23);
            this.lblMsg.TabIndex = 759;
            this.lblMsg.Text = "Message";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(228, 322);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 755;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            this.bttnClear.Click += new System.EventHandler(this.BttnClear_Click);
            // 
            // txtUnitName
            // 
            // 
            // 
            // 
            this.txtUnitName.CustomButton.Image = null;
            this.txtUnitName.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtUnitName.CustomButton.Name = "";
            this.txtUnitName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUnitName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUnitName.CustomButton.TabIndex = 1;
            this.txtUnitName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUnitName.CustomButton.UseSelectable = true;
            this.txtUnitName.CustomButton.Visible = false;
            this.txtUnitName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtUnitName.Lines = new string[0];
            this.txtUnitName.Location = new System.Drawing.Point(36, 91);
            this.txtUnitName.MaxLength = 32767;
            this.txtUnitName.Name = "txtUnitName";
            this.txtUnitName.PasswordChar = '\0';
            this.txtUnitName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitName.SelectedText = "";
            this.txtUnitName.SelectionLength = 0;
            this.txtUnitName.SelectionStart = 0;
            this.txtUnitName.ShortcutsEnabled = true;
            this.txtUnitName.Size = new System.Drawing.Size(397, 23);
            this.txtUnitName.TabIndex = 752;
            this.txtUnitName.UseSelectable = true;
            this.txtUnitName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUnitName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(36, 63);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(82, 19);
            this.metroLabel1.TabIndex = 757;
            this.metroLabel1.Text = "Unit Name :";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // bttnSave
            // 
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(335, 322);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 44);
            this.bttnSave.TabIndex = 756;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            this.bttnSave.Click += new System.EventHandler(this.BttnSave_Click);
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(121, 322);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 754;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // txtUnitSymbol
            // 
            // 
            // 
            // 
            this.txtUnitSymbol.CustomButton.Image = null;
            this.txtUnitSymbol.CustomButton.Location = new System.Drawing.Point(375, 1);
            this.txtUnitSymbol.CustomButton.Name = "";
            this.txtUnitSymbol.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUnitSymbol.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUnitSymbol.CustomButton.TabIndex = 1;
            this.txtUnitSymbol.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUnitSymbol.CustomButton.UseSelectable = true;
            this.txtUnitSymbol.CustomButton.Visible = false;
            this.txtUnitSymbol.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtUnitSymbol.Lines = new string[0];
            this.txtUnitSymbol.Location = new System.Drawing.Point(36, 164);
            this.txtUnitSymbol.MaxLength = 32767;
            this.txtUnitSymbol.Name = "txtUnitSymbol";
            this.txtUnitSymbol.PasswordChar = '\0';
            this.txtUnitSymbol.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitSymbol.SelectedText = "";
            this.txtUnitSymbol.SelectionLength = 0;
            this.txtUnitSymbol.SelectionStart = 0;
            this.txtUnitSymbol.ShortcutsEnabled = true;
            this.txtUnitSymbol.Size = new System.Drawing.Size(397, 23);
            this.txtUnitSymbol.TabIndex = 762;
            this.txtUnitSymbol.UseSelectable = true;
            this.txtUnitSymbol.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUnitSymbol.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(36, 135);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(91, 19);
            this.metroLabel4.TabIndex = 763;
            this.metroLabel4.Text = "Unit Symbol :";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // frmB_BackOfzAddUnit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnClose;
            this.ClientSize = new System.Drawing.Size(458, 391);
            this.Controls.Add(this.txtUnitSymbol);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.cmbUnitGroup);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtUnitName);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnSave);
            this.Controls.Add(this.bttnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddUnit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddUnit";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddUnit_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroComboBox cmbUnitGroup;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroTextBox txtUnitName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClose;
        private MetroFramework.Controls.MetroTextBox txtUnitSymbol;
        private MetroFramework.Controls.MetroLabel metroLabel4;
    }
}