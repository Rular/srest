﻿namespace SREST
{
    partial class frmB_BackOfzAddUnitConversion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.cmbFromUnit = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.txtConversionValue = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.cmbUnitGroup = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cmbToUnit = new MetroFramework.Controls.MetroComboBox();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(-1, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(460, 46);
            this.metroPanel1.TabIndex = 774;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(460, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Unit Conversion";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // cmbFromUnit
            // 
            this.cmbFromUnit.FormattingEnabled = true;
            this.cmbFromUnit.ItemHeight = 23;
            this.cmbFromUnit.Location = new System.Drawing.Point(33, 155);
            this.cmbFromUnit.Name = "cmbFromUnit";
            this.cmbFromUnit.Size = new System.Drawing.Size(397, 29);
            this.cmbFromUnit.TabIndex = 765;
            this.cmbFromUnit.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(33, 130);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(78, 19);
            this.metroLabel4.TabIndex = 776;
            this.metroLabel4.Text = "From Unit :";
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(33, 340);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(398, 23);
            this.lblMsg.TabIndex = 773;
            this.lblMsg.Text = "Message";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // bttnClear
            // 
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(223, 383);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 44);
            this.bttnClear.TabIndex = 769;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // txtConversionValue
            // 
            // 
            // 
            // 
            this.txtConversionValue.CustomButton.Image = null;
            this.txtConversionValue.CustomButton.Location = new System.Drawing.Point(367, 1);
            this.txtConversionValue.CustomButton.Name = "";
            this.txtConversionValue.CustomButton.Size = new System.Drawing.Size(29, 29);
            this.txtConversionValue.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtConversionValue.CustomButton.TabIndex = 1;
            this.txtConversionValue.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtConversionValue.CustomButton.UseSelectable = true;
            this.txtConversionValue.CustomButton.Visible = false;
            this.txtConversionValue.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtConversionValue.Lines = new string[0];
            this.txtConversionValue.Location = new System.Drawing.Point(33, 294);
            this.txtConversionValue.MaxLength = 32767;
            this.txtConversionValue.Name = "txtConversionValue";
            this.txtConversionValue.PasswordChar = '\0';
            this.txtConversionValue.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConversionValue.SelectedText = "";
            this.txtConversionValue.SelectionLength = 0;
            this.txtConversionValue.SelectionStart = 0;
            this.txtConversionValue.ShortcutsEnabled = true;
            this.txtConversionValue.Size = new System.Drawing.Size(397, 31);
            this.txtConversionValue.TabIndex = 767;
            this.txtConversionValue.UseSelectable = true;
            this.txtConversionValue.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtConversionValue.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(33, 268);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(122, 19);
            this.metroLabel2.TabIndex = 772;
            this.metroLabel2.Text = "Conversion Value :";
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(33, 197);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(64, 19);
            this.metroLabel1.TabIndex = 771;
            this.metroLabel1.Text = "To Unit : ";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // bttnSave
            // 
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(330, 383);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 44);
            this.bttnSave.TabIndex = 770;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(116, 383);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 44);
            this.bttnClose.TabIndex = 768;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            // 
            // cmbUnitGroup
            // 
            this.cmbUnitGroup.FormattingEnabled = true;
            this.cmbUnitGroup.ItemHeight = 23;
            this.cmbUnitGroup.Location = new System.Drawing.Point(33, 89);
            this.cmbUnitGroup.Name = "cmbUnitGroup";
            this.cmbUnitGroup.Size = new System.Drawing.Size(397, 29);
            this.cmbUnitGroup.TabIndex = 764;
            this.cmbUnitGroup.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(33, 64);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(89, 19);
            this.metroLabel3.TabIndex = 775;
            this.metroLabel3.Text = "Unit Group  :";
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // cmbToUnit
            // 
            this.cmbToUnit.FormattingEnabled = true;
            this.cmbToUnit.ItemHeight = 23;
            this.cmbToUnit.Location = new System.Drawing.Point(33, 223);
            this.cmbToUnit.Name = "cmbToUnit";
            this.cmbToUnit.Size = new System.Drawing.Size(397, 29);
            this.cmbToUnit.TabIndex = 777;
            this.cmbToUnit.UseSelectable = true;
            // 
            // frmB_BackOfzAddUnitConversion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 446);
            this.Controls.Add(this.cmbToUnit);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.cmbFromUnit);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.bttnClear);
            this.Controls.Add(this.txtConversionValue);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.bttnSave);
            this.Controls.Add(this.bttnClose);
            this.Controls.Add(this.cmbUnitGroup);
            this.Controls.Add(this.metroLabel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddUnitConversion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddUnitConversion";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddUnitConversion_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroComboBox cmbFromUnit;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.Label lblMsg;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroTextBox txtConversionValue;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClose;
        private MetroFramework.Controls.MetroComboBox cmbUnitGroup;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cmbToUnit;
    }
}