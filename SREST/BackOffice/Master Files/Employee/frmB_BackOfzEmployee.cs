﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzEmployee : frmC_ModelBlack
    {
        public frmB_BackOfzEmployee()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzEmployee singleton = null;

        public static frmB_BackOfzEmployee Instance
        {
            get
            {
                if (frmB_BackOfzEmployee.singleton == null)
                {
                    frmB_BackOfzEmployee.singleton = new frmB_BackOfzEmployee();
                }
                return frmB_BackOfzEmployee.singleton;
            }
        }

        private void FrmB_BackOfzEmployee_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzEmployee.singleton = null;
            this.Close();
        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddEmployee().ShowDialog();
        }

        private void FrmB_BackOfzEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzEmployee.singleton = null;
        }
    }
}
