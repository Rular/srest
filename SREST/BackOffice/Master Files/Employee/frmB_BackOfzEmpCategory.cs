﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzEmpCategory : frmC_ModelBlack
    {
        public frmB_BackOfzEmpCategory()
        {
            InitializeComponent();
        }


        private static frmB_BackOfzEmpCategory singleton = null;

        public static frmB_BackOfzEmpCategory Instance
        {
            get
            {
                if (frmB_BackOfzEmpCategory.singleton == null)
                {
                    frmB_BackOfzEmpCategory.singleton = new frmB_BackOfzEmpCategory();
                }
                return frmB_BackOfzEmpCategory.singleton;
            }
        }

        private void FrmB_BackOfzEmpCategory_Load(object sender, EventArgs e)
        {

        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_BackOfzAddEmpCategory().ShowDialog();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_BackOfzEmpCategory.singleton = null;
            this.Close();
        }

        private void FrmB_BackOfzEmpCategory_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzEmpCategory.singleton = null;
        }
    }
}
