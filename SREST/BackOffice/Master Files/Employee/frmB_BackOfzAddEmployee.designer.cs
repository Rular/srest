﻿namespace SREST
{
    partial class frmB_BackOfzAddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutSub = new System.Windows.Forms.TableLayoutPanel();
            this.txtNicNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.cmbEmpCategory = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtAddress = new MetroFramework.Controls.MetroTextBox();
            this.txtContactNo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cmbCountry = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.toggleIsWaiter = new MetroFramework.Controls.MetroToggle();
            this.tableLayoutPanelBttns = new System.Windows.Forms.TableLayoutPanel();
            this.bttnSave = new MetroFramework.Controls.MetroButton();
            this.bttnClear = new MetroFramework.Controls.MetroButton();
            this.bttnClose = new MetroFramework.Controls.MetroButton();
            this.lblMsg = new System.Windows.Forms.Label();
            this.txtEmployeeCode = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanelPic = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelSubPic = new System.Windows.Forms.TableLayoutPanel();
            this.PicBoxItem = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanelCamButton = new System.Windows.Forms.TableLayoutPanel();
            this.bttncapture = new MetroFramework.Controls.MetroButton();
            this.bttnstart = new MetroFramework.Controls.MetroButton();
            this.cmbcamlist = new System.Windows.Forms.ComboBox();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutMain.SuspendLayout();
            this.tableLayoutSub.SuspendLayout();
            this.tableLayoutPanelBttns.SuspendLayout();
            this.tableLayoutPanelPic.SuspendLayout();
            this.tableLayoutPanelSubPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).BeginInit();
            this.tableLayoutPanelCamButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.Enabled = false;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(943, 45);
            this.metroPanel1.TabIndex = 731;
            this.metroPanel1.UseCustomBackColor = true;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(943, 45);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Employee Setup";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.UseCustomBackColor = true;
            this.lblTitle.UseCustomForeColor = true;
            // 
            // metroPanel2
            // 
            this.metroPanel2.AutoScroll = true;
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel2.HorizontalScrollbar = true;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 45);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(943, 32);
            this.metroPanel2.TabIndex = 732;
            this.metroPanel2.UseCustomBackColor = true;
            this.metroPanel2.VerticalScrollbar = true;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.AutoScroll = true;
            this.tableLayoutMain.ColumnCount = 2;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutSub, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutPanelPic, 1, 0);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 77);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 1;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutMain.Size = new System.Drawing.Size(943, 523);
            this.tableLayoutMain.TabIndex = 733;
            // 
            // tableLayoutSub
            // 
            this.tableLayoutSub.AutoScroll = true;
            this.tableLayoutSub.ColumnCount = 2;
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.99083F));
            this.tableLayoutSub.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.00917F));
            this.tableLayoutSub.Controls.Add(this.txtNicNumber, 1, 2);
            this.tableLayoutSub.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutSub.Controls.Add(this.cmbEmpCategory, 1, 0);
            this.tableLayoutSub.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutSub.Controls.Add(this.metroLabel3, 0, 2);
            this.tableLayoutSub.Controls.Add(this.metroLabel4, 0, 3);
            this.tableLayoutSub.Controls.Add(this.txtName, 1, 3);
            this.tableLayoutSub.Controls.Add(this.metroLabel5, 0, 4);
            this.tableLayoutSub.Controls.Add(this.txtAddress, 1, 4);
            this.tableLayoutSub.Controls.Add(this.txtContactNo, 1, 5);
            this.tableLayoutSub.Controls.Add(this.metroLabel6, 0, 5);
            this.tableLayoutSub.Controls.Add(this.metroLabel7, 0, 6);
            this.tableLayoutSub.Controls.Add(this.cmbCountry, 1, 6);
            this.tableLayoutSub.Controls.Add(this.metroLabel12, 0, 7);
            this.tableLayoutSub.Controls.Add(this.toggleIsWaiter, 1, 7);
            this.tableLayoutSub.Controls.Add(this.tableLayoutPanelBttns, 1, 8);
            this.tableLayoutSub.Controls.Add(this.lblMsg, 1, 9);
            this.tableLayoutSub.Controls.Add(this.txtEmployeeCode, 1, 1);
            this.tableLayoutSub.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutSub.Name = "tableLayoutSub";
            this.tableLayoutSub.RowCount = 11;
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.183995F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.92F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.44F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.92F));
            this.tableLayoutSub.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.16667F));
            this.tableLayoutSub.Size = new System.Drawing.Size(545, 625);
            this.tableLayoutSub.TabIndex = 0;
            // 
            // txtNicNumber
            // 
            this.txtNicNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtNicNumber.CustomButton.Image = null;
            this.txtNicNumber.CustomButton.Location = new System.Drawing.Point(347, 1);
            this.txtNicNumber.CustomButton.Name = "";
            this.txtNicNumber.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.txtNicNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNicNumber.CustomButton.TabIndex = 1;
            this.txtNicNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNicNumber.CustomButton.UseSelectable = true;
            this.txtNicNumber.CustomButton.Visible = false;
            this.txtNicNumber.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtNicNumber.Lines = new string[0];
            this.txtNicNumber.Location = new System.Drawing.Point(161, 110);
            this.txtNicNumber.MaxLength = 32767;
            this.txtNicNumber.Name = "txtNicNumber";
            this.txtNicNumber.PasswordChar = '\0';
            this.txtNicNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNicNumber.SelectedText = "";
            this.txtNicNumber.SelectionLength = 0;
            this.txtNicNumber.SelectionStart = 0;
            this.txtNicNumber.ShortcutsEnabled = true;
            this.txtNicNumber.Size = new System.Drawing.Size(381, 35);
            this.txtNicNumber.TabIndex = 774;
            this.txtNicNumber.UseSelectable = true;
            this.txtNicNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNicNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(152, 51);
            this.metroLabel1.TabIndex = 746;
            this.metroLabel1.Text = "Category ";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // cmbEmpCategory
            // 
            this.cmbEmpCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbEmpCategory.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbEmpCategory.FormattingEnabled = true;
            this.cmbEmpCategory.ItemHeight = 29;
            this.cmbEmpCategory.Location = new System.Drawing.Point(161, 8);
            this.cmbEmpCategory.Name = "cmbEmpCategory";
            this.cmbEmpCategory.Size = new System.Drawing.Size(381, 35);
            this.cmbEmpCategory.TabIndex = 747;
            this.cmbEmpCategory.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel2.Location = new System.Drawing.Point(3, 51);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(152, 51);
            this.metroLabel2.TabIndex = 748;
            this.metroLabel2.Text = "Employee Code";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel2.UseCustomBackColor = true;
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel3.Location = new System.Drawing.Point(3, 102);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(152, 51);
            this.metroLabel3.TabIndex = 750;
            this.metroLabel3.Text = "NIC Number";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel3.UseCustomBackColor = true;
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel4.Location = new System.Drawing.Point(3, 153);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(152, 51);
            this.metroLabel4.TabIndex = 752;
            this.metroLabel4.Text = "Name";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel4.UseCustomBackColor = true;
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtName.CustomButton.Image = null;
            this.txtName.CustomButton.Location = new System.Drawing.Point(347, 1);
            this.txtName.CustomButton.Name = "";
            this.txtName.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.txtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtName.CustomButton.TabIndex = 1;
            this.txtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtName.CustomButton.UseSelectable = true;
            this.txtName.CustomButton.Visible = false;
            this.txtName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtName.Lines = new string[0];
            this.txtName.Location = new System.Drawing.Point(161, 161);
            this.txtName.MaxLength = 32767;
            this.txtName.Name = "txtName";
            this.txtName.PasswordChar = '\0';
            this.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtName.SelectedText = "";
            this.txtName.SelectionLength = 0;
            this.txtName.SelectionStart = 0;
            this.txtName.ShortcutsEnabled = true;
            this.txtName.Size = new System.Drawing.Size(381, 35);
            this.txtName.TabIndex = 753;
            this.txtName.UseSelectable = true;
            this.txtName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel5.Location = new System.Drawing.Point(3, 204);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(152, 51);
            this.metroLabel5.TabIndex = 754;
            this.metroLabel5.Text = "Address";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel5.UseCustomBackColor = true;
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtAddress.CustomButton.Image = null;
            this.txtAddress.CustomButton.Location = new System.Drawing.Point(347, 1);
            this.txtAddress.CustomButton.Name = "";
            this.txtAddress.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.txtAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAddress.CustomButton.TabIndex = 1;
            this.txtAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAddress.CustomButton.UseSelectable = true;
            this.txtAddress.CustomButton.Visible = false;
            this.txtAddress.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtAddress.Lines = new string[0];
            this.txtAddress.Location = new System.Drawing.Point(161, 212);
            this.txtAddress.MaxLength = 32767;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.PasswordChar = '\0';
            this.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAddress.SelectedText = "";
            this.txtAddress.SelectionLength = 0;
            this.txtAddress.SelectionStart = 0;
            this.txtAddress.ShortcutsEnabled = true;
            this.txtAddress.Size = new System.Drawing.Size(381, 35);
            this.txtAddress.TabIndex = 755;
            this.txtAddress.UseSelectable = true;
            this.txtAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtContactNo
            // 
            this.txtContactNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtContactNo.CustomButton.Image = null;
            this.txtContactNo.CustomButton.Location = new System.Drawing.Point(347, 1);
            this.txtContactNo.CustomButton.Name = "";
            this.txtContactNo.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.txtContactNo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtContactNo.CustomButton.TabIndex = 1;
            this.txtContactNo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtContactNo.CustomButton.UseSelectable = true;
            this.txtContactNo.CustomButton.Visible = false;
            this.txtContactNo.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtContactNo.Lines = new string[0];
            this.txtContactNo.Location = new System.Drawing.Point(161, 263);
            this.txtContactNo.MaxLength = 32767;
            this.txtContactNo.Name = "txtContactNo";
            this.txtContactNo.PasswordChar = '\0';
            this.txtContactNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtContactNo.SelectedText = "";
            this.txtContactNo.SelectionLength = 0;
            this.txtContactNo.SelectionStart = 0;
            this.txtContactNo.ShortcutsEnabled = true;
            this.txtContactNo.Size = new System.Drawing.Size(381, 35);
            this.txtContactNo.TabIndex = 757;
            this.txtContactNo.UseSelectable = true;
            this.txtContactNo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtContactNo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel6.Location = new System.Drawing.Point(3, 255);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(152, 51);
            this.metroLabel6.TabIndex = 756;
            this.metroLabel6.Text = "Contact No";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel6.UseCustomBackColor = true;
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel7.Location = new System.Drawing.Point(3, 306);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(152, 51);
            this.metroLabel7.TabIndex = 758;
            this.metroLabel7.Text = "Country";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel7.UseCustomBackColor = true;
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // cmbCountry
            // 
            this.cmbCountry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCountry.DropDownHeight = 200;
            this.cmbCountry.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.IntegralHeight = false;
            this.cmbCountry.ItemHeight = 29;
            this.cmbCountry.Location = new System.Drawing.Point(161, 314);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(381, 35);
            this.cmbCountry.TabIndex = 759;
            this.cmbCountry.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel12.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel12.ForeColor = System.Drawing.SystemColors.Desktop;
            this.metroLabel12.Location = new System.Drawing.Point(3, 357);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(152, 62);
            this.metroLabel12.TabIndex = 768;
            this.metroLabel12.Text = "Is Waiter";
            this.metroLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel12.UseCustomBackColor = true;
            this.metroLabel12.UseCustomForeColor = true;
            // 
            // toggleIsWaiter
            // 
            this.toggleIsWaiter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.toggleIsWaiter.AutoSize = true;
            this.toggleIsWaiter.Location = new System.Drawing.Point(161, 379);
            this.toggleIsWaiter.Name = "toggleIsWaiter";
            this.toggleIsWaiter.Size = new System.Drawing.Size(80, 17);
            this.toggleIsWaiter.TabIndex = 769;
            this.toggleIsWaiter.Text = "Off";
            this.toggleIsWaiter.UseCustomBackColor = true;
            this.toggleIsWaiter.UseSelectable = true;
            // 
            // tableLayoutPanelBttns
            // 
            this.tableLayoutPanelBttns.AutoScroll = true;
            this.tableLayoutPanelBttns.ColumnCount = 3;
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBttns.Controls.Add(this.bttnSave, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClear, 0, 0);
            this.tableLayoutPanelBttns.Controls.Add(this.bttnClose, 0, 0);
            this.tableLayoutPanelBttns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBttns.Location = new System.Drawing.Point(161, 422);
            this.tableLayoutPanelBttns.Name = "tableLayoutPanelBttns";
            this.tableLayoutPanelBttns.RowCount = 1;
            this.tableLayoutPanelBttns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBttns.Size = new System.Drawing.Size(381, 53);
            this.tableLayoutPanelBttns.TabIndex = 772;
            // 
            // bttnSave
            // 
            this.bttnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnSave.Location = new System.Drawing.Point(221, 3);
            this.bttnSave.Name = "bttnSave";
            this.bttnSave.Size = new System.Drawing.Size(103, 47);
            this.bttnSave.TabIndex = 7;
            this.bttnSave.Text = "&Save ( F2 )";
            this.bttnSave.UseCustomBackColor = true;
            this.bttnSave.UseCustomForeColor = true;
            this.bttnSave.UseSelectable = true;
            // 
            // bttnClear
            // 
            this.bttnClear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClear.Location = new System.Drawing.Point(112, 3);
            this.bttnClear.Name = "bttnClear";
            this.bttnClear.Size = new System.Drawing.Size(103, 47);
            this.bttnClear.TabIndex = 6;
            this.bttnClear.Text = "Clear";
            this.bttnClear.UseCustomBackColor = true;
            this.bttnClear.UseCustomForeColor = true;
            this.bttnClear.UseSelectable = true;
            // 
            // bttnClose
            // 
            this.bttnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnClose.Location = new System.Drawing.Point(3, 3);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(103, 47);
            this.bttnClose.TabIndex = 5;
            this.bttnClose.Text = "Close";
            this.bttnClose.UseCustomBackColor = true;
            this.bttnClose.UseCustomForeColor = true;
            this.bttnClose.UseSelectable = true;
            // 
            // lblMsg
            // 
            this.lblMsg.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMsg.Location = new System.Drawing.Point(161, 478);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(368, 22);
            this.lblMsg.TabIndex = 773;
            this.lblMsg.Text = "Messege";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // txtEmployeeCode
            // 
            this.txtEmployeeCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtEmployeeCode.CustomButton.Image = null;
            this.txtEmployeeCode.CustomButton.Location = new System.Drawing.Point(347, 1);
            this.txtEmployeeCode.CustomButton.Name = "";
            this.txtEmployeeCode.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.txtEmployeeCode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtEmployeeCode.CustomButton.TabIndex = 1;
            this.txtEmployeeCode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtEmployeeCode.CustomButton.UseSelectable = true;
            this.txtEmployeeCode.CustomButton.Visible = false;
            this.txtEmployeeCode.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtEmployeeCode.Lines = new string[0];
            this.txtEmployeeCode.Location = new System.Drawing.Point(161, 59);
            this.txtEmployeeCode.MaxLength = 32767;
            this.txtEmployeeCode.Name = "txtEmployeeCode";
            this.txtEmployeeCode.PasswordChar = '\0';
            this.txtEmployeeCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmployeeCode.SelectedText = "";
            this.txtEmployeeCode.SelectionLength = 0;
            this.txtEmployeeCode.SelectionStart = 0;
            this.txtEmployeeCode.ShortcutsEnabled = true;
            this.txtEmployeeCode.Size = new System.Drawing.Size(381, 35);
            this.txtEmployeeCode.TabIndex = 751;
            this.txtEmployeeCode.UseSelectable = true;
            this.txtEmployeeCode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtEmployeeCode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tableLayoutPanelPic
            // 
            this.tableLayoutPanelPic.AutoScroll = true;
            this.tableLayoutPanelPic.ColumnCount = 1;
            this.tableLayoutPanelPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelPic.Controls.Add(this.tableLayoutPanelSubPic, 0, 0);
            this.tableLayoutPanelPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelPic.Location = new System.Drawing.Point(554, 3);
            this.tableLayoutPanelPic.Name = "tableLayoutPanelPic";
            this.tableLayoutPanelPic.RowCount = 2;
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.41958F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.58042F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPic.Size = new System.Drawing.Size(386, 720);
            this.tableLayoutPanelPic.TabIndex = 1;
            // 
            // tableLayoutPanelSubPic
            // 
            this.tableLayoutPanelSubPic.AutoScroll = true;
            this.tableLayoutPanelSubPic.ColumnCount = 3;
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.57627F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.42373F));
            this.tableLayoutPanelSubPic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanelSubPic.Controls.Add(this.PicBoxItem, 1, 0);
            this.tableLayoutPanelSubPic.Controls.Add(this.tableLayoutPanelCamButton, 1, 1);
            this.tableLayoutPanelSubPic.Controls.Add(this.cmbcamlist, 1, 2);
            this.tableLayoutPanelSubPic.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelSubPic.Name = "tableLayoutPanelSubPic";
            this.tableLayoutPanelSubPic.RowCount = 4;
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 224F));
            this.tableLayoutPanelSubPic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelSubPic.Size = new System.Drawing.Size(380, 503);
            this.tableLayoutPanelSubPic.TabIndex = 0;
            // 
            // PicBoxItem
            // 
            this.PicBoxItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PicBoxItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicBoxItem.Location = new System.Drawing.Point(109, 3);
            this.PicBoxItem.Name = "PicBoxItem";
            this.PicBoxItem.Size = new System.Drawing.Size(195, 204);
            this.PicBoxItem.TabIndex = 11;
            this.PicBoxItem.TabStop = false;
            // 
            // tableLayoutPanelCamButton
            // 
            this.tableLayoutPanelCamButton.AutoScroll = true;
            this.tableLayoutPanelCamButton.ColumnCount = 2;
            this.tableLayoutPanelCamButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCamButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCamButton.Controls.Add(this.bttncapture, 0, 0);
            this.tableLayoutPanelCamButton.Controls.Add(this.bttnstart, 0, 0);
            this.tableLayoutPanelCamButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCamButton.Location = new System.Drawing.Point(109, 213);
            this.tableLayoutPanelCamButton.Name = "tableLayoutPanelCamButton";
            this.tableLayoutPanelCamButton.RowCount = 1;
            this.tableLayoutPanelCamButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCamButton.Size = new System.Drawing.Size(195, 33);
            this.tableLayoutPanelCamButton.TabIndex = 12;
            // 
            // bttncapture
            // 
            this.bttncapture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttncapture.Location = new System.Drawing.Point(100, 3);
            this.bttncapture.Name = "bttncapture";
            this.bttncapture.Size = new System.Drawing.Size(92, 27);
            this.bttncapture.TabIndex = 67;
            this.bttncapture.Text = "Capture";
            this.bttncapture.UseSelectable = true;
            // 
            // bttnstart
            // 
            this.bttnstart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnstart.Location = new System.Drawing.Point(3, 3);
            this.bttnstart.Name = "bttnstart";
            this.bttnstart.Size = new System.Drawing.Size(91, 27);
            this.bttnstart.TabIndex = 66;
            this.bttnstart.Text = "Start";
            this.bttnstart.UseSelectable = true;
            // 
            // cmbcamlist
            // 
            this.cmbcamlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbcamlist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcamlist.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcamlist.FormattingEnabled = true;
            this.cmbcamlist.Location = new System.Drawing.Point(109, 252);
            this.cmbcamlist.Name = "cmbcamlist";
            this.cmbcamlist.Size = new System.Drawing.Size(195, 26);
            this.cmbcamlist.TabIndex = 68;
            // 
            // frmB_BackOfzAddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnClose;
            this.ClientSize = new System.Drawing.Size(943, 600);
            this.Controls.Add(this.tableLayoutMain);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmB_BackOfzAddEmployee";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmB_BackOfzAddEmployee";
            this.Load += new System.EventHandler(this.FrmB_BackOfzAddEmployee_Load);
            this.metroPanel1.ResumeLayout(false);
            this.tableLayoutMain.ResumeLayout(false);
            this.tableLayoutSub.ResumeLayout(false);
            this.tableLayoutSub.PerformLayout();
            this.tableLayoutPanelBttns.ResumeLayout(false);
            this.tableLayoutPanelPic.ResumeLayout(false);
            this.tableLayoutPanelSubPic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxItem)).EndInit();
            this.tableLayoutPanelCamButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSub;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cmbEmpCategory;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtEmployeeCode;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtName;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtAddress;
        private MetroFramework.Controls.MetroTextBox txtContactNo;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroComboBox cmbCountry;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPic;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSubPic;
        internal System.Windows.Forms.PictureBox PicBoxItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCamButton;
        private MetroFramework.Controls.MetroButton bttncapture;
        private MetroFramework.Controls.MetroButton bttnstart;
        private System.Windows.Forms.ComboBox cmbcamlist;
        private MetroFramework.Controls.MetroTextBox txtNicNumber;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroToggle toggleIsWaiter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelBttns;
        private MetroFramework.Controls.MetroButton bttnSave;
        private MetroFramework.Controls.MetroButton bttnClear;
        private MetroFramework.Controls.MetroButton bttnClose;
    }
}