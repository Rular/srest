﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzAddEmployee : Form
    {
        public frmB_BackOfzAddEmployee()
        {
            InitializeComponent();
        }


        public  static List<string> GetAllCountrysNames()
        {
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            var rez = cultures.Select(cult => (new RegionInfo(cult.LCID)).DisplayName).Distinct().OrderBy(q => q).ToList();

            return rez;
        }

        private void FrmB_BackOfzAddEmployee_Load(object sender, EventArgs e)
        {
            cmbCountry.DataSource = GetAllCountrysNames();
            
        }
    }
}
