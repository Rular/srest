﻿namespace SREST
{
    partial class frmB_BackOfzStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzStock));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileLocations = new MetroFramework.Controls.MetroTile();
            this.TileOpeningStock = new MetroFramework.Controls.MetroTile();
            this.TilePurchaseOrder = new MetroFramework.Controls.MetroTile();
            this.TileGRN = new MetroFramework.Controls.MetroTile();
            this.TilePRN = new MetroFramework.Controls.MetroTile();
            this.TileRequisition = new MetroFramework.Controls.MetroTile();
            this.TileStockTrnsOut = new MetroFramework.Controls.MetroTile();
            this.TileStockTransIn = new MetroFramework.Controls.MetroTile();
            this.TileStockAdjusment = new MetroFramework.Controls.MetroTile();
            this.TileBarcodePrint = new MetroFramework.Controls.MetroTile();
            this.button1 = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.TileLocations);
            this.flowLayoutPanel1.Controls.Add(this.TileOpeningStock);
            this.flowLayoutPanel1.Controls.Add(this.TilePurchaseOrder);
            this.flowLayoutPanel1.Controls.Add(this.TileGRN);
            this.flowLayoutPanel1.Controls.Add(this.TilePRN);
            this.flowLayoutPanel1.Controls.Add(this.TileRequisition);
            this.flowLayoutPanel1.Controls.Add(this.TileStockTrnsOut);
            this.flowLayoutPanel1.Controls.Add(this.TileStockTransIn);
            this.flowLayoutPanel1.Controls.Add(this.TileStockAdjusment);
            this.flowLayoutPanel1.Controls.Add(this.TileBarcodePrint);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 125);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(767, 418);
            this.flowLayoutPanel1.TabIndex = 719;
            // 
            // TileLocations
            // 
            this.TileLocations.ActiveControl = null;
            this.TileLocations.Location = new System.Drawing.Point(3, 3);
            this.TileLocations.Name = "TileLocations";
            this.TileLocations.Size = new System.Drawing.Size(176, 94);
            this.TileLocations.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileLocations.TabIndex = 2;
            this.TileLocations.Text = "Locations";
            this.TileLocations.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileLocations.TileImage = ((System.Drawing.Image)(resources.GetObject("TileLocations.TileImage")));
            this.TileLocations.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileLocations.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileLocations.UseSelectable = true;
            this.TileLocations.UseTileImage = true;
            // 
            // TileOpeningStock
            // 
            this.TileOpeningStock.ActiveControl = null;
            this.TileOpeningStock.Location = new System.Drawing.Point(185, 3);
            this.TileOpeningStock.Name = "TileOpeningStock";
            this.TileOpeningStock.Size = new System.Drawing.Size(176, 94);
            this.TileOpeningStock.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileOpeningStock.TabIndex = 4;
            this.TileOpeningStock.Text = " Opening Stock";
            this.TileOpeningStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileOpeningStock.TileImage = ((System.Drawing.Image)(resources.GetObject("TileOpeningStock.TileImage")));
            this.TileOpeningStock.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileOpeningStock.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileOpeningStock.UseSelectable = true;
            this.TileOpeningStock.UseTileImage = true;
            // 
            // TilePurchaseOrder
            // 
            this.TilePurchaseOrder.ActiveControl = null;
            this.TilePurchaseOrder.Location = new System.Drawing.Point(367, 3);
            this.TilePurchaseOrder.Name = "TilePurchaseOrder";
            this.TilePurchaseOrder.Size = new System.Drawing.Size(176, 94);
            this.TilePurchaseOrder.Style = MetroFramework.MetroColorStyle.Silver;
            this.TilePurchaseOrder.TabIndex = 5;
            this.TilePurchaseOrder.Text = "Purchase Order";
            this.TilePurchaseOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TilePurchaseOrder.TileImage = ((System.Drawing.Image)(resources.GetObject("TilePurchaseOrder.TileImage")));
            this.TilePurchaseOrder.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TilePurchaseOrder.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TilePurchaseOrder.UseSelectable = true;
            this.TilePurchaseOrder.UseTileImage = true;
            // 
            // TileGRN
            // 
            this.TileGRN.ActiveControl = null;
            this.TileGRN.Location = new System.Drawing.Point(549, 3);
            this.TileGRN.Name = "TileGRN";
            this.TileGRN.Size = new System.Drawing.Size(176, 94);
            this.TileGRN.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileGRN.TabIndex = 6;
            this.TileGRN.Text = "GRN";
            this.TileGRN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileGRN.TileImage = ((System.Drawing.Image)(resources.GetObject("TileGRN.TileImage")));
            this.TileGRN.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileGRN.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileGRN.UseSelectable = true;
            this.TileGRN.UseTileImage = true;
            // 
            // TilePRN
            // 
            this.TilePRN.ActiveControl = null;
            this.TilePRN.Location = new System.Drawing.Point(3, 103);
            this.TilePRN.Name = "TilePRN";
            this.TilePRN.Size = new System.Drawing.Size(176, 94);
            this.TilePRN.Style = MetroFramework.MetroColorStyle.Silver;
            this.TilePRN.TabIndex = 7;
            this.TilePRN.Text = "PRN";
            this.TilePRN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TilePRN.TileImage = ((System.Drawing.Image)(resources.GetObject("TilePRN.TileImage")));
            this.TilePRN.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TilePRN.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TilePRN.UseSelectable = true;
            this.TilePRN.UseTileImage = true;
            // 
            // TileRequisition
            // 
            this.TileRequisition.ActiveControl = null;
            this.TileRequisition.Location = new System.Drawing.Point(185, 103);
            this.TileRequisition.Name = "TileRequisition";
            this.TileRequisition.Size = new System.Drawing.Size(176, 94);
            this.TileRequisition.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileRequisition.TabIndex = 8;
            this.TileRequisition.Text = "Requisition";
            this.TileRequisition.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileRequisition.TileImage = ((System.Drawing.Image)(resources.GetObject("TileRequisition.TileImage")));
            this.TileRequisition.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileRequisition.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileRequisition.UseSelectable = true;
            this.TileRequisition.UseTileImage = true;
            // 
            // TileStockTrnsOut
            // 
            this.TileStockTrnsOut.ActiveControl = null;
            this.TileStockTrnsOut.Location = new System.Drawing.Point(367, 103);
            this.TileStockTrnsOut.Name = "TileStockTrnsOut";
            this.TileStockTrnsOut.Size = new System.Drawing.Size(176, 94);
            this.TileStockTrnsOut.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileStockTrnsOut.TabIndex = 9;
            this.TileStockTrnsOut.Text = "Stock Trans Out";
            this.TileStockTrnsOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileStockTrnsOut.TileImage = ((System.Drawing.Image)(resources.GetObject("TileStockTrnsOut.TileImage")));
            this.TileStockTrnsOut.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileStockTrnsOut.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileStockTrnsOut.UseSelectable = true;
            this.TileStockTrnsOut.UseTileImage = true;
            // 
            // TileStockTransIn
            // 
            this.TileStockTransIn.ActiveControl = null;
            this.TileStockTransIn.Location = new System.Drawing.Point(549, 103);
            this.TileStockTransIn.Name = "TileStockTransIn";
            this.TileStockTransIn.Size = new System.Drawing.Size(176, 94);
            this.TileStockTransIn.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileStockTransIn.TabIndex = 10;
            this.TileStockTransIn.Text = "Stock Trans In";
            this.TileStockTransIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileStockTransIn.TileImage = ((System.Drawing.Image)(resources.GetObject("TileStockTransIn.TileImage")));
            this.TileStockTransIn.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileStockTransIn.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileStockTransIn.UseSelectable = true;
            this.TileStockTransIn.UseTileImage = true;
            // 
            // TileStockAdjusment
            // 
            this.TileStockAdjusment.ActiveControl = null;
            this.TileStockAdjusment.Location = new System.Drawing.Point(3, 203);
            this.TileStockAdjusment.Name = "TileStockAdjusment";
            this.TileStockAdjusment.Size = new System.Drawing.Size(176, 94);
            this.TileStockAdjusment.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileStockAdjusment.TabIndex = 11;
            this.TileStockAdjusment.Text = "Stock Adjust";
            this.TileStockAdjusment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileStockAdjusment.TileImage = ((System.Drawing.Image)(resources.GetObject("TileStockAdjusment.TileImage")));
            this.TileStockAdjusment.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileStockAdjusment.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileStockAdjusment.UseSelectable = true;
            this.TileStockAdjusment.UseTileImage = true;
            // 
            // TileBarcodePrint
            // 
            this.TileBarcodePrint.ActiveControl = null;
            this.TileBarcodePrint.Location = new System.Drawing.Point(185, 203);
            this.TileBarcodePrint.Name = "TileBarcodePrint";
            this.TileBarcodePrint.Size = new System.Drawing.Size(176, 94);
            this.TileBarcodePrint.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileBarcodePrint.TabIndex = 12;
            this.TileBarcodePrint.Text = "Barcode Print";
            this.TileBarcodePrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileBarcodePrint.TileImage = ((System.Drawing.Image)(resources.GetObject("TileBarcodePrint.TileImage")));
            this.TileBarcodePrint.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileBarcodePrint.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileBarcodePrint.UseSelectable = true;
            this.TileBarcodePrint.UseTileImage = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gray;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(197, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 62);
            this.button1.TabIndex = 724;
            this.button1.TabStop = false;
            this.button1.Text = " Stock";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 723;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            this.bttnBackOffice.Click += new System.EventHandler(this.BttnBackOffice_Click);
            // 
            // frmB_BackOfzStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 574);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bttnBackOffice);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzStock";
            this.Text = "frmB_BackOfzStock";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmB_BackOfzStock_FormClosing);
            this.Load += new System.EventHandler(this.FrmB_BackOfzStock_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileLocations;
        private MetroFramework.Controls.MetroTile TileOpeningStock;
        private MetroFramework.Controls.MetroTile TilePurchaseOrder;
        private MetroFramework.Controls.MetroTile TileGRN;
        private MetroFramework.Controls.MetroTile TilePRN;
        private MetroFramework.Controls.MetroTile TileRequisition;
        private MetroFramework.Controls.MetroTile TileStockTrnsOut;
        private MetroFramework.Controls.MetroTile TileStockTransIn;
        private MetroFramework.Controls.MetroTile TileStockAdjusment;
        private MetroFramework.Controls.MetroTile TileBarcodePrint;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bttnBackOffice;
    }
}