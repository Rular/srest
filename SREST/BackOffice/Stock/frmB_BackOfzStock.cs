﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzStock : frmC_ModelGreen
    {
        public frmB_BackOfzStock()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzStock singleton = null;

        public static frmB_BackOfzStock Instance
        {
            get
            {
                if (frmB_BackOfzStock.singleton == null)
                {
                    frmB_BackOfzStock.singleton = new frmB_BackOfzStock();
                }
                return frmB_BackOfzStock.singleton;
            }
        }

        private void FrmB_BackOfzStock_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzStock_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzStock.singleton = null;
        }

        private void BttnBackOffice_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMainMenu settings = frmB_BackOfzMainMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }

       
    }
}
