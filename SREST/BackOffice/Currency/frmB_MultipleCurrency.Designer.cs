﻿namespace SREST
{
    partial class frmB_MultipleCurrency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_MultipleCurrency));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bttnDelete = new System.Windows.Forms.Button();
            this.bttnEditCategory = new System.Windows.Forms.Button();
            this.bttnNew = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SearchBar = new ADGV.SearchToolBar();
            this.dgwMultipleCurrency = new ADGV.AdvancedDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwMultipleCurrency)).BeginInit();
            this.SuspendLayout();
            // 
            // bttnClose
            // 
            this.bttnClose.Location = new System.Drawing.Point(911, 0);
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // lblFormCaption
            // 
            this.lblFormCaption.Location = new System.Drawing.Point(663, 0);
            this.lblFormCaption.Text = "frmC_Model";
            // 
            // bttnDelete
            // 
            this.bttnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(42)))), ((int)(((byte)(16)))));
            this.bttnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnDelete.FlatAppearance.BorderSize = 0;
            this.bttnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnDelete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnDelete.Image = ((System.Drawing.Image)(resources.GetObject("bttnDelete.Image")));
            this.bttnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnDelete.Location = new System.Drawing.Point(246, 8);
            this.bttnDelete.Name = "bttnDelete";
            this.bttnDelete.Size = new System.Drawing.Size(111, 42);
            this.bttnDelete.TabIndex = 739;
            this.bttnDelete.TabStop = false;
            this.bttnDelete.Text = "Delete";
            this.bttnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnDelete.UseVisualStyleBackColor = false;
            // 
            // bttnEditCategory
            // 
            this.bttnEditCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(95)))), ((int)(((byte)(144)))));
            this.bttnEditCategory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnEditCategory.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnEditCategory.FlatAppearance.BorderSize = 0;
            this.bttnEditCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnEditCategory.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.bttnEditCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnEditCategory.Image = ((System.Drawing.Image)(resources.GetObject("bttnEditCategory.Image")));
            this.bttnEditCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnEditCategory.Location = new System.Drawing.Point(129, 8);
            this.bttnEditCategory.Name = "bttnEditCategory";
            this.bttnEditCategory.Size = new System.Drawing.Size(111, 42);
            this.bttnEditCategory.TabIndex = 738;
            this.bttnEditCategory.TabStop = false;
            this.bttnEditCategory.Text = "Edit";
            this.bttnEditCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnEditCategory.UseVisualStyleBackColor = false;
            // 
            // bttnNew
            // 
            this.bttnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            this.bttnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnNew.FlatAppearance.BorderSize = 0;
            this.bttnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNew.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.bttnNew.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnNew.Image = ((System.Drawing.Image)(resources.GetObject("bttnNew.Image")));
            this.bttnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNew.Location = new System.Drawing.Point(12, 8);
            this.bttnNew.Name = "bttnNew";
            this.bttnNew.Size = new System.Drawing.Size(111, 42);
            this.bttnNew.TabIndex = 737;
            this.bttnNew.TabStop = false;
            this.bttnNew.Text = "New (F5)";
            this.bttnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnNew.UseVisualStyleBackColor = false;
            this.bttnNew.Click += new System.EventHandler(this.BttnNew_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.SearchBar, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgwMultipleCurrency, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 57);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(930, 358);
            this.tableLayoutPanel1.TabIndex = 740;
            // 
            // SearchBar
            // 
            this.SearchBar.AllowMerge = false;
            this.SearchBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.SearchBar.Location = new System.Drawing.Point(0, 0);
            this.SearchBar.MaximumSize = new System.Drawing.Size(0, 25);
            this.SearchBar.MinimumSize = new System.Drawing.Size(0, 25);
            this.SearchBar.Name = "SearchBar";
            this.SearchBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.SearchBar.Size = new System.Drawing.Size(930, 25);
            this.SearchBar.TabIndex = 722;
            this.SearchBar.Text = "Search Bar";
            // 
            // dgwMultipleCurrency
            // 
            this.dgwMultipleCurrency.AllowUserToAddRows = false;
            this.dgwMultipleCurrency.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Linen;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgwMultipleCurrency.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgwMultipleCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgwMultipleCurrency.AutoGenerateContextFilters = true;
            this.dgwMultipleCurrency.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.dgwMultipleCurrency.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgwMultipleCurrency.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgwMultipleCurrency.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(95)))), ((int)(((byte)(144)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwMultipleCurrency.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgwMultipleCurrency.ColumnHeadersHeight = 35;
            this.dgwMultipleCurrency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgwMultipleCurrency.DateWithTime = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgwMultipleCurrency.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgwMultipleCurrency.EnableHeadersVisualStyles = false;
            this.dgwMultipleCurrency.GridColor = System.Drawing.Color.AliceBlue;
            this.dgwMultipleCurrency.Location = new System.Drawing.Point(3, 28);
            this.dgwMultipleCurrency.MultiSelect = false;
            this.dgwMultipleCurrency.Name = "dgwMultipleCurrency";
            this.dgwMultipleCurrency.ReadOnly = true;
            this.dgwMultipleCurrency.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwMultipleCurrency.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgwMultipleCurrency.RowHeadersWidth = 25;
            this.dgwMultipleCurrency.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgwMultipleCurrency.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgwMultipleCurrency.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgwMultipleCurrency.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
            this.dgwMultipleCurrency.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgwMultipleCurrency.RowTemplate.Height = 30;
            this.dgwMultipleCurrency.Size = new System.Drawing.Size(924, 505);
            this.dgwMultipleCurrency.TabIndex = 717;
            this.dgwMultipleCurrency.TimeFilter = true;
            // 
            // frmB_MultipleCurrency
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 415);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.bttnDelete);
            this.Controls.Add(this.bttnEditCategory);
            this.Controls.Add(this.bttnNew);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_MultipleCurrency";
            this.Text = "Multiple Currency";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmB_MultipleCurrency_FormClosing);
            this.Load += new System.EventHandler(this.FrmB_MultipleCurrency_Load);
            this.Controls.SetChildIndex(this.bttnNew, 0);
            this.Controls.SetChildIndex(this.bttnEditCategory, 0);
            this.Controls.SetChildIndex(this.bttnDelete, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwMultipleCurrency)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnDelete;
        private System.Windows.Forms.Button bttnEditCategory;
        private System.Windows.Forms.Button bttnNew;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public ADGV.SearchToolBar SearchBar;
        private ADGV.AdvancedDataGridView dgwMultipleCurrency;
    }
}