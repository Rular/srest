﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_MultipleCurrency : frmC_ModelBlack
    {
        public frmB_MultipleCurrency()
        {
            InitializeComponent();
        }

        private static frmB_MultipleCurrency singleton = null;

        public static frmB_MultipleCurrency Instance
        {
            get
            {
                if (frmB_MultipleCurrency.singleton == null)
                {
                    frmB_MultipleCurrency.singleton = new frmB_MultipleCurrency();
                }
                return frmB_MultipleCurrency.singleton;
            }
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F5)
            {
                new frmB_BackOfzAddCategory().ShowDialog();


            }
            else if (keyData == Keys.F3)
            {
                if (SearchBar.Visible == false)
                    SearchBar.Visible = true;
                else
                    SearchBar.Items[3].Select();

            }

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);

        }

        private void FrmB_MultipleCurrency_Load(object sender, EventArgs e)
        {

        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            frmB_MultipleCurrency.singleton = null;
            this.Close();
        }

        private void FrmB_MultipleCurrency_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_MultipleCurrency.singleton = null;
        }

        private void BttnNew_Click(object sender, EventArgs e)
        {
            new frmB_AddMultipleCurrency().ShowDialog();
        }
    }
}
