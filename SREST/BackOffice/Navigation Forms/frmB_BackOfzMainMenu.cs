﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzMainMenu : frmC_ModelGreen
    {
        public frmB_BackOfzMainMenu()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzMainMenu singleton = null;

        public static frmB_BackOfzMainMenu Instance
        {
            get
            {
                if (frmB_BackOfzMainMenu.singleton == null)
                {
                    frmB_BackOfzMainMenu.singleton = new frmB_BackOfzMainMenu();
                }
                return frmB_BackOfzMainMenu.singleton;
            }
        }

        private void FrmS_SettingsMain_Load(object sender, EventArgs e)
        {

        }

        private void FrmS_SettingsMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmB_BackOfzMainMenu.singleton = null;
        }

        private void TileStock_Click(object sender, EventArgs e)
        {
            frmB_BackOfzStock stock = frmB_BackOfzStock.Instance;
            stock.MdiParent = this.ParentForm;
            if (stock.Visible == true)
            {
                stock.Activate();
            }
            else
            {
                frmB_BackOfzStock.Instance.Show();
            }
        }

        private void TileMasterFiles_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMasterFilesMenu MasterFIles = frmB_BackOfzMasterFilesMenu.Instance;
            MasterFIles.MdiParent = this.ParentForm;
            if (MasterFIles.Visible == true)
            {
                MasterFIles.Activate();
            }
            else
            {
                frmB_BackOfzMasterFilesMenu.Instance.Show();
            }
        }

        private void TileReservations_Click(object sender, EventArgs e)
        {

        }
    }
}
