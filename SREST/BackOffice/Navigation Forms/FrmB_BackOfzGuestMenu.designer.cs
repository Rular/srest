﻿namespace SREST
{
    partial class FrmB_BackOfzGuestMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzGuestMenu));
            this.bttnGuestSetup = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileGuest_Setup = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnGuestSetup
            // 
            this.bttnGuestSetup.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnGuestSetup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnGuestSetup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnGuestSetup.FlatAppearance.BorderSize = 0;
            this.bttnGuestSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnGuestSetup.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnGuestSetup.ForeColor = System.Drawing.Color.Gray;
            this.bttnGuestSetup.Image = ((System.Drawing.Image)(resources.GetObject("bttnGuestSetup.Image")));
            this.bttnGuestSetup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnGuestSetup.Location = new System.Drawing.Point(336, 60);
            this.bttnGuestSetup.Name = "bttnGuestSetup";
            this.bttnGuestSetup.Size = new System.Drawing.Size(145, 62);
            this.bttnGuestSetup.TabIndex = 729;
            this.bttnGuestSetup.TabStop = false;
            this.bttnGuestSetup.Text = "Guest Setup";
            this.bttnGuestSetup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnGuestSetup.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 728;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 727;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileGuest_Setup);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 726;
            // 
            // TileGuest_Setup
            // 
            this.TileGuest_Setup.ActiveControl = null;
            this.TileGuest_Setup.Location = new System.Drawing.Point(3, 3);
            this.TileGuest_Setup.Name = "TileGuest_Setup";
            this.TileGuest_Setup.Size = new System.Drawing.Size(176, 94);
            this.TileGuest_Setup.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileGuest_Setup.TabIndex = 3;
            this.TileGuest_Setup.Text = "Guest_Setup";
            this.TileGuest_Setup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileGuest_Setup.TileImage = ((System.Drawing.Image)(resources.GetObject("TileGuest_Setup.TileImage")));
            this.TileGuest_Setup.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileGuest_Setup.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileGuest_Setup.UseSelectable = true;
            this.TileGuest_Setup.UseTileImage = true;
            // 
            // FrmB_BackOfzGuest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bttnGuestSetup);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzGuest";
            this.Text = "FrmB_BackOfzGuest";
            this.Load += new System.EventHandler(this.FrmB_BackOfzGuest_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnGuestSetup, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnGuestSetup;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileGuest_Setup;
    }
}