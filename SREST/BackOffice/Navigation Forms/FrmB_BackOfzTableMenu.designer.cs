﻿namespace SREST
{
    partial class FrmB_BackOfzTableMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzTableMenu));
            this.bttnTable = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileTable_Category = new MetroFramework.Controls.MetroTile();
            this.Tiletable = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnTable
            // 
            this.bttnTable.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnTable.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnTable.FlatAppearance.BorderSize = 0;
            this.bttnTable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnTable.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnTable.ForeColor = System.Drawing.Color.Gray;
            this.bttnTable.Image = ((System.Drawing.Image)(resources.GetObject("bttnTable.Image")));
            this.bttnTable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnTable.Location = new System.Drawing.Point(336, 60);
            this.bttnTable.Name = "bttnTable";
            this.bttnTable.Size = new System.Drawing.Size(145, 62);
            this.bttnTable.TabIndex = 731;
            this.bttnTable.TabStop = false;
            this.bttnTable.Text = "Table";
            this.bttnTable.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 730;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            this.bttnMasterFile.Click += new System.EventHandler(this.BttnMasterFile_Click);
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 729;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            this.bttnBackOffice.Click += new System.EventHandler(this.BttnBackOffice_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileTable_Category);
            this.flowLayoutPanel1.Controls.Add(this.Tiletable);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 732;
            // 
            // TileTable_Category
            // 
            this.TileTable_Category.ActiveControl = null;
            this.TileTable_Category.Location = new System.Drawing.Point(3, 3);
            this.TileTable_Category.Name = "TileTable_Category";
            this.TileTable_Category.Size = new System.Drawing.Size(176, 94);
            this.TileTable_Category.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileTable_Category.TabIndex = 3;
            this.TileTable_Category.Text = "Table Category";
            this.TileTable_Category.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileTable_Category.TileImage = ((System.Drawing.Image)(resources.GetObject("TileTable_Category.TileImage")));
            this.TileTable_Category.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileTable_Category.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileTable_Category.UseSelectable = true;
            this.TileTable_Category.UseTileImage = true;
            this.TileTable_Category.Click += new System.EventHandler(this.TileTable_Category_Click);
            // 
            // Tiletable
            // 
            this.Tiletable.ActiveControl = null;
            this.Tiletable.Location = new System.Drawing.Point(185, 3);
            this.Tiletable.Name = "Tiletable";
            this.Tiletable.Size = new System.Drawing.Size(176, 94);
            this.Tiletable.Style = MetroFramework.MetroColorStyle.Silver;
            this.Tiletable.TabIndex = 5;
            this.Tiletable.Text = "Table";
            this.Tiletable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tiletable.TileImage = ((System.Drawing.Image)(resources.GetObject("Tiletable.TileImage")));
            this.Tiletable.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Tiletable.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.Tiletable.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.Tiletable.UseSelectable = true;
            this.Tiletable.UseTileImage = true;
            this.Tiletable.Click += new System.EventHandler(this.Tiletable_Click);
            // 
            // FrmB_BackOfzTableMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnTable);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzTableMenu";
            this.Text = "FrmB_BackOfzTable";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmB_BackOfzTableMenu_FormClosing);
            this.Load += new System.EventHandler(this.FrmB_BackOfzTable_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnTable, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnTable;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileTable_Category;
        private MetroFramework.Controls.MetroTile Tiletable;
    }
}