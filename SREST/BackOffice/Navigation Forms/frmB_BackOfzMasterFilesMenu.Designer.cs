﻿namespace SREST
{
    partial class frmB_BackOfzMasterFilesMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzMasterFilesMenu));
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileCurrency = new MetroFramework.Controls.MetroTile();
            this.TileGuests = new MetroFramework.Controls.MetroTile();
            this.TileEmployee = new MetroFramework.Controls.MetroTile();
            this.TileTax = new MetroFramework.Controls.MetroTile();
            this.TileItemMaster = new MetroFramework.Controls.MetroTile();
            this.TileTableMaster = new MetroFramework.Controls.MetroTile();
            this.TileSupplier = new MetroFramework.Controls.MetroTile();
            this.button1 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 719;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            this.bttnBackOffice.Click += new System.EventHandler(this.BttnBackOffice_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.TileCurrency);
            this.flowLayoutPanel1.Controls.Add(this.TileGuests);
            this.flowLayoutPanel1.Controls.Add(this.TileEmployee);
            this.flowLayoutPanel1.Controls.Add(this.TileTax);
            this.flowLayoutPanel1.Controls.Add(this.TileItemMaster);
            this.flowLayoutPanel1.Controls.Add(this.TileTableMaster);
            this.flowLayoutPanel1.Controls.Add(this.TileSupplier);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 125);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(767, 418);
            this.flowLayoutPanel1.TabIndex = 720;
            // 
            // TileCurrency
            // 
            this.TileCurrency.ActiveControl = null;
            this.TileCurrency.Location = new System.Drawing.Point(3, 3);
            this.TileCurrency.Name = "TileCurrency";
            this.TileCurrency.Size = new System.Drawing.Size(176, 94);
            this.TileCurrency.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileCurrency.TabIndex = 2;
            this.TileCurrency.Text = "Currency";
            this.TileCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileCurrency.TileImage = ((System.Drawing.Image)(resources.GetObject("TileCurrency.TileImage")));
            this.TileCurrency.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileCurrency.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileCurrency.UseSelectable = true;
            this.TileCurrency.UseTileImage = true;
            this.TileCurrency.Click += new System.EventHandler(this.TileCurrency_Click);
            // 
            // TileGuests
            // 
            this.TileGuests.ActiveControl = null;
            this.TileGuests.Location = new System.Drawing.Point(185, 3);
            this.TileGuests.Name = "TileGuests";
            this.TileGuests.Size = new System.Drawing.Size(176, 94);
            this.TileGuests.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileGuests.TabIndex = 4;
            this.TileGuests.Text = "Guests";
            this.TileGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileGuests.TileImage = ((System.Drawing.Image)(resources.GetObject("TileGuests.TileImage")));
            this.TileGuests.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileGuests.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileGuests.UseSelectable = true;
            this.TileGuests.UseTileImage = true;
            this.TileGuests.Click += new System.EventHandler(this.TileGuests_Click);
            // 
            // TileEmployee
            // 
            this.TileEmployee.ActiveControl = null;
            this.TileEmployee.Location = new System.Drawing.Point(367, 3);
            this.TileEmployee.Name = "TileEmployee";
            this.TileEmployee.Size = new System.Drawing.Size(176, 94);
            this.TileEmployee.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileEmployee.TabIndex = 5;
            this.TileEmployee.Text = "Employee";
            this.TileEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileEmployee.TileImage = ((System.Drawing.Image)(resources.GetObject("TileEmployee.TileImage")));
            this.TileEmployee.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileEmployee.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileEmployee.UseSelectable = true;
            this.TileEmployee.UseTileImage = true;
            this.TileEmployee.Click += new System.EventHandler(this.TileEmployee_Click);
            // 
            // TileTax
            // 
            this.TileTax.ActiveControl = null;
            this.TileTax.Location = new System.Drawing.Point(549, 3);
            this.TileTax.Name = "TileTax";
            this.TileTax.Size = new System.Drawing.Size(176, 94);
            this.TileTax.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileTax.TabIndex = 6;
            this.TileTax.Text = "Tax";
            this.TileTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileTax.TileImage = ((System.Drawing.Image)(resources.GetObject("TileTax.TileImage")));
            this.TileTax.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileTax.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileTax.UseSelectable = true;
            this.TileTax.UseTileImage = true;
            // 
            // TileItemMaster
            // 
            this.TileItemMaster.ActiveControl = null;
            this.TileItemMaster.Location = new System.Drawing.Point(3, 103);
            this.TileItemMaster.Name = "TileItemMaster";
            this.TileItemMaster.Size = new System.Drawing.Size(176, 94);
            this.TileItemMaster.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileItemMaster.TabIndex = 7;
            this.TileItemMaster.Text = "Item Master";
            this.TileItemMaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileItemMaster.TileImage = ((System.Drawing.Image)(resources.GetObject("TileItemMaster.TileImage")));
            this.TileItemMaster.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileItemMaster.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileItemMaster.UseSelectable = true;
            this.TileItemMaster.UseTileImage = true;
            this.TileItemMaster.Click += new System.EventHandler(this.TileItemMaster_Click);
            // 
            // TileTableMaster
            // 
            this.TileTableMaster.ActiveControl = null;
            this.TileTableMaster.Location = new System.Drawing.Point(185, 103);
            this.TileTableMaster.Name = "TileTableMaster";
            this.TileTableMaster.Size = new System.Drawing.Size(176, 94);
            this.TileTableMaster.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileTableMaster.TabIndex = 8;
            this.TileTableMaster.Text = "Table Master";
            this.TileTableMaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileTableMaster.TileImage = ((System.Drawing.Image)(resources.GetObject("TileTableMaster.TileImage")));
            this.TileTableMaster.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileTableMaster.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileTableMaster.UseSelectable = true;
            this.TileTableMaster.UseTileImage = true;
            this.TileTableMaster.Click += new System.EventHandler(this.TileTableMaster_Click);
            // 
            // TileSupplier
            // 
            this.TileSupplier.ActiveControl = null;
            this.TileSupplier.Location = new System.Drawing.Point(367, 103);
            this.TileSupplier.Name = "TileSupplier";
            this.TileSupplier.Size = new System.Drawing.Size(176, 94);
            this.TileSupplier.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSupplier.TabIndex = 9;
            this.TileSupplier.Text = "Supplier Master";
            this.TileSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSupplier.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSupplier.TileImage")));
            this.TileSupplier.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSupplier.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSupplier.UseSelectable = true;
            this.TileSupplier.UseTileImage = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gray;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(197, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 62);
            this.button1.TabIndex = 722;
            this.button1.TabStop = false;
            this.button1.Text = "       Master Files";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // frmB_BackOfzMasterFilesMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzMasterFilesMenu";
            this.Text = "frmB_BackOfzMasterFiles";
            this.Load += new System.EventHandler(this.FrmB_BackOfzMasterFiles_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileCurrency;
        private MetroFramework.Controls.MetroTile TileGuests;
        private MetroFramework.Controls.MetroTile TileEmployee;
        private MetroFramework.Controls.MetroTile TileTax;
        private MetroFramework.Controls.MetroTile TileItemMaster;
        private MetroFramework.Controls.MetroTile TileTableMaster;
        private MetroFramework.Controls.MetroTile TileSupplier;
        private System.Windows.Forms.Button button1;
    }
}