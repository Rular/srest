﻿namespace SREST
{
    partial class frmB_BackOfzMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzMainMenu));
            this.bttnNewOrder = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileMasterFiles = new MetroFramework.Controls.MetroTile();
            this.TileStock = new MetroFramework.Controls.MetroTile();
            this.TileProduction = new MetroFramework.Controls.MetroTile();
            this.TileReservations = new MetroFramework.Controls.MetroTile();
            this.TileHallReservation = new MetroFramework.Controls.MetroTile();
            this.TileRestaurent = new MetroFramework.Controls.MetroTile();
            this.TileInvoicing = new MetroFramework.Controls.MetroTile();
            this.TilePayManagement = new MetroFramework.Controls.MetroTile();
            this.TileBanquetPay = new MetroFramework.Controls.MetroTile();
            this.TileHousekeeping = new MetroFramework.Controls.MetroTile();
            this.TileLostFound = new MetroFramework.Controls.MetroTile();
            this.TileForecasting = new MetroFramework.Controls.MetroTile();
            this.TileSystemOptions = new MetroFramework.Controls.MetroTile();
            this.TileUsers = new MetroFramework.Controls.MetroTile();
            this.TileReports = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnNewOrder
            // 
            this.bttnNewOrder.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnNewOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnNewOrder.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnNewOrder.FlatAppearance.BorderSize = 0;
            this.bttnNewOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNewOrder.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNewOrder.ForeColor = System.Drawing.Color.Gray;
            this.bttnNewOrder.Image = ((System.Drawing.Image)(resources.GetObject("bttnNewOrder.Image")));
            this.bttnNewOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNewOrder.Location = new System.Drawing.Point(12, 57);
            this.bttnNewOrder.Name = "bttnNewOrder";
            this.bttnNewOrder.Size = new System.Drawing.Size(204, 62);
            this.bttnNewOrder.TabIndex = 717;
            this.bttnNewOrder.TabStop = false;
            this.bttnNewOrder.Text = "       Back Office";
            this.bttnNewOrder.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.TileMasterFiles);
            this.flowLayoutPanel1.Controls.Add(this.TileStock);
            this.flowLayoutPanel1.Controls.Add(this.TileProduction);
            this.flowLayoutPanel1.Controls.Add(this.TileReservations);
            this.flowLayoutPanel1.Controls.Add(this.TileHallReservation);
            this.flowLayoutPanel1.Controls.Add(this.TileRestaurent);
            this.flowLayoutPanel1.Controls.Add(this.TileInvoicing);
            this.flowLayoutPanel1.Controls.Add(this.TilePayManagement);
            this.flowLayoutPanel1.Controls.Add(this.TileBanquetPay);
            this.flowLayoutPanel1.Controls.Add(this.TileHousekeeping);
            this.flowLayoutPanel1.Controls.Add(this.TileLostFound);
            this.flowLayoutPanel1.Controls.Add(this.TileForecasting);
            this.flowLayoutPanel1.Controls.Add(this.TileSystemOptions);
            this.flowLayoutPanel1.Controls.Add(this.TileUsers);
            this.flowLayoutPanel1.Controls.Add(this.TileReports);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 125);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(905, 313);
            this.flowLayoutPanel1.TabIndex = 718;
            // 
            // TileMasterFiles
            // 
            this.TileMasterFiles.ActiveControl = null;
            this.TileMasterFiles.Location = new System.Drawing.Point(3, 3);
            this.TileMasterFiles.Name = "TileMasterFiles";
            this.TileMasterFiles.Size = new System.Drawing.Size(176, 94);
            this.TileMasterFiles.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileMasterFiles.TabIndex = 2;
            this.TileMasterFiles.Text = "Master Files";
            this.TileMasterFiles.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileMasterFiles.TileImage = ((System.Drawing.Image)(resources.GetObject("TileMasterFiles.TileImage")));
            this.TileMasterFiles.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileMasterFiles.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileMasterFiles.UseSelectable = true;
            this.TileMasterFiles.UseTileImage = true;
            this.TileMasterFiles.Click += new System.EventHandler(this.TileMasterFiles_Click);
            // 
            // TileStock
            // 
            this.TileStock.ActiveControl = null;
            this.TileStock.Location = new System.Drawing.Point(185, 3);
            this.TileStock.Name = "TileStock";
            this.TileStock.Size = new System.Drawing.Size(176, 94);
            this.TileStock.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileStock.TabIndex = 4;
            this.TileStock.Text = "Stock";
            this.TileStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileStock.TileImage = ((System.Drawing.Image)(resources.GetObject("TileStock.TileImage")));
            this.TileStock.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileStock.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileStock.UseSelectable = true;
            this.TileStock.UseTileImage = true;
            this.TileStock.Click += new System.EventHandler(this.TileStock_Click);
            // 
            // TileProduction
            // 
            this.TileProduction.ActiveControl = null;
            this.TileProduction.Location = new System.Drawing.Point(367, 3);
            this.TileProduction.Name = "TileProduction";
            this.TileProduction.Size = new System.Drawing.Size(176, 94);
            this.TileProduction.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileProduction.TabIndex = 5;
            this.TileProduction.Text = "Production";
            this.TileProduction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileProduction.TileImage = ((System.Drawing.Image)(resources.GetObject("TileProduction.TileImage")));
            this.TileProduction.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileProduction.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileProduction.UseSelectable = true;
            this.TileProduction.UseTileImage = true;
            // 
            // TileReservations
            // 
            this.TileReservations.ActiveControl = null;
            this.TileReservations.Location = new System.Drawing.Point(549, 3);
            this.TileReservations.Name = "TileReservations";
            this.TileReservations.Size = new System.Drawing.Size(176, 94);
            this.TileReservations.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileReservations.TabIndex = 6;
            this.TileReservations.Text = "Recervations";
            this.TileReservations.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileReservations.TileImage = ((System.Drawing.Image)(resources.GetObject("TileReservations.TileImage")));
            this.TileReservations.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileReservations.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileReservations.UseSelectable = true;
            this.TileReservations.UseTileImage = true;
            this.TileReservations.Click += new System.EventHandler(this.TileReservations_Click);
            // 
            // TileHallReservation
            // 
            this.TileHallReservation.ActiveControl = null;
            this.TileHallReservation.Location = new System.Drawing.Point(3, 103);
            this.TileHallReservation.Name = "TileHallReservation";
            this.TileHallReservation.Size = new System.Drawing.Size(176, 94);
            this.TileHallReservation.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileHallReservation.TabIndex = 7;
            this.TileHallReservation.Text = "Hall Recervation";
            this.TileHallReservation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileHallReservation.TileImage = ((System.Drawing.Image)(resources.GetObject("TileHallReservation.TileImage")));
            this.TileHallReservation.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileHallReservation.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileHallReservation.UseSelectable = true;
            this.TileHallReservation.UseTileImage = true;
            // 
            // TileRestaurent
            // 
            this.TileRestaurent.ActiveControl = null;
            this.TileRestaurent.Location = new System.Drawing.Point(185, 103);
            this.TileRestaurent.Name = "TileRestaurent";
            this.TileRestaurent.Size = new System.Drawing.Size(176, 94);
            this.TileRestaurent.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileRestaurent.TabIndex = 8;
            this.TileRestaurent.Text = "Restaurent";
            this.TileRestaurent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileRestaurent.TileImage = ((System.Drawing.Image)(resources.GetObject("TileRestaurent.TileImage")));
            this.TileRestaurent.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileRestaurent.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileRestaurent.UseSelectable = true;
            this.TileRestaurent.UseTileImage = true;
            // 
            // TileInvoicing
            // 
            this.TileInvoicing.ActiveControl = null;
            this.TileInvoicing.Location = new System.Drawing.Point(367, 103);
            this.TileInvoicing.Name = "TileInvoicing";
            this.TileInvoicing.Size = new System.Drawing.Size(176, 94);
            this.TileInvoicing.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileInvoicing.TabIndex = 9;
            this.TileInvoicing.Text = "Invoicing";
            this.TileInvoicing.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileInvoicing.TileImage = ((System.Drawing.Image)(resources.GetObject("TileInvoicing.TileImage")));
            this.TileInvoicing.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileInvoicing.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileInvoicing.UseSelectable = true;
            this.TileInvoicing.UseTileImage = true;
            // 
            // TilePayManagement
            // 
            this.TilePayManagement.ActiveControl = null;
            this.TilePayManagement.Location = new System.Drawing.Point(549, 103);
            this.TilePayManagement.Name = "TilePayManagement";
            this.TilePayManagement.Size = new System.Drawing.Size(176, 94);
            this.TilePayManagement.Style = MetroFramework.MetroColorStyle.Silver;
            this.TilePayManagement.TabIndex = 10;
            this.TilePayManagement.Text = "Pay Manage";
            this.TilePayManagement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TilePayManagement.TileImage = ((System.Drawing.Image)(resources.GetObject("TilePayManagement.TileImage")));
            this.TilePayManagement.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TilePayManagement.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TilePayManagement.UseSelectable = true;
            this.TilePayManagement.UseTileImage = true;
            // 
            // TileBanquetPay
            // 
            this.TileBanquetPay.ActiveControl = null;
            this.TileBanquetPay.Location = new System.Drawing.Point(3, 203);
            this.TileBanquetPay.Name = "TileBanquetPay";
            this.TileBanquetPay.Size = new System.Drawing.Size(176, 94);
            this.TileBanquetPay.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileBanquetPay.TabIndex = 11;
            this.TileBanquetPay.Text = "Banquet Pay";
            this.TileBanquetPay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileBanquetPay.TileImage = ((System.Drawing.Image)(resources.GetObject("TileBanquetPay.TileImage")));
            this.TileBanquetPay.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileBanquetPay.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileBanquetPay.UseSelectable = true;
            this.TileBanquetPay.UseTileImage = true;
            // 
            // TileHousekeeping
            // 
            this.TileHousekeeping.ActiveControl = null;
            this.TileHousekeeping.Location = new System.Drawing.Point(185, 203);
            this.TileHousekeeping.Name = "TileHousekeeping";
            this.TileHousekeeping.Size = new System.Drawing.Size(176, 94);
            this.TileHousekeeping.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileHousekeeping.TabIndex = 12;
            this.TileHousekeeping.Text = "Housekeeping";
            this.TileHousekeeping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileHousekeeping.TileImage = ((System.Drawing.Image)(resources.GetObject("TileHousekeeping.TileImage")));
            this.TileHousekeeping.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileHousekeeping.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileHousekeeping.UseSelectable = true;
            this.TileHousekeeping.UseTileImage = true;
            // 
            // TileLostFound
            // 
            this.TileLostFound.ActiveControl = null;
            this.TileLostFound.Location = new System.Drawing.Point(367, 203);
            this.TileLostFound.Name = "TileLostFound";
            this.TileLostFound.Size = new System.Drawing.Size(176, 94);
            this.TileLostFound.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileLostFound.TabIndex = 13;
            this.TileLostFound.Text = "Lost && Found";
            this.TileLostFound.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileLostFound.TileImage = ((System.Drawing.Image)(resources.GetObject("TileLostFound.TileImage")));
            this.TileLostFound.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileLostFound.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileLostFound.UseSelectable = true;
            this.TileLostFound.UseTileImage = true;
            // 
            // TileForecasting
            // 
            this.TileForecasting.ActiveControl = null;
            this.TileForecasting.Location = new System.Drawing.Point(549, 203);
            this.TileForecasting.Name = "TileForecasting";
            this.TileForecasting.Size = new System.Drawing.Size(176, 94);
            this.TileForecasting.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileForecasting.TabIndex = 14;
            this.TileForecasting.Text = "Forecasting";
            this.TileForecasting.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileForecasting.TileImage = ((System.Drawing.Image)(resources.GetObject("TileForecasting.TileImage")));
            this.TileForecasting.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileForecasting.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileForecasting.UseSelectable = true;
            this.TileForecasting.UseTileImage = true;
            // 
            // TileSystemOptions
            // 
            this.TileSystemOptions.ActiveControl = null;
            this.TileSystemOptions.Location = new System.Drawing.Point(3, 303);
            this.TileSystemOptions.Name = "TileSystemOptions";
            this.TileSystemOptions.Size = new System.Drawing.Size(176, 94);
            this.TileSystemOptions.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSystemOptions.TabIndex = 0;
            this.TileSystemOptions.Text = "System Options";
            this.TileSystemOptions.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSystemOptions.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSystemOptions.TileImage")));
            this.TileSystemOptions.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSystemOptions.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSystemOptions.UseSelectable = true;
            this.TileSystemOptions.UseTileImage = true;
            // 
            // TileUsers
            // 
            this.TileUsers.ActiveControl = null;
            this.TileUsers.Location = new System.Drawing.Point(185, 303);
            this.TileUsers.Name = "TileUsers";
            this.TileUsers.Size = new System.Drawing.Size(176, 94);
            this.TileUsers.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileUsers.TabIndex = 1;
            this.TileUsers.Text = "System Users";
            this.TileUsers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileUsers.TileImage = ((System.Drawing.Image)(resources.GetObject("TileUsers.TileImage")));
            this.TileUsers.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileUsers.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileUsers.UseSelectable = true;
            this.TileUsers.UseTileImage = true;
            // 
            // TileReports
            // 
            this.TileReports.ActiveControl = null;
            this.TileReports.Location = new System.Drawing.Point(367, 303);
            this.TileReports.Name = "TileReports";
            this.TileReports.Size = new System.Drawing.Size(176, 94);
            this.TileReports.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileReports.TabIndex = 3;
            this.TileReports.Text = "Reports";
            this.TileReports.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileReports.TileImage = ((System.Drawing.Image)(resources.GetObject("TileReports.TileImage")));
            this.TileReports.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileReports.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileReports.UseSelectable = true;
            this.TileReports.UseTileImage = true;
            // 
            // frmB_BackOfzMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnNewOrder);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzMainMenu";
            this.Text = "frmS_SettingsMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmS_SettingsMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmS_SettingsMain_Load);
            this.Controls.SetChildIndex(this.bttnNewOrder, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnNewOrder;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileSystemOptions;
        private MetroFramework.Controls.MetroTile TileUsers;
        private MetroFramework.Controls.MetroTile TileReports;
        private MetroFramework.Controls.MetroTile TileMasterFiles;
        private MetroFramework.Controls.MetroTile TileStock;
        private MetroFramework.Controls.MetroTile TileProduction;
        private MetroFramework.Controls.MetroTile TileReservations;
        private MetroFramework.Controls.MetroTile TileHallReservation;
        private MetroFramework.Controls.MetroTile TileRestaurent;
        private MetroFramework.Controls.MetroTile TileInvoicing;
        private MetroFramework.Controls.MetroTile TilePayManagement;
        private MetroFramework.Controls.MetroTile TileBanquetPay;
        private MetroFramework.Controls.MetroTile TileHousekeeping;
        private MetroFramework.Controls.MetroTile TileLostFound;
        private MetroFramework.Controls.MetroTile TileForecasting;
    }
}