﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class FrmB_BackOfzEmpMenu : frmC_ModelGreen
    {
        public FrmB_BackOfzEmpMenu()
        {
            InitializeComponent();
        }

        private static FrmB_BackOfzEmpMenu singleton = null;

        public static FrmB_BackOfzEmpMenu Instance
        {
            get
            {
                if (FrmB_BackOfzEmpMenu.singleton == null)
                {
                    FrmB_BackOfzEmpMenu.singleton = new FrmB_BackOfzEmpMenu();
                }
                return FrmB_BackOfzEmpMenu.singleton;
            }

        }
        private void FrmB_BackOfzEmployee_Load(object sender, EventArgs e)
        {

        }

        private void TileEmployee_Category_Click(object sender, EventArgs e)
        {
            frmB_BackOfzEmpCategory empCat = frmB_BackOfzEmpCategory.Instance;
            empCat.MdiParent = this.ParentForm;
            if (empCat.Visible == true)
            {
                empCat.Activate();
            }
            else
            {
                frmB_BackOfzEmpCategory.Instance.Show();
            }
        }

        private void TileEmployee_Setup_Click(object sender, EventArgs e)
        {
            frmB_BackOfzEmployee emp = frmB_BackOfzEmployee.Instance;
            emp.MdiParent = this.ParentForm;
            if (emp.Visible == true)
            {
                emp.Activate();
            }
            else
            {
                frmB_BackOfzEmployee.Instance.Show();
            }
        }
    }
}
