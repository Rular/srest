﻿namespace SREST
{
    partial class FrmB_BackOfzEmpMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzEmpMenu));
            this.bttnEmployee = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileEmployee_Category = new MetroFramework.Controls.MetroTile();
            this.TileEmployee_Setup = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnEmployee
            // 
            this.bttnEmployee.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnEmployee.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnEmployee.FlatAppearance.BorderSize = 0;
            this.bttnEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnEmployee.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnEmployee.ForeColor = System.Drawing.Color.Gray;
            this.bttnEmployee.Image = ((System.Drawing.Image)(resources.GetObject("bttnEmployee.Image")));
            this.bttnEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnEmployee.Location = new System.Drawing.Point(336, 60);
            this.bttnEmployee.Name = "bttnEmployee";
            this.bttnEmployee.Size = new System.Drawing.Size(145, 62);
            this.bttnEmployee.TabIndex = 728;
            this.bttnEmployee.TabStop = false;
            this.bttnEmployee.Text = "Employee";
            this.bttnEmployee.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 727;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 726;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileEmployee_Category);
            this.flowLayoutPanel1.Controls.Add(this.TileEmployee_Setup);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 729;
            // 
            // TileEmployee_Category
            // 
            this.TileEmployee_Category.ActiveControl = null;
            this.TileEmployee_Category.Location = new System.Drawing.Point(3, 3);
            this.TileEmployee_Category.Name = "TileEmployee_Category";
            this.TileEmployee_Category.Size = new System.Drawing.Size(176, 94);
            this.TileEmployee_Category.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileEmployee_Category.TabIndex = 3;
            this.TileEmployee_Category.Text = "Category";
            this.TileEmployee_Category.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileEmployee_Category.TileImage = ((System.Drawing.Image)(resources.GetObject("TileEmployee_Category.TileImage")));
            this.TileEmployee_Category.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileEmployee_Category.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileEmployee_Category.UseSelectable = true;
            this.TileEmployee_Category.UseTileImage = true;
            this.TileEmployee_Category.Click += new System.EventHandler(this.TileEmployee_Category_Click);
            // 
            // TileEmployee_Setup
            // 
            this.TileEmployee_Setup.ActiveControl = null;
            this.TileEmployee_Setup.Location = new System.Drawing.Point(185, 3);
            this.TileEmployee_Setup.Name = "TileEmployee_Setup";
            this.TileEmployee_Setup.Size = new System.Drawing.Size(176, 94);
            this.TileEmployee_Setup.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileEmployee_Setup.TabIndex = 5;
            this.TileEmployee_Setup.Text = "Employee Setup";
            this.TileEmployee_Setup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileEmployee_Setup.TileImage = ((System.Drawing.Image)(resources.GetObject("TileEmployee_Setup.TileImage")));
            this.TileEmployee_Setup.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileEmployee_Setup.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.TileEmployee_Setup.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileEmployee_Setup.UseSelectable = true;
            this.TileEmployee_Setup.UseTileImage = true;
            this.TileEmployee_Setup.Click += new System.EventHandler(this.TileEmployee_Setup_Click);
            // 
            // FrmB_BackOfzEmpMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnEmployee);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzEmpMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee";
            this.Load += new System.EventHandler(this.FrmB_BackOfzEmployee_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnEmployee, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnEmployee;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileEmployee_Category;
        private MetroFramework.Controls.MetroTile TileEmployee_Setup;
    }
}