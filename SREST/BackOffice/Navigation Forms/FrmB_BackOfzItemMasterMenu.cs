﻿using SREST.BackOffice;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class FrmB_BackOfzItemMasterMenu : frmC_ModelGreen
    {
        public FrmB_BackOfzItemMasterMenu()
        {
            InitializeComponent();
        }

        private static FrmB_BackOfzItemMasterMenu singleton = null;

        public static FrmB_BackOfzItemMasterMenu Instance
        {
            get
            {
                if (FrmB_BackOfzItemMasterMenu.singleton == null)
                {
                    FrmB_BackOfzItemMasterMenu.singleton = new FrmB_BackOfzItemMasterMenu();
                }
                return FrmB_BackOfzItemMasterMenu.singleton;
            }
        }

        private void FrmB_BackOfzItemMaster_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzItemMaster_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmB_BackOfzItemMasterMenu singleton = null;
        }

        private void BttnBackOffice_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMainMenu settings = frmB_BackOfzMainMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMasterFilesMenu settings = frmB_BackOfzMasterFilesMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMasterFilesMenu.Instance.Show();
            }
        }

        private void TileItmCategory_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemCategory ItemCat = frmB_BackOfzItemCategory.Instance;
            ItemCat.MdiParent = this.ParentForm;
            if (ItemCat.Visible == true)
            {
                ItemCat.Activate();
            }
            else
            {
                frmB_BackOfzItemCategory.Instance.Show();
            }
        }

        private void TileSubCategory_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemSubCategory ItemSubCat = frmB_BackOfzItemSubCategory.Instance;
            ItemSubCat.MdiParent = this.ParentForm;
            if (ItemSubCat.Visible == true)
            {
                ItemSubCat.Activate();
            }
            else
            {
                frmB_BackOfzItemSubCategory.Instance.Show();
            }
        }

        private void TileSubCategory2_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItemSubCategory2 ItemSubCat2 = frmB_BackOfzItemSubCategory2.Instance;
            ItemSubCat2.MdiParent = this.ParentForm;
            if (ItemSubCat2.Visible == true)
            {
                ItemSubCat2.Activate();
            }
            else
            {
                frmB_BackOfzItemSubCategory2.Instance.Show();
            }
        }

        private void TileUnitGroup_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnitGroup settings = frmB_BackOfzUnitGroup.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzUnitGroup.Instance.Show();
            }
        }

        private void TileItem_Click(object sender, EventArgs e)
        {
            frmB_BackOfzItem item = frmB_BackOfzItem.Instance;
            item.MdiParent = this.ParentForm;
            if (item.Visible == true)
            {
                item.Activate();
            }
            else
            {
                frmB_BackOfzItem.Instance.Show();
            }
        }

        private void TileUnit_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnit unit = frmB_BackOfzUnit.Instance;
            unit.MdiParent = this.ParentForm;
            if (unit.Visible == true)
            {
                unit.Activate();
            }
            else
            {
                frmB_BackOfzUnit.Instance.Show();
            }
        }

        private void TileUnitConversion_Click(object sender, EventArgs e)
        {
            frmB_BackOfzUnitConversion unitConv = frmB_BackOfzUnitConversion.Instance;
            unitConv.MdiParent = this.ParentForm;
            if (unitConv.Visible == true)
            {
                unitConv.Activate();
            }
            else
            {
                frmB_BackOfzUnitConversion.Instance.Show();
            }
        }

        private void TilePrinters_Click(object sender, EventArgs e)
        {
            frmB_BackOfzPrinters printers = frmB_BackOfzPrinters.Instance;
            printers.MdiParent = this.ParentForm;
            if (printers.Visible == true)
            {
                printers.Activate();
            }
            else
            {
                frmB_BackOfzPrinters.Instance.Show();
            }
        }

        private void TileExcellUpload_Click(object sender, EventArgs e)
        {
            frmB_BackOfzExcellUpload excellUp = frmB_BackOfzExcellUpload.Instance;
            excellUp.MdiParent = this.ParentForm;
            if (excellUp.Visible == true)
            {
                excellUp.Activate();
            }
            else
            {
                frmB_BackOfzExcellUpload.Instance.Show();
            }
        }
    }
}
