﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class FrmB_BackOfzCurrencyMenu : frmC_ModelGreen
    {
        public FrmB_BackOfzCurrencyMenu()
        {
            InitializeComponent();
        }

        private static FrmB_BackOfzCurrencyMenu singleton = null;

        public static FrmB_BackOfzCurrencyMenu Instance
        {
            get
            {
                if (FrmB_BackOfzCurrencyMenu.singleton == null)
                {
                    FrmB_BackOfzCurrencyMenu.singleton = new FrmB_BackOfzCurrencyMenu();
                }
                return FrmB_BackOfzCurrencyMenu.singleton;
            }
        }

        private void FrmB_BackOfzCurrancey_Load(object sender, EventArgs e)
        {

        }

        private void TileMcurrancy_Click(object sender, EventArgs e)
        {
            frmB_MultipleCurrency multipleCurrency = frmB_MultipleCurrency.Instance;
            multipleCurrency.MdiParent = this.ParentForm;
            if (multipleCurrency.Visible == true)
            {
                multipleCurrency.Activate();
            }
            else
            {
                frmB_MultipleCurrency.Instance.Show();
            }
        }
    }
}
