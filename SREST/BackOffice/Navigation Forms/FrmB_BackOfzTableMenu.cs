﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class FrmB_BackOfzTableMenu : frmC_ModelGreen
    {
        public FrmB_BackOfzTableMenu()
        {
            InitializeComponent();
        }

        private static FrmB_BackOfzTableMenu singleton = null;

        public static FrmB_BackOfzTableMenu Instance
        {
            get
            {
                if (FrmB_BackOfzTableMenu.singleton == null)
                {
                    FrmB_BackOfzTableMenu.singleton = new FrmB_BackOfzTableMenu();
                }
                return FrmB_BackOfzTableMenu.singleton;
            }
        }

        private void FrmB_BackOfzTable_Load(object sender, EventArgs e)
        {

        }

        private void FrmB_BackOfzTableMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmB_BackOfzTableMenu.singleton = null;
        }

        private void BttnBackOffice_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMainMenu settings = frmB_BackOfzMainMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }

        private void BttnMasterFile_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMasterFilesMenu settings = frmB_BackOfzMasterFilesMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMasterFilesMenu.Instance.Show();
            }
        }

        private void TileTable_Category_Click(object sender, EventArgs e)
        {
            frmB_BackOfzTableCategory tableSubCat = frmB_BackOfzTableCategory.Instance;
            tableSubCat.MdiParent = this.ParentForm;
            if (tableSubCat.Visible == true)
            {
                tableSubCat.Activate();
            }
            else
            {
                frmB_BackOfzTableCategory.Instance.Show();
            }
        }

        private void Tiletable_Click(object sender, EventArgs e)
        {
            frmB_BackOfzTable table = frmB_BackOfzTable.Instance;
            table.MdiParent = this.ParentForm;
            if (table.Visible == true)
            {
                table.Activate();
            }
            else
            {
                frmB_BackOfzTable.Instance.Show();
            }
        }
    }
}
