﻿namespace SREST
{
    partial class FrmB_BackOfzTaxMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzTaxMenu));
            this.bttnTax = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileTax_Master = new MetroFramework.Controls.MetroTile();
            this.TileRoom_Tax = new MetroFramework.Controls.MetroTile();
            this.TileHall_Tax = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnTax
            // 
            this.bttnTax.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnTax.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnTax.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnTax.FlatAppearance.BorderSize = 0;
            this.bttnTax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnTax.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnTax.ForeColor = System.Drawing.Color.Gray;
            this.bttnTax.Image = ((System.Drawing.Image)(resources.GetObject("bttnTax.Image")));
            this.bttnTax.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnTax.Location = new System.Drawing.Point(336, 60);
            this.bttnTax.Name = "bttnTax";
            this.bttnTax.Size = new System.Drawing.Size(145, 62);
            this.bttnTax.TabIndex = 728;
            this.bttnTax.TabStop = false;
            this.bttnTax.Text = "Tax";
            this.bttnTax.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 727;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 726;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileTax_Master);
            this.flowLayoutPanel1.Controls.Add(this.TileRoom_Tax);
            this.flowLayoutPanel1.Controls.Add(this.TileHall_Tax);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 729;
            // 
            // TileTax_Master
            // 
            this.TileTax_Master.ActiveControl = null;
            this.TileTax_Master.Location = new System.Drawing.Point(3, 3);
            this.TileTax_Master.Name = "TileTax_Master";
            this.TileTax_Master.Size = new System.Drawing.Size(176, 94);
            this.TileTax_Master.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileTax_Master.TabIndex = 3;
            this.TileTax_Master.Text = "Tax Master";
            this.TileTax_Master.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileTax_Master.TileImage = ((System.Drawing.Image)(resources.GetObject("TileTax_Master.TileImage")));
            this.TileTax_Master.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileTax_Master.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileTax_Master.UseSelectable = true;
            this.TileTax_Master.UseTileImage = true;
            // 
            // TileRoom_Tax
            // 
            this.TileRoom_Tax.ActiveControl = null;
            this.TileRoom_Tax.Location = new System.Drawing.Point(185, 3);
            this.TileRoom_Tax.Name = "TileRoom_Tax";
            this.TileRoom_Tax.Size = new System.Drawing.Size(176, 94);
            this.TileRoom_Tax.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileRoom_Tax.TabIndex = 5;
            this.TileRoom_Tax.Text = "Assign  Room Tax";
            this.TileRoom_Tax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileRoom_Tax.TileImage = ((System.Drawing.Image)(resources.GetObject("TileRoom_Tax.TileImage")));
            this.TileRoom_Tax.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileRoom_Tax.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.TileRoom_Tax.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileRoom_Tax.UseSelectable = true;
            this.TileRoom_Tax.UseTileImage = true;
            // 
            // TileHall_Tax
            // 
            this.TileHall_Tax.ActiveControl = null;
            this.TileHall_Tax.Location = new System.Drawing.Point(367, 3);
            this.TileHall_Tax.Name = "TileHall_Tax";
            this.TileHall_Tax.Size = new System.Drawing.Size(176, 94);
            this.TileHall_Tax.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileHall_Tax.TabIndex = 6;
            this.TileHall_Tax.Text = "Assign Hall Tax";
            this.TileHall_Tax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileHall_Tax.TileImage = ((System.Drawing.Image)(resources.GetObject("TileHall_Tax.TileImage")));
            this.TileHall_Tax.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileHall_Tax.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.TileHall_Tax.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileHall_Tax.UseSelectable = true;
            this.TileHall_Tax.UseTileImage = true;
            // 
            // FrmB_BackOfzTax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnTax);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzTax";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Back Office Tax";
            this.Load += new System.EventHandler(this.FrmB_BackOfzTax_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnTax, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnTax;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileTax_Master;
        private MetroFramework.Controls.MetroTile TileRoom_Tax;
        private MetroFramework.Controls.MetroTile TileHall_Tax;
    }
}