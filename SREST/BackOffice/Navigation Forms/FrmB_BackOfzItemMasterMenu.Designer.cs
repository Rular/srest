﻿namespace SREST
{
    partial class FrmB_BackOfzItemMasterMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzItemMasterMenu));
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileItmCategory = new MetroFramework.Controls.MetroTile();
            this.TileSubCategory = new MetroFramework.Controls.MetroTile();
            this.TileSubCategory2 = new MetroFramework.Controls.MetroTile();
            this.TileUnitGroup = new MetroFramework.Controls.MetroTile();
            this.TileUnit = new MetroFramework.Controls.MetroTile();
            this.TileUnitConversion = new MetroFramework.Controls.MetroTile();
            this.TilePrinters = new MetroFramework.Controls.MetroTile();
            this.TileItem = new MetroFramework.Controls.MetroTile();
            this.TileExcellUpload = new MetroFramework.Controls.MetroTile();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(15, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(164, 62);
            this.bttnBackOffice.TabIndex = 720;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "       Back Office";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            this.bttnBackOffice.Click += new System.EventHandler(this.BttnBackOffice_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.TileItmCategory);
            this.flowLayoutPanel1.Controls.Add(this.TileSubCategory);
            this.flowLayoutPanel1.Controls.Add(this.TileSubCategory2);
            this.flowLayoutPanel1.Controls.Add(this.TileUnitGroup);
            this.flowLayoutPanel1.Controls.Add(this.TileUnit);
            this.flowLayoutPanel1.Controls.Add(this.TileUnitConversion);
            this.flowLayoutPanel1.Controls.Add(this.TilePrinters);
            this.flowLayoutPanel1.Controls.Add(this.TileItem);
            this.flowLayoutPanel1.Controls.Add(this.TileExcellUpload);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 125);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(767, 382);
            this.flowLayoutPanel1.TabIndex = 721;
            // 
            // TileItmCategory
            // 
            this.TileItmCategory.ActiveControl = null;
            this.TileItmCategory.Location = new System.Drawing.Point(3, 3);
            this.TileItmCategory.Name = "TileItmCategory";
            this.TileItmCategory.Size = new System.Drawing.Size(176, 94);
            this.TileItmCategory.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileItmCategory.TabIndex = 2;
            this.TileItmCategory.Text = "Item Category";
            this.TileItmCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileItmCategory.TileImage = ((System.Drawing.Image)(resources.GetObject("TileItmCategory.TileImage")));
            this.TileItmCategory.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileItmCategory.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileItmCategory.UseSelectable = true;
            this.TileItmCategory.UseTileImage = true;
            this.TileItmCategory.Click += new System.EventHandler(this.TileItmCategory_Click);
            // 
            // TileSubCategory
            // 
            this.TileSubCategory.ActiveControl = null;
            this.TileSubCategory.Location = new System.Drawing.Point(185, 3);
            this.TileSubCategory.Name = "TileSubCategory";
            this.TileSubCategory.Size = new System.Drawing.Size(176, 94);
            this.TileSubCategory.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSubCategory.TabIndex = 4;
            this.TileSubCategory.Text = "Sub Category";
            this.TileSubCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSubCategory.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSubCategory.TileImage")));
            this.TileSubCategory.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSubCategory.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSubCategory.UseSelectable = true;
            this.TileSubCategory.UseTileImage = true;
            this.TileSubCategory.Click += new System.EventHandler(this.TileSubCategory_Click);
            // 
            // TileSubCategory2
            // 
            this.TileSubCategory2.ActiveControl = null;
            this.TileSubCategory2.Location = new System.Drawing.Point(367, 3);
            this.TileSubCategory2.Name = "TileSubCategory2";
            this.TileSubCategory2.Size = new System.Drawing.Size(176, 94);
            this.TileSubCategory2.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSubCategory2.TabIndex = 5;
            this.TileSubCategory2.Text = "Sub Category 2";
            this.TileSubCategory2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSubCategory2.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSubCategory2.TileImage")));
            this.TileSubCategory2.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSubCategory2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSubCategory2.UseSelectable = true;
            this.TileSubCategory2.UseTileImage = true;
            this.TileSubCategory2.Click += new System.EventHandler(this.TileSubCategory2_Click);
            // 
            // TileUnitGroup
            // 
            this.TileUnitGroup.ActiveControl = null;
            this.TileUnitGroup.Location = new System.Drawing.Point(549, 3);
            this.TileUnitGroup.Name = "TileUnitGroup";
            this.TileUnitGroup.Size = new System.Drawing.Size(176, 94);
            this.TileUnitGroup.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileUnitGroup.TabIndex = 6;
            this.TileUnitGroup.Text = "Unit Group";
            this.TileUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileUnitGroup.TileImage = ((System.Drawing.Image)(resources.GetObject("TileUnitGroup.TileImage")));
            this.TileUnitGroup.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileUnitGroup.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileUnitGroup.UseSelectable = true;
            this.TileUnitGroup.UseTileImage = true;
            this.TileUnitGroup.Click += new System.EventHandler(this.TileUnitGroup_Click);
            // 
            // TileUnit
            // 
            this.TileUnit.ActiveControl = null;
            this.TileUnit.Location = new System.Drawing.Point(3, 103);
            this.TileUnit.Name = "TileUnit";
            this.TileUnit.Size = new System.Drawing.Size(176, 94);
            this.TileUnit.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileUnit.TabIndex = 7;
            this.TileUnit.Text = "Unit";
            this.TileUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileUnit.TileImage = ((System.Drawing.Image)(resources.GetObject("TileUnit.TileImage")));
            this.TileUnit.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileUnit.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileUnit.UseSelectable = true;
            this.TileUnit.UseTileImage = true;
            this.TileUnit.Click += new System.EventHandler(this.TileUnit_Click);
            // 
            // TileUnitConversion
            // 
            this.TileUnitConversion.ActiveControl = null;
            this.TileUnitConversion.Location = new System.Drawing.Point(185, 103);
            this.TileUnitConversion.Name = "TileUnitConversion";
            this.TileUnitConversion.Size = new System.Drawing.Size(176, 94);
            this.TileUnitConversion.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileUnitConversion.TabIndex = 8;
            this.TileUnitConversion.Text = "Unit Conversion";
            this.TileUnitConversion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileUnitConversion.TileImage = ((System.Drawing.Image)(resources.GetObject("TileUnitConversion.TileImage")));
            this.TileUnitConversion.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileUnitConversion.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileUnitConversion.UseSelectable = true;
            this.TileUnitConversion.UseTileImage = true;
            this.TileUnitConversion.Click += new System.EventHandler(this.TileUnitConversion_Click);
            // 
            // TilePrinters
            // 
            this.TilePrinters.ActiveControl = null;
            this.TilePrinters.Location = new System.Drawing.Point(367, 103);
            this.TilePrinters.Name = "TilePrinters";
            this.TilePrinters.Size = new System.Drawing.Size(176, 94);
            this.TilePrinters.Style = MetroFramework.MetroColorStyle.Silver;
            this.TilePrinters.TabIndex = 9;
            this.TilePrinters.Text = "Printers";
            this.TilePrinters.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TilePrinters.TileImage = ((System.Drawing.Image)(resources.GetObject("TilePrinters.TileImage")));
            this.TilePrinters.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TilePrinters.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TilePrinters.UseSelectable = true;
            this.TilePrinters.UseTileImage = true;
            this.TilePrinters.Click += new System.EventHandler(this.TilePrinters_Click);
            // 
            // TileItem
            // 
            this.TileItem.ActiveControl = null;
            this.TileItem.Location = new System.Drawing.Point(549, 103);
            this.TileItem.Name = "TileItem";
            this.TileItem.Size = new System.Drawing.Size(176, 94);
            this.TileItem.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileItem.TabIndex = 10;
            this.TileItem.Text = "Item";
            this.TileItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileItem.TileImage = ((System.Drawing.Image)(resources.GetObject("TileItem.TileImage")));
            this.TileItem.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileItem.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileItem.UseSelectable = true;
            this.TileItem.UseTileImage = true;
            this.TileItem.Click += new System.EventHandler(this.TileItem_Click);
            // 
            // TileExcellUpload
            // 
            this.TileExcellUpload.ActiveControl = null;
            this.TileExcellUpload.Location = new System.Drawing.Point(3, 203);
            this.TileExcellUpload.Name = "TileExcellUpload";
            this.TileExcellUpload.Size = new System.Drawing.Size(176, 94);
            this.TileExcellUpload.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileExcellUpload.TabIndex = 11;
            this.TileExcellUpload.Text = "Excell Upload";
            this.TileExcellUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileExcellUpload.TileImage = ((System.Drawing.Image)(resources.GetObject("TileExcellUpload.TileImage")));
            this.TileExcellUpload.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileExcellUpload.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileExcellUpload.UseSelectable = true;
            this.TileExcellUpload.UseTileImage = true;
            this.TileExcellUpload.Click += new System.EventHandler(this.TileExcellUpload_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gray;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(185, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 62);
            this.button1.TabIndex = 723;
            this.button1.TabStop = false;
            this.button1.Text = "       Master Files";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.AliceBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Gray;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(336, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 62);
            this.button2.TabIndex = 724;
            this.button2.TabStop = false;
            this.button2.Text = "       Item Master";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // FrmB_BackOfzItemMasterMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 516);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzItemMasterMenu";
            this.Text = "FrmB_BackOfzItemMaster";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmB_BackOfzItemMaster_FormClosing);
            this.Load += new System.EventHandler(this.FrmB_BackOfzItemMaster_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileItmCategory;
        private MetroFramework.Controls.MetroTile TileSubCategory;
        private MetroFramework.Controls.MetroTile TileSubCategory2;
        private MetroFramework.Controls.MetroTile TileUnitGroup;
        private MetroFramework.Controls.MetroTile TileUnit;
        private MetroFramework.Controls.MetroTile TileUnitConversion;
        private MetroFramework.Controls.MetroTile TilePrinters;
        private MetroFramework.Controls.MetroTile TileItem;
        private MetroFramework.Controls.MetroTile TileExcellUpload;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}