﻿namespace SREST
{
    partial class FrmB_BackOfzCurrencyMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmB_BackOfzCurrencyMenu));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileCurrency_Exchange = new MetroFramework.Controls.MetroTile();
            this.TileMcurrancy = new MetroFramework.Controls.MetroTile();
            this.bttnCurrency = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileCurrency_Exchange);
            this.flowLayoutPanel1.Controls.Add(this.TileMcurrancy);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 715;
            // 
            // TileCurrency_Exchange
            // 
            this.TileCurrency_Exchange.ActiveControl = null;
            this.TileCurrency_Exchange.Location = new System.Drawing.Point(3, 3);
            this.TileCurrency_Exchange.Name = "TileCurrency_Exchange";
            this.TileCurrency_Exchange.Size = new System.Drawing.Size(176, 94);
            this.TileCurrency_Exchange.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileCurrency_Exchange.TabIndex = 3;
            this.TileCurrency_Exchange.Text = "Exchange";
            this.TileCurrency_Exchange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileCurrency_Exchange.TileImage = ((System.Drawing.Image)(resources.GetObject("TileCurrency_Exchange.TileImage")));
            this.TileCurrency_Exchange.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileCurrency_Exchange.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileCurrency_Exchange.UseSelectable = true;
            this.TileCurrency_Exchange.UseTileImage = true;
            // 
            // TileMcurrancy
            // 
            this.TileMcurrancy.ActiveControl = null;
            this.TileMcurrancy.Location = new System.Drawing.Point(185, 3);
            this.TileMcurrancy.Name = "TileMcurrancy";
            this.TileMcurrancy.Size = new System.Drawing.Size(176, 94);
            this.TileMcurrancy.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileMcurrancy.TabIndex = 5;
            this.TileMcurrancy.Text = "Multiple Currency";
            this.TileMcurrancy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileMcurrancy.TileImage = ((System.Drawing.Image)(resources.GetObject("TileMcurrancy.TileImage")));
            this.TileMcurrancy.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileMcurrancy.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.TileMcurrancy.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileMcurrancy.UseSelectable = true;
            this.TileMcurrancy.UseTileImage = true;
            this.TileMcurrancy.Click += new System.EventHandler(this.TileMcurrancy_Click);
            // 
            // bttnCurrency
            // 
            this.bttnCurrency.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnCurrency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnCurrency.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnCurrency.FlatAppearance.BorderSize = 0;
            this.bttnCurrency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnCurrency.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnCurrency.ForeColor = System.Drawing.Color.Gray;
            this.bttnCurrency.Image = ((System.Drawing.Image)(resources.GetObject("bttnCurrency.Image")));
            this.bttnCurrency.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnCurrency.Location = new System.Drawing.Point(336, 60);
            this.bttnCurrency.Name = "bttnCurrency";
            this.bttnCurrency.Size = new System.Drawing.Size(145, 62);
            this.bttnCurrency.TabIndex = 725;
            this.bttnCurrency.TabStop = false;
            this.bttnCurrency.Text = "Currency";
            this.bttnCurrency.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 724;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 723;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // FrmB_BackOfzCurrencyMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bttnCurrency);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmB_BackOfzCurrencyMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmB_BackOfzCurrancey";
            this.Load += new System.EventHandler(this.FrmB_BackOfzCurrancey_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnCurrency, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private MetroFramework.Controls.MetroTile TileCurrency_Exchange;
        private MetroFramework.Controls.MetroTile TileMcurrancy;
        private System.Windows.Forms.Button bttnCurrency;
    }
}