﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class FrmB_BackOfzGuestMenu : frmC_ModelGreen
    {
        public FrmB_BackOfzGuestMenu()
        {
            InitializeComponent();
        }

        private static FrmB_BackOfzGuestMenu singleton = null;

        public static FrmB_BackOfzGuestMenu Instance
        {
            get
            {
                if (FrmB_BackOfzGuestMenu.singleton == null)
                {
                    FrmB_BackOfzGuestMenu.singleton = new FrmB_BackOfzGuestMenu();
                }
                return FrmB_BackOfzGuestMenu.singleton;
            }
        }

        private void FrmB_BackOfzGuest_Load(object sender, EventArgs e)
        {

        }
    }
}
