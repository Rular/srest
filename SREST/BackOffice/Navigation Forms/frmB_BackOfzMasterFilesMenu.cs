﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmB_BackOfzMasterFilesMenu : frmC_ModelGreen
    {
        public frmB_BackOfzMasterFilesMenu()
        {
            InitializeComponent();
        }

        private static frmB_BackOfzMasterFilesMenu singleton = null;

        public static frmB_BackOfzMasterFilesMenu Instance
        {
            get
            {
                if (frmB_BackOfzMasterFilesMenu.singleton == null)
                {
                    frmB_BackOfzMasterFilesMenu.singleton = new frmB_BackOfzMasterFilesMenu();
                }
                return frmB_BackOfzMasterFilesMenu.singleton;
            }
        }

        private void FrmB_BackOfzMasterFiles_Load(object sender, EventArgs e)
        {

        }

        private void BttnBackOffice_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMainMenu settings = frmB_BackOfzMainMenu.Instance;
            settings.MdiParent = this.ParentForm;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }

        private void TileItemMaster_Click(object sender, EventArgs e)
        {
            FrmB_BackOfzItemMasterMenu Itm_Master = FrmB_BackOfzItemMasterMenu.Instance;
            Itm_Master.MdiParent = this.ParentForm;
            if (Itm_Master.Visible == true)
            {
                Itm_Master.Activate();
            }
            else
            {
                FrmB_BackOfzItemMasterMenu.Instance.Show();
            }
        }

        private void TileCurrency_Click(object sender, EventArgs e)
        {
            FrmB_BackOfzCurrencyMenu currency = FrmB_BackOfzCurrencyMenu.Instance;
            currency.MdiParent = this.ParentForm;
            if (currency.Visible == true)
            {
                currency.Activate();
            }
            else
            {
                FrmB_BackOfzCurrencyMenu.Instance.Show();
            }
        }

        private void TileGuests_Click(object sender, EventArgs e)
        {
            FrmB_BackOfzGuestMenu guest = FrmB_BackOfzGuestMenu.Instance;
            guest.MdiParent = this.ParentForm;
            if (guest.Visible == true)
            {
                guest.Activate();
            }
            else
            {
                FrmB_BackOfzGuestMenu.Instance.Show();
            }
        }

        private void TileEmployee_Click(object sender, EventArgs e)
        {
            FrmB_BackOfzEmpMenu employeeMenu = FrmB_BackOfzEmpMenu.Instance;
            employeeMenu.MdiParent = this.ParentForm;
            if (employeeMenu.Visible == true)
            {
                employeeMenu.Activate();
            }
            else
            {
                FrmB_BackOfzEmpMenu.Instance.Show();
            }
        }

        private void TileTableMaster_Click(object sender, EventArgs e)
        {
            FrmB_BackOfzTableMenu tbl = FrmB_BackOfzTableMenu.Instance;
            tbl.MdiParent = this.ParentForm;
            if (tbl.Visible == true)
            {
                tbl.Activate();
            }
            else
            {
                FrmB_BackOfzTableMenu.Instance.Show();
            }
        }
    }
}
