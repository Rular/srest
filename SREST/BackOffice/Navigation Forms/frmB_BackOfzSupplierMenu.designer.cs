﻿namespace SREST
{
    partial class frmB_BackOfzSupplierMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzSupplierMenu));
            this.bttnSupplier = new System.Windows.Forms.Button();
            this.bttnMasterFile = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileSupplier_Setup = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnSupplier
            // 
            this.bttnSupplier.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnSupplier.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnSupplier.FlatAppearance.BorderSize = 0;
            this.bttnSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnSupplier.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnSupplier.ForeColor = System.Drawing.Color.Gray;
            this.bttnSupplier.Image = ((System.Drawing.Image)(resources.GetObject("bttnSupplier.Image")));
            this.bttnSupplier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnSupplier.Location = new System.Drawing.Point(336, 60);
            this.bttnSupplier.Name = "bttnSupplier";
            this.bttnSupplier.Size = new System.Drawing.Size(145, 62);
            this.bttnSupplier.TabIndex = 731;
            this.bttnSupplier.TabStop = false;
            this.bttnSupplier.Text = "Supplier";
            this.bttnSupplier.UseVisualStyleBackColor = true;
            // 
            // bttnMasterFile
            // 
            this.bttnMasterFile.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnMasterFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnMasterFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnMasterFile.FlatAppearance.BorderSize = 0;
            this.bttnMasterFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMasterFile.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMasterFile.ForeColor = System.Drawing.Color.Gray;
            this.bttnMasterFile.Image = ((System.Drawing.Image)(resources.GetObject("bttnMasterFile.Image")));
            this.bttnMasterFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnMasterFile.Location = new System.Drawing.Point(197, 60);
            this.bttnMasterFile.Name = "bttnMasterFile";
            this.bttnMasterFile.Size = new System.Drawing.Size(145, 62);
            this.bttnMasterFile.TabIndex = 730;
            this.bttnMasterFile.TabStop = false;
            this.bttnMasterFile.Text = "       Master Files";
            this.bttnMasterFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMasterFile.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 729;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileSupplier_Setup);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 732;
            // 
            // TileSupplier_Setup
            // 
            this.TileSupplier_Setup.ActiveControl = null;
            this.TileSupplier_Setup.Location = new System.Drawing.Point(3, 3);
            this.TileSupplier_Setup.Name = "TileSupplier_Setup";
            this.TileSupplier_Setup.Size = new System.Drawing.Size(176, 94);
            this.TileSupplier_Setup.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSupplier_Setup.TabIndex = 3;
            this.TileSupplier_Setup.Text = "Supplier Setup";
            this.TileSupplier_Setup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSupplier_Setup.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSupplier_Setup.TileImage")));
            this.TileSupplier_Setup.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSupplier_Setup.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSupplier_Setup.UseSelectable = true;
            this.TileSupplier_Setup.UseTileImage = true;
            // 
            // frmB_BackOfzSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnSupplier);
            this.Controls.Add(this.bttnMasterFile);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzSupplier";
            this.Text = "Supplier";
            this.Load += new System.EventHandler(this.frmB_BackOfzSupplier_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnMasterFile, 0);
            this.Controls.SetChildIndex(this.bttnSupplier, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnSupplier;
        private System.Windows.Forms.Button bttnMasterFile;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileSupplier_Setup;
    }
}