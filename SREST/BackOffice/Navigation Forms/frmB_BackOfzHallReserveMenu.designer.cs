﻿namespace SREST
{
    partial class frmB_BackOfzHall_ReserveMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzHall_ReserveMenu));
            this.bttnHall_Reservation = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileHall_Reaservation = new MetroFramework.Controls.MetroTile();
            this.TileGuests = new MetroFramework.Controls.MetroTile();
            this.TileEmployee = new MetroFramework.Controls.MetroTile();
            this.TileTax = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnHall_Reservation
            // 
            this.bttnHall_Reservation.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnHall_Reservation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnHall_Reservation.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnHall_Reservation.FlatAppearance.BorderSize = 0;
            this.bttnHall_Reservation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnHall_Reservation.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnHall_Reservation.ForeColor = System.Drawing.Color.Gray;
            this.bttnHall_Reservation.Image = ((System.Drawing.Image)(resources.GetObject("bttnHall_Reservation.Image")));
            this.bttnHall_Reservation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnHall_Reservation.Location = new System.Drawing.Point(197, 60);
            this.bttnHall_Reservation.Name = "bttnHall_Reservation";
            this.bttnHall_Reservation.Size = new System.Drawing.Size(158, 62);
            this.bttnHall_Reservation.TabIndex = 724;
            this.bttnHall_Reservation.TabStop = false;
            this.bttnHall_Reservation.Text = "Hall Reservation";
            this.bttnHall_Reservation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnHall_Reservation.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 723;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.TileHall_Reaservation);
            this.flowLayoutPanel1.Controls.Add(this.TileGuests);
            this.flowLayoutPanel1.Controls.Add(this.TileEmployee);
            this.flowLayoutPanel1.Controls.Add(this.TileTax);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(767, 418);
            this.flowLayoutPanel1.TabIndex = 725;
            // 
            // TileHall_Reaservation
            // 
            this.TileHall_Reaservation.ActiveControl = null;
            this.TileHall_Reaservation.Location = new System.Drawing.Point(3, 3);
            this.TileHall_Reaservation.Name = "TileHall_Reaservation";
            this.TileHall_Reaservation.Size = new System.Drawing.Size(176, 94);
            this.TileHall_Reaservation.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileHall_Reaservation.TabIndex = 2;
            this.TileHall_Reaservation.Text = "Hall Resavation";
            this.TileHall_Reaservation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileHall_Reaservation.TileImage = ((System.Drawing.Image)(resources.GetObject("TileHall_Reaservation.TileImage")));
            this.TileHall_Reaservation.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileHall_Reaservation.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileHall_Reaservation.UseSelectable = true;
            this.TileHall_Reaservation.UseTileImage = true;
            // 
            // TileGuests
            // 
            this.TileGuests.ActiveControl = null;
            this.TileGuests.Location = new System.Drawing.Point(185, 3);
            this.TileGuests.Name = "TileGuests";
            this.TileGuests.Size = new System.Drawing.Size(176, 94);
            this.TileGuests.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileGuests.TabIndex = 4;
            this.TileGuests.Text = " Reservatio Gride";
            this.TileGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileGuests.TileImage = ((System.Drawing.Image)(resources.GetObject("TileGuests.TileImage")));
            this.TileGuests.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileGuests.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileGuests.UseSelectable = true;
            this.TileGuests.UseTileImage = true;
            // 
            // TileEmployee
            // 
            this.TileEmployee.ActiveControl = null;
            this.TileEmployee.Location = new System.Drawing.Point(367, 3);
            this.TileEmployee.Name = "TileEmployee";
            this.TileEmployee.Size = new System.Drawing.Size(176, 94);
            this.TileEmployee.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileEmployee.TabIndex = 5;
            this.TileEmployee.Text = "Inquiry";
            this.TileEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TileEmployee.TileImage = ((System.Drawing.Image)(resources.GetObject("TileEmployee.TileImage")));
            this.TileEmployee.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileEmployee.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileEmployee.UseSelectable = true;
            this.TileEmployee.UseTileImage = true;
            // 
            // TileTax
            // 
            this.TileTax.ActiveControl = null;
            this.TileTax.Location = new System.Drawing.Point(549, 3);
            this.TileTax.Name = "TileTax";
            this.TileTax.Size = new System.Drawing.Size(176, 94);
            this.TileTax.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileTax.TabIndex = 6;
            this.TileTax.Text = "Inquiry Report";
            this.TileTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileTax.TileImage = ((System.Drawing.Image)(resources.GetObject("TileTax.TileImage")));
            this.TileTax.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileTax.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileTax.UseSelectable = true;
            this.TileTax.UseTileImage = true;
            // 
            // frmB_BackOfzHall_Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnHall_Reservation);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzHall_Reservation";
            this.Text = "frmB_BackOfzHall_Reservation";
            this.Load += new System.EventHandler(this.frmB_BackOfzHall_Reservation_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnHall_Reservation, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnHall_Reservation;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileHall_Reaservation;
        private MetroFramework.Controls.MetroTile TileGuests;
        private MetroFramework.Controls.MetroTile TileEmployee;
        private MetroFramework.Controls.MetroTile TileTax;
    }
}