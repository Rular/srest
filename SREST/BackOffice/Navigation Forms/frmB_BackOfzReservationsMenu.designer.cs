﻿namespace SREST
{
    partial class frmB_BackOfzReservationsMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB_BackOfzReservationsMenu));
            this.bttnReservations = new System.Windows.Forms.Button();
            this.bttnBackOffice = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TileAddReservations = new MetroFramework.Controls.MetroTile();
            this.TileReservationsSchedule = new MetroFramework.Controls.MetroTile();
            this.TileSchedulev2 = new MetroFramework.Controls.MetroTile();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bttnReservations
            // 
            this.bttnReservations.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnReservations.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnReservations.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnReservations.FlatAppearance.BorderSize = 0;
            this.bttnReservations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnReservations.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnReservations.ForeColor = System.Drawing.Color.Gray;
            this.bttnReservations.Image = ((System.Drawing.Image)(resources.GetObject("bttnReservations.Image")));
            this.bttnReservations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnReservations.Location = new System.Drawing.Point(197, 60);
            this.bttnReservations.Name = "bttnReservations";
            this.bttnReservations.Size = new System.Drawing.Size(145, 62);
            this.bttnReservations.TabIndex = 732;
            this.bttnReservations.TabStop = false;
            this.bttnReservations.Text = "Reservations";
            this.bttnReservations.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnReservations.UseVisualStyleBackColor = true;
            // 
            // bttnBackOffice
            // 
            this.bttnBackOffice.BackColor = System.Drawing.Color.AliceBlue;
            this.bttnBackOffice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnBackOffice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnBackOffice.FlatAppearance.BorderSize = 0;
            this.bttnBackOffice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnBackOffice.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnBackOffice.ForeColor = System.Drawing.Color.Gray;
            this.bttnBackOffice.Image = ((System.Drawing.Image)(resources.GetObject("bttnBackOffice.Image")));
            this.bttnBackOffice.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnBackOffice.Location = new System.Drawing.Point(12, 60);
            this.bttnBackOffice.Name = "bttnBackOffice";
            this.bttnBackOffice.Size = new System.Drawing.Size(179, 62);
            this.bttnBackOffice.TabIndex = 731;
            this.bttnBackOffice.TabStop = false;
            this.bttnBackOffice.Text = "Back Office \r\n";
            this.bttnBackOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnBackOffice.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.TileAddReservations);
            this.flowLayoutPanel1.Controls.Add(this.TileReservationsSchedule);
            this.flowLayoutPanel1.Controls.Add(this.TileSchedulev2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(8, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(773, 316);
            this.flowLayoutPanel1.TabIndex = 733;
            // 
            // TileAddReservations
            // 
            this.TileAddReservations.ActiveControl = null;
            this.TileAddReservations.Location = new System.Drawing.Point(3, 3);
            this.TileAddReservations.Name = "TileAddReservations";
            this.TileAddReservations.Size = new System.Drawing.Size(176, 94);
            this.TileAddReservations.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileAddReservations.TabIndex = 3;
            this.TileAddReservations.Text = "Reservations";
            this.TileAddReservations.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileAddReservations.TileImage = ((System.Drawing.Image)(resources.GetObject("TileAddReservations.TileImage")));
            this.TileAddReservations.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileAddReservations.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileAddReservations.UseSelectable = true;
            this.TileAddReservations.UseTileImage = true;
            // 
            // TileReservationsSchedule
            // 
            this.TileReservationsSchedule.ActiveControl = null;
            this.TileReservationsSchedule.Location = new System.Drawing.Point(185, 3);
            this.TileReservationsSchedule.Name = "TileReservationsSchedule";
            this.TileReservationsSchedule.Size = new System.Drawing.Size(176, 94);
            this.TileReservationsSchedule.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileReservationsSchedule.TabIndex = 5;
            this.TileReservationsSchedule.Text = "Schedule";
            this.TileReservationsSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileReservationsSchedule.TileImage = ((System.Drawing.Image)(resources.GetObject("TileReservationsSchedule.TileImage")));
            this.TileReservationsSchedule.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileReservationsSchedule.TileTextFontSize = MetroFramework.MetroTileTextSize.Small;
            this.TileReservationsSchedule.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileReservationsSchedule.UseSelectable = true;
            this.TileReservationsSchedule.UseTileImage = true;
            // 
            // TileSchedulev2
            // 
            this.TileSchedulev2.ActiveControl = null;
            this.TileSchedulev2.Location = new System.Drawing.Point(367, 3);
            this.TileSchedulev2.Name = "TileSchedulev2";
            this.TileSchedulev2.Size = new System.Drawing.Size(176, 94);
            this.TileSchedulev2.Style = MetroFramework.MetroColorStyle.Silver;
            this.TileSchedulev2.TabIndex = 11;
            this.TileSchedulev2.Text = "Schedule V2";
            this.TileSchedulev2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TileSchedulev2.TileImage = ((System.Drawing.Image)(resources.GetObject("TileSchedulev2.TileImage")));
            this.TileSchedulev2.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TileSchedulev2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.TileSchedulev2.UseSelectable = true;
            this.TileSchedulev2.UseTileImage = true;
            // 
            // frmB_BackOfzReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.bttnReservations);
            this.Controls.Add(this.bttnBackOffice);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmB_BackOfzReservations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reservations";
            this.Load += new System.EventHandler(this.frmB_BackOfzReservations_Load);
            this.Controls.SetChildIndex(this.bttnBackOffice, 0);
            this.Controls.SetChildIndex(this.bttnReservations, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bttnReservations;
        private System.Windows.Forms.Button bttnBackOffice;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroTile TileAddReservations;
        private MetroFramework.Controls.MetroTile TileReservationsSchedule;
        private MetroFramework.Controls.MetroTile TileSchedulev2;
    }
}