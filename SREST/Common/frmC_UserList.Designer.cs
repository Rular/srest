﻿namespace SREST
{
    partial class frmC_UserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTermal = new MetroFramework.Controls.MetroLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // lblTermal
            // 
            this.lblTermal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTermal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.lblTermal.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTermal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTermal.Location = new System.Drawing.Point(850, 7);
            this.lblTermal.Name = "lblTermal";
            this.lblTermal.Size = new System.Drawing.Size(173, 30);
            this.lblTermal.TabIndex = 715;
            this.lblTermal.Text = "Register  : 1";
            this.lblTermal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblTermal.UseCustomBackColor = true;
            this.lblTermal.UseCustomForeColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))));
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 39);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1026, 411);
            this.flowLayoutPanel1.TabIndex = 716;
            // 
            // frmC_UserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 450);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.lblTermal);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmC_UserList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "frmC_UserList";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmC_UserList_FormClosing);
            this.Load += new System.EventHandler(this.FrmC_UserList_Load);
            this.Controls.SetChildIndex(this.lblTermal, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblTermal;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}