﻿namespace SREST
{
    partial class frmC_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmC_Login));
            this.bttnLogin = new MetroFramework.Controls.MetroButton();
            this.bttnCancel = new MetroFramework.Controls.MetroButton();
            this.lblRole = new MetroFramework.Controls.MetroLabel();
            this.lblUserName = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTermal = new MetroFramework.Controls.MetroLabel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblErrorMsg = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bttnLogin
            // 
            this.bttnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnLogin.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnLogin.Location = new System.Drawing.Point(657, 341);
            this.bttnLogin.Name = "bttnLogin";
            this.bttnLogin.Size = new System.Drawing.Size(201, 44);
            this.bttnLogin.TabIndex = 728;
            this.bttnLogin.Text = "Sign in";
            this.bttnLogin.UseCustomBackColor = true;
            this.bttnLogin.UseCustomForeColor = true;
            this.bttnLogin.UseSelectable = true;
            this.bttnLogin.Click += new System.EventHandler(this.BttnLogin_Click);
            // 
            // bttnCancel
            // 
            this.bttnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(118)))), ((int)(((byte)(115)))));
            this.bttnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bttnCancel.Location = new System.Drawing.Point(516, 341);
            this.bttnCancel.Name = "bttnCancel";
            this.bttnCancel.Size = new System.Drawing.Size(135, 44);
            this.bttnCancel.TabIndex = 727;
            this.bttnCancel.Text = "Cancel";
            this.bttnCancel.UseCustomBackColor = true;
            this.bttnCancel.UseCustomForeColor = true;
            this.bttnCancel.UseSelectable = true;
            this.bttnCancel.Click += new System.EventHandler(this.BttnCancel_Click);
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblRole.ForeColor = System.Drawing.Color.Gray;
            this.lblRole.Location = new System.Drawing.Point(595, 251);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(35, 19);
            this.lblRole.TabIndex = 725;
            this.lblRole.Text = "Role";
            this.lblRole.UseCustomBackColor = true;
            this.lblRole.UseCustomForeColor = true;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblUserName.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(595, 222);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(99, 25);
            this.lblUserName.TabIndex = 724;
            this.lblUserName.Text = "User Name";
            this.lblUserName.UseCustomBackColor = true;
            this.lblUserName.UseCustomForeColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(516, 222);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 723;
            this.pictureBox1.TabStop = false;
            // 
            // lblTermal
            // 
            this.lblTermal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTermal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.lblTermal.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTermal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTermal.Location = new System.Drawing.Point(1184, 5);
            this.lblTermal.Name = "lblTermal";
            this.lblTermal.Size = new System.Drawing.Size(173, 30);
            this.lblTermal.TabIndex = 729;
            this.lblTermal.Text = "Register  : 1";
            this.lblTermal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblTermal.UseCustomBackColor = true;
            this.lblTermal.UseCustomForeColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(516, 299);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(342, 31);
            this.txtPassword.TabIndex = 730;
            this.txtPassword.TextChanged += new System.EventHandler(this.TxtPassword_TextChanged);
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtPassword_KeyDown);
            // 
            // lblErrorMsg
            // 
            this.lblErrorMsg.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblErrorMsg.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblErrorMsg.ForeColor = System.Drawing.Color.Red;
            this.lblErrorMsg.Location = new System.Drawing.Point(516, 397);
            this.lblErrorMsg.Name = "lblErrorMsg";
            this.lblErrorMsg.Size = new System.Drawing.Size(342, 32);
            this.lblErrorMsg.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrorMsg.TabIndex = 731;
            this.lblErrorMsg.Text = " Error Message";
            this.lblErrorMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblErrorMsg.UseCustomBackColor = true;
            this.lblErrorMsg.UseCustomForeColor = true;
            this.lblErrorMsg.Visible = false;
            // 
            // frmC_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(1360, 720);
            this.Controls.Add(this.lblErrorMsg);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblTermal);
            this.Controls.Add(this.bttnLogin);
            this.Controls.Add(this.bttnCancel);
            this.Controls.Add(this.lblRole);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.pictureBox1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmC_Login";
            this.Text = "frmC_Login";
            this.Activated += new System.EventHandler(this.FrmC_Login_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmC_Login_FormClosing);
            this.Load += new System.EventHandler(this.FrmC_Login_Load);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.lblUserName, 0);
            this.Controls.SetChildIndex(this.lblRole, 0);
            this.Controls.SetChildIndex(this.bttnCancel, 0);
            this.Controls.SetChildIndex(this.bttnLogin, 0);
            this.Controls.SetChildIndex(this.lblTermal, 0);
            this.Controls.SetChildIndex(this.txtPassword, 0);
            this.Controls.SetChildIndex(this.lblErrorMsg, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton bttnLogin;
        private MetroFramework.Controls.MetroButton bttnCancel;
        private MetroFramework.Controls.MetroLabel lblRole;
        private MetroFramework.Controls.MetroLabel lblUserName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel lblTermal;
        private System.Windows.Forms.TextBox txtPassword;
        private MetroFramework.Controls.MetroLabel lblErrorMsg;
    }
}