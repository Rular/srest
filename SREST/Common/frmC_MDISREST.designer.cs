﻿namespace SREST
{
    partial class frmU_MDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmU_MDI));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTime = new MetroFramework.Controls.MetroLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bttnClose = new System.Windows.Forms.Button();
            this.bttnMinimize = new System.Windows.Forms.Button();
            this.bttnNofications = new System.Windows.Forms.Button();
            this.bttnNetwork = new System.Windows.Forms.Button();
            this.bttnMainMenu = new System.Windows.Forms.Button();
            this.bttnUser = new System.Windows.Forms.Button();
            this.ContextMenuMain = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.tablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.receiptsRefundsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cashInOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.closeCashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.clockInOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.timesheetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.ContextMenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.lblTime.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblTime.Location = new System.Drawing.Point(0, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(77, 43);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "12.00 AM";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTime.UseCustomBackColor = true;
            this.lblTime.UseCustomForeColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(35)))));
            this.panel1.Controls.Add(this.bttnClose);
            this.panel1.Controls.Add(this.bttnMinimize);
            this.panel1.Controls.Add(this.bttnNofications);
            this.panel1.Controls.Add(this.bttnNetwork);
            this.panel1.Controls.Add(this.bttnMainMenu);
            this.panel1.Controls.Add(this.bttnUser);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1082, 43);
            this.panel1.TabIndex = 8;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // bttnClose
            // 
            this.bttnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnClose.FlatAppearance.BorderSize = 0;
            this.bttnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnClose.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnClose.Image = ((System.Drawing.Image)(resources.GetObject("bttnClose.Image")));
            this.bttnClose.Location = new System.Drawing.Point(1035, 1);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(44, 43);
            this.bttnClose.TabIndex = 15;
            this.bttnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnClose.UseVisualStyleBackColor = false;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // bttnMinimize
            // 
            this.bttnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnMinimize.FlatAppearance.BorderSize = 0;
            this.bttnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMinimize.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMinimize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("bttnMinimize.Image")));
            this.bttnMinimize.Location = new System.Drawing.Point(988, 1);
            this.bttnMinimize.Name = "bttnMinimize";
            this.bttnMinimize.Size = new System.Drawing.Size(44, 43);
            this.bttnMinimize.TabIndex = 14;
            this.bttnMinimize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnMinimize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnMinimize.UseVisualStyleBackColor = false;
            this.bttnMinimize.Click += new System.EventHandler(this.BttnMinimize_Click);
            // 
            // bttnNofications
            // 
            this.bttnNofications.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnNofications.FlatAppearance.BorderSize = 0;
            this.bttnNofications.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNofications.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNofications.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnNofications.Image = ((System.Drawing.Image)(resources.GetObject("bttnNofications.Image")));
            this.bttnNofications.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNofications.Location = new System.Drawing.Point(133, 0);
            this.bttnNofications.Name = "bttnNofications";
            this.bttnNofications.Size = new System.Drawing.Size(95, 43);
            this.bttnNofications.TabIndex = 13;
            this.bttnNofications.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnNofications.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNofications.UseVisualStyleBackColor = false;
            // 
            // bttnNetwork
            // 
            this.bttnNetwork.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnNetwork.FlatAppearance.BorderSize = 0;
            this.bttnNetwork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNetwork.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNetwork.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnNetwork.Image = global::SREST.Properties.Resources.Wifi_Connected_Icon;
            this.bttnNetwork.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNetwork.Location = new System.Drawing.Point(83, 1);
            this.bttnNetwork.Name = "bttnNetwork";
            this.bttnNetwork.Size = new System.Drawing.Size(44, 43);
            this.bttnNetwork.TabIndex = 12;
            this.bttnNetwork.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnNetwork.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnNetwork.UseVisualStyleBackColor = false;
            this.bttnNetwork.Click += new System.EventHandler(this.BttnNetwork_Click);
            // 
            // bttnMainMenu
            // 
            this.bttnMainMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnMainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnMainMenu.FlatAppearance.BorderSize = 0;
            this.bttnMainMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnMainMenu.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnMainMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnMainMenu.Location = new System.Drawing.Point(762, 0);
            this.bttnMainMenu.Name = "bttnMainMenu";
            this.bttnMainMenu.Size = new System.Drawing.Size(222, 43);
            this.bttnMainMenu.TabIndex = 11;
            this.bttnMainMenu.Text = "Main Menu";
            this.bttnMainMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnMainMenu.UseVisualStyleBackColor = false;
            this.bttnMainMenu.Click += new System.EventHandler(this.BttnMainMenu_Click);
            // 
            // bttnUser
            // 
            this.bttnUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(38)))), ((int)(((byte)(41)))));
            this.bttnUser.FlatAppearance.BorderSize = 0;
            this.bttnUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnUser.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnUser.Image = ((System.Drawing.Image)(resources.GetObject("bttnUser.Image")));
            this.bttnUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnUser.Location = new System.Drawing.Point(234, 0);
            this.bttnUser.Name = "bttnUser";
            this.bttnUser.Size = new System.Drawing.Size(179, 43);
            this.bttnUser.TabIndex = 10;
            this.bttnUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bttnUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bttnUser.UseVisualStyleBackColor = false;
            this.bttnUser.Click += new System.EventHandler(this.BttnUser_Click);
            // 
            // ContextMenuMain
            // 
            this.ContextMenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tablesToolStripMenuItem,
            this.toolStripSeparator1,
            this.ordersToolStripMenuItem,
            this.toolStripSeparator2,
            this.receiptsRefundsToolStripMenuItem,
            this.toolStripSeparator3,
            this.tipsToolStripMenuItem,
            this.toolStripSeparator4,
            this.cashInOutToolStripMenuItem,
            this.toolStripSeparator5,
            this.closeCashToolStripMenuItem,
            this.toolStripSeparator6,
            this.reportsToolStripMenuItem,
            this.toolStripSeparator7,
            this.clockInOutToolStripMenuItem,
            this.toolStripSeparator8,
            this.timesheetsToolStripMenuItem,
            this.toolStripSeparator9,
            this.profileToolStripMenuItem,
            this.toolStripSeparator10,
            this.helpToolStripMenuItem,
            this.toolStripSeparator11,
            this.settingsToolStripMenuItem,
            this.toolStripSeparator12,
            this.exitToolStripMenuItem});
            this.ContextMenuMain.Name = "ContextMenuMain";
            this.ContextMenuMain.Size = new System.Drawing.Size(223, 570);
            this.ContextMenuMain.Text = "Tables";
            this.ContextMenuMain.VisibleChanged += new System.EventHandler(this.ContextMenuMain_VisibleChanged);
            // 
            // tablesToolStripMenuItem
            // 
            this.tablesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.tablesToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.tablesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tablesToolStripMenuItem.Image")));
            this.tablesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tablesToolStripMenuItem.Name = "tablesToolStripMenuItem";
            this.tablesToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.tablesToolStripMenuItem.Text = "Tables";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(219, 6);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.ordersToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.ordersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ordersToolStripMenuItem.Image")));
            this.ordersToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.ordersToolStripMenuItem.Text = "Orders";
            this.ordersToolStripMenuItem.Click += new System.EventHandler(this.OrdersToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(219, 6);
            // 
            // receiptsRefundsToolStripMenuItem
            // 
            this.receiptsRefundsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.receiptsRefundsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.receiptsRefundsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("receiptsRefundsToolStripMenuItem.Image")));
            this.receiptsRefundsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.receiptsRefundsToolStripMenuItem.Name = "receiptsRefundsToolStripMenuItem";
            this.receiptsRefundsToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.receiptsRefundsToolStripMenuItem.Text = "Receipts && Refunds";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(219, 6);
            // 
            // tipsToolStripMenuItem
            // 
            this.tipsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.tipsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.tipsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tipsToolStripMenuItem.Image")));
            this.tipsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tipsToolStripMenuItem.Name = "tipsToolStripMenuItem";
            this.tipsToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.tipsToolStripMenuItem.Text = "Tips";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(219, 6);
            // 
            // cashInOutToolStripMenuItem
            // 
            this.cashInOutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.cashInOutToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.cashInOutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cashInOutToolStripMenuItem.Image")));
            this.cashInOutToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cashInOutToolStripMenuItem.Name = "cashInOutToolStripMenuItem";
            this.cashInOutToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.cashInOutToolStripMenuItem.Text = "Cash In / Out";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(219, 6);
            // 
            // closeCashToolStripMenuItem
            // 
            this.closeCashToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.closeCashToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.closeCashToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("closeCashToolStripMenuItem.Image")));
            this.closeCashToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.closeCashToolStripMenuItem.Name = "closeCashToolStripMenuItem";
            this.closeCashToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.closeCashToolStripMenuItem.Text = "Close Cash";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(219, 6);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.reportsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.reportsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportsToolStripMenuItem.Image")));
            this.reportsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(219, 6);
            // 
            // clockInOutToolStripMenuItem
            // 
            this.clockInOutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.clockInOutToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.clockInOutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clockInOutToolStripMenuItem.Image")));
            this.clockInOutToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.clockInOutToolStripMenuItem.Name = "clockInOutToolStripMenuItem";
            this.clockInOutToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.clockInOutToolStripMenuItem.Text = "Clock In / Out";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(219, 6);
            // 
            // timesheetsToolStripMenuItem
            // 
            this.timesheetsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.timesheetsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.timesheetsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("timesheetsToolStripMenuItem.Image")));
            this.timesheetsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.timesheetsToolStripMenuItem.Name = "timesheetsToolStripMenuItem";
            this.timesheetsToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.timesheetsToolStripMenuItem.Text = "Timesheets";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(219, 6);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.profileToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.profileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileToolStripMenuItem.Image")));
            this.profileToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.profileToolStripMenuItem.Text = "Profile";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(219, 6);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.helpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripMenuItem.Image")));
            this.helpToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(219, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.settingsToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.settingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("settingsToolStripMenuItem.Image")));
            this.settingsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.settingsToolStripMenuItem.Text = "Back Office";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(219, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(131)))), ((int)(((byte)(28)))));
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(222, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // frmU_MDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1082, 453);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.LightGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmU_MDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SREST";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmU_MDI_FormClosing);
            this.Load += new System.EventHandler(this.FrmU_MDI_Load);
            this.panel1.ResumeLayout(false);
            this.ContextMenuMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Controls.MetroLabel lblTime;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroContextMenu ContextMenuMain;
        private System.Windows.Forms.ToolStripMenuItem tablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiptsRefundsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashInOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeCashToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clockInOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timesheetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.Button bttnMainMenu;
        private System.Windows.Forms.Button bttnNetwork;
        private System.Windows.Forms.Button bttnNofications;
        private System.Windows.Forms.Button bttnClose;
        private System.Windows.Forms.Button bttnMinimize;
        public System.Windows.Forms.Button bttnUser;
    }
}



