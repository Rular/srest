﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmC_Login : SREST.frmC_ModelGreen
    {

        clsU_User cuser = new clsU_User();
        cls_Connection con = new cls_Connection();

        public  static String User_Name,User_Role,User_Id;
        public frmC_Login()
        {
            InitializeComponent();
        }

        private static frmC_Login singleton = null;

        public static frmC_Login Instance
        {
            get
            {
                if (frmC_Login.singleton == null)
                {
                    frmC_Login.singleton = new frmC_Login();
                }
                return frmC_Login.singleton;
            }
        }


        public static void SetUSers(string username,string role,string userId)
        {
            User_Name = username;
            User_Role = role;
            User_Id = userId;

            //set user name to class conntion
            cls_Connection.USer_Name = username;
        }

        private void FrmC_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmC_Login.singleton = null;
        }

        private void FrmC_Login_Load(object sender, EventArgs e)
        {
            lblUserName.Text = User_Name;
            lblRole.Text = User_Role;

        }

        private void BttnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void TxtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //enter key is down
                if (cuser.IsExistsUser(User_Id, txtPassword.Text.ToString()) == true)
                {

                    frmT_OrderMain orderMain = frmT_OrderMain.Instance;
                    orderMain.MdiParent = this.ParentForm;
                    orderMain.Show();
                    this.Hide();

                }
                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "You have entered an invalid password.";
                    txtPassword.Text = "";
                    txtPassword.Focus();
                }
            }

        }

        private void BttnLogin_Click(object sender, EventArgs e)
        {
           
            cls_Connection.USer_Name = User_Name;
            if (cuser.IsExistsUser(User_Id,txtPassword.Text.ToString()) == true)
            {


                frmT_OrderMain orderMain = frmT_OrderMain.Instance;
                orderMain.MdiParent = this.ParentForm;
                frmU_MDI.User_Name = User_Name;
                orderMain.Show();
                this.Hide();
              
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "You have entered an invalid password.";
                txtPassword.Text = "";
                txtPassword.Focus();
            }
        }

        private void FrmC_Login_Activated(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }
    }
}
