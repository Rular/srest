namespace SREST
{
    partial class frmC_ModelBlack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFormCaption = new System.Windows.Forms.Label();
            this.bttnClose = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(80)))), ((int)(((byte)(81)))));
            this.panel1.Controls.Add(this.lblFormCaption);
            this.panel1.Controls.Add(this.bttnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1097, 57);
            this.panel1.TabIndex = 714;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // lblFormCaption
            // 
            this.lblFormCaption.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblFormCaption.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFormCaption.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormCaption.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblFormCaption.Location = new System.Drawing.Point(830, 0);
            this.lblFormCaption.Name = "lblFormCaption";
            this.lblFormCaption.Size = new System.Drawing.Size(248, 57);
            this.lblFormCaption.TabIndex = 1;
            this.lblFormCaption.Text = "Form Caption";
            this.lblFormCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bttnClose
            // 
            this.bttnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(42)))), ((int)(((byte)(16)))));
            this.bttnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.bttnClose.Location = new System.Drawing.Point(1078, 0);
            this.bttnClose.Name = "bttnClose";
            this.bttnClose.Size = new System.Drawing.Size(19, 57);
            this.bttnClose.TabIndex = 0;
            this.bttnClose.TabStop = false;
            this.bttnClose.Click += new System.EventHandler(this.BttnClose_Click);
            // 
            // frmC_ModelBlack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1097, 595);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(115, 90);
            this.Name = "frmC_ModelBlack";
            this.ShowInTaskbar = false;
            this.Text = "frmC_Model";
            this.Load += new System.EventHandler(this.frmU_Model_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmU_Model_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.PictureBox bttnClose;
        public System.Windows.Forms.Label lblFormCaption;
    }
}