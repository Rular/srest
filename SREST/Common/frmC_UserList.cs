﻿using SREST.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SREST;


namespace SREST
{
    public partial class frmC_UserList : SREST.frmC_ModelGreen
    {

        clsU_User user = new clsU_User();

        public string URoles;


       public  DataTable users = new DataTable();
        public DataTable usersRoles = new DataTable();
        public frmC_UserList()
        {
            InitializeComponent();
        }

        private static frmC_UserList singleton = null;

        public static frmC_UserList Instance
        {
            get
            {
                if (frmC_UserList.singleton == null)
                {
                    frmC_UserList.singleton = new frmC_UserList();
                }
                return frmC_UserList.singleton;
            }
        }

        private void FrmC_UserList_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmC_UserList.singleton = null;
        }

        private void FrmC_UserList_Load(object sender, EventArgs e)
        {
            
            users = user.GetUsers();

            foreach(DataRow rows in users.Rows)
            {
                MetroFramework.Controls.MetroTile Tile = new MetroFramework.Controls.MetroTile();
                Tile.ActiveControl = null;
                Tile.AutoSize = true;
                Tile.Location = new System.Drawing.Point(3, 3);
                Tile.Name = rows[2].ToString();
                Tile.Size = new System.Drawing.Size(166, 89);
                Tile.Style = MetroFramework.MetroColorStyle.Silver;
                Tile.TabIndex = 0;
                Tile.Text = rows[1].ToString();
                Tile.TextAlign = System.Drawing.ContentAlignment.BottomRight;
                Tile.TileImage = SREST.Properties.Resources.user32;
                Tile.TileImageAlign = System.Drawing.ContentAlignment.BottomLeft;
                Tile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
                Tile.UseSelectable = true;
                Tile.UseTileImage = true;
                Tile.Tag = rows[0].ToString(); ;
                flowLayoutPanel1.Controls.Add(Tile);
                //events
                Tile.Click += new EventHandler(button_Click);
            }


        }

        protected void button_Click(object sender, EventArgs e)
        {

            Button button = sender as Button;
            // identify which button was clicked and perform necessary actions
            frmC_Login login = frmC_Login.Instance;
            login.MdiParent = this.ParentForm ;
            if (login.Visible == true)
            {
                login.Activate();
            }
            else
            {
                //  frmC_Login log = new frmC_Login("admin",button.Text.ToString()); 
                // frmC_Login.

                frmC_Login.SetUSers(button.Text.ToString(), button.Name.ToString(),button.Tag.ToString());
                frmC_Login.Instance.Show();
                
            }
        }
    }
}
