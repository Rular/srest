﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Drawing.Printing;
using System.Net;
using System.Diagnostics;
using MetroFramework;
using MetroFramework.Forms;

namespace SREST
{
    public partial class frmU_MDI : Form
    {

        public static string User_Name;
       
        public frmU_MDI()
        {
            InitializeComponent();
        }


        public static void GetLoginUserName(string UserName)
        {
            User_Name = UserName;
        }
        private void ShowButtons(DataTable dt, Panel Panel)
        {
          
            
        }

        private Image getMenuImage(string MenuTag, byte[] byteImage)
        {

            return null;
        }

        private bool ThumbnailCallback()
        {
            return false;
        }

        private void MetroButton1_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("hh:mm tt");

        }

        private void FrmU_MDI_Load(object sender, EventArgs e)
        {

            // check internet connection
            bool Internet = CheckForInternetConnection();

            if (Internet == true)
                bttnNetwork.Image = SREST.Properties.Resources.Wifi_Connected_Icon;
            else
                bttnNetwork.Image = SREST.Properties.Resources.No_Wifi_Icon;

            if (cls_Connection.USer_Name != null)
            {
                bttnUser.Text = User_Name;
            }
            else
            {
                bttnUser.Text = "User";
            }

            frmC_UserList userList = frmC_UserList.Instance;
            userList.MdiParent = this;
            if (userList.Visible == true)
            {
                userList.Activate();
            }
            else
            {
                frmC_UserList.Instance.Show();
            }

            BttnUser_Click( sender, e);
           // bttnUser.Text = cls_Connection.USer_Name.ToString();
        }

     
        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ContextMenuMain_VisibleChanged(object sender, EventArgs e)
        {
            if (ContextMenuMain.Visible == true)
            {
               bttnMainMenu.BackColor = Color.FromArgb(0,136,17);
            }
            else
            {
               bttnMainMenu.BackColor = Color.FromArgb(40, 38, 41);

            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void BttnMainMenu_Click(object sender, EventArgs e)
        {
            ContextMenuMain.Show(bttnMainMenu.Left + this.Left, bttnMainMenu.Top + bttnMainMenu.Height + this.Top);
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void BttnNetwork_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("NCPA.cpl");
            startInfo.UseShellExecute = true;

            Process.Start(startInfo);
        }

        private void BttnClose_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MetroMessageBox.Show(this, "\nContinue Exit Program?","SOFTGRAVITAS RESTAURENT MANAGEMENT SYSTEM (SREST) | EXIT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.Yes)
            {
                System.Environment.Exit(0);
            }
           
        }

        private void BttnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void FrmU_MDI_FormClosing(object sender, FormClosingEventArgs e)
        {
          
        }

        private void BttnUser_Click(object sender, EventArgs e)
        {
            bttnUser.Text = User_Name;
        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmB_BackOfzMainMenu settings = frmB_BackOfzMainMenu.Instance;
            settings.MdiParent = this;
            if (settings.Visible == true)
            {
                settings.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }

        private void OrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmT_OrderMain orderMain = frmT_OrderMain.Instance;
            orderMain.MdiParent = this;
            if (orderMain.Visible == true)
            {
                orderMain.Activate();
            }
            else
            {
                frmT_OrderMain.Instance.Show();
            }
        }

        private void MetroButton1_Click_1(object sender, EventArgs e)
        {

            frmB_BackOfzMainMenu Main = frmB_BackOfzMainMenu.Instance;
            Main.MdiParent = this;
            if (Main.Visible == true)
            {
                Main.Activate();
            }
            else
            {
                frmB_BackOfzMainMenu.Instance.Show();
            }
        }
    }
}
