﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmT_OrderType : Form
    {

        private static frmT_OrderType singleton = null;

        public static frmT_OrderType Instance
        {
            get
            {
                if (frmT_OrderType.singleton == null)
                {
                    frmT_OrderType.singleton = new frmT_OrderType();
                }

                return frmT_OrderType.singleton;
            }
        }

        public frmT_OrderType()
        {
            InitializeComponent();
        }

        private void BttnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
