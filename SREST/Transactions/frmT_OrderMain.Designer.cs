﻿namespace SREST
{
    partial class frmT_OrderMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmT_OrderMain));
            this.bttnNewOrder = new System.Windows.Forms.Button();
            this.dgwOrders = new ADGV.AdvancedDataGridView();
            this.searchToolBar1 = new ADGV.SearchToolBar();
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // bttnClose
            // 
            this.bttnClose.Location = new System.Drawing.Point(887, 0);
            this.bttnClose.Visible = false;
            // 
            // lblFormCaption
            // 
            this.lblFormCaption.Location = new System.Drawing.Point(639, 0);
            this.lblFormCaption.Text = "frmC_Model";
            this.lblFormCaption.Visible = false;
            // 
            // bttnNewOrder
            // 
            this.bttnNewOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bttnNewOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnNewOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bttnNewOrder.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(136)))), ((int)(((byte)(17)))));
            this.bttnNewOrder.FlatAppearance.BorderSize = 0;
            this.bttnNewOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnNewOrder.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bttnNewOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bttnNewOrder.Image = ((System.Drawing.Image)(resources.GetObject("bttnNewOrder.Image")));
            this.bttnNewOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bttnNewOrder.Location = new System.Drawing.Point(715, 6);
            this.bttnNewOrder.Name = "bttnNewOrder";
            this.bttnNewOrder.Size = new System.Drawing.Size(179, 42);
            this.bttnNewOrder.TabIndex = 715;
            this.bttnNewOrder.TabStop = false;
            this.bttnNewOrder.Text = "    New Order";
            this.bttnNewOrder.UseVisualStyleBackColor = false;
            this.bttnNewOrder.Click += new System.EventHandler(this.BttnNewOrder_Click);
            // 
            // dgwOrders
            // 
            this.dgwOrders.AllowUserToAddRows = false;
            this.dgwOrders.AllowUserToDeleteRows = false;
            this.dgwOrders.AutoGenerateContextFilters = true;
            this.dgwOrders.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.dgwOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwOrders.DateWithTime = true;
            this.dgwOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgwOrders.Location = new System.Drawing.Point(0, 57);
            this.dgwOrders.Name = "dgwOrders";
            this.dgwOrders.ReadOnly = true;
            this.dgwOrders.Size = new System.Drawing.Size(906, 358);
            this.dgwOrders.TabIndex = 716;
            this.dgwOrders.TimeFilter = true;
            // 
            // searchToolBar1
            // 
            this.searchToolBar1.AllowMerge = false;
            this.searchToolBar1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.searchToolBar1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.searchToolBar1.Location = new System.Drawing.Point(0, 57);
            this.searchToolBar1.MaximumSize = new System.Drawing.Size(0, 25);
            this.searchToolBar1.MinimumSize = new System.Drawing.Size(0, 25);
            this.searchToolBar1.Name = "searchToolBar1";
            this.searchToolBar1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.searchToolBar1.Size = new System.Drawing.Size(906, 25);
            this.searchToolBar1.TabIndex = 717;
            this.searchToolBar1.Text = "searchToolBar1";
            // 
            // frmT_OrderMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 415);
            this.Controls.Add(this.searchToolBar1);
            this.Controls.Add(this.dgwOrders);
            this.Controls.Add(this.bttnNewOrder);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "frmT_OrderMain";
            this.Text = "frmT_OrderMain";
            this.Load += new System.EventHandler(this.FrmT_OrderMain_Load);
            this.Controls.SetChildIndex(this.bttnNewOrder, 0);
            this.Controls.SetChildIndex(this.dgwOrders, 0);
            this.Controls.SetChildIndex(this.searchToolBar1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bttnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bttnNewOrder;
        private ADGV.AdvancedDataGridView dgwOrders;
        private ADGV.SearchToolBar searchToolBar1;
    }
}