﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SREST
{
    public partial class frmT_OrderMain : frmC_ModelBlack
    {
        public frmT_OrderMain()
        {
            InitializeComponent();
        }

        private static frmT_OrderMain singleton = null;

        public static frmT_OrderMain Instance
        {
            get
            {
                if (frmT_OrderMain.singleton == null)
                {
                    frmT_OrderMain.singleton = new frmT_OrderMain();
                }

                return frmT_OrderMain.singleton;
            }
        }

        private void FrmT_OrderMain_Load(object sender, EventArgs e)
        {
            
        }

        private void BttnNewOrder_Click(object sender, EventArgs e)
        {
            new frmT_OrderType().ShowDialog();
        }
    }
}
